% load('Z:\2017-03-08 NARX models\With David Westwick\Gabriel_s decouplings ALS\decoupling_data.mat');
load('Z:\2017-03-08 NARX models\With David Westwick\Gabriel_s decouplings ALS\over 15 expers\rand_init_1.mat')
% load('Z:\2017-03-08 NARX models\With David Westwick\data_for_plot_thesis_chapter5\data_iterations\rand_init_2.mat')

%%

[Voptim, Goptim, Woptim] = theta2VGW(thetaOptimizedCoeffs, n, m, rtilde, degreeMax);

yOptimModeled = OutputNL(Voptim, Goptim, Woptim, uOperatingPoints);

measureOutput = norm(yOptimModeled - yOutputReal, 'fro') / norm(yOutputReal, 'fro');

clf
hold on
plot(db(fft(yOptimModeled/N)))
plot(db(fft((yOptimModeled(1:2:N/2) - yOutputReal(1:2:N/2))/N)), 'rx')
xlabel('Frequency (normalized)');
ylabel('output (db of magnitude)');
set(gca, 'xlim', [0 N/4], 'xtick', [0 N/4], 'xticklabel', [0, 0.5], 'ytick', [-40 0 20 60])
legend('modeled output', 'prediction error');
% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\fig_output_als_with_errors.tex', 'width', '0.8\textwidth', 'height', '5cm');



