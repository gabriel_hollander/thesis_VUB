\chapter[Parameter reduction of nonlinear models using the CPD]{Parameter reduction of nonlinear models using the Canonical Polyadic Decomposition}\label{chap_4}

\abstracttext{%
\noindent Every parametric model lies on the trade-off line between accuracy and interpretability. 
Increasing a model's interpretability, while keeping its accuracy as good as possible, is of great importance for every existing model today.
Currently, block-oriented and nonlinear state-space models may become hard to interpret due to large internal MIMO nonlinear functions, and need to be simplified \cite{Giri2010,Paduart2008,MaartenSchoukens2014,Mulders2014}. 
Therefore, we designed a model-reduction technique using the tensor CPD for a special type of static nonlinear multiple-input multiple-output models.
For these models, we analyzed how the quality of the model varies in function of the model order.\newline 
This chapter proposes a tensor-based initialization and compares it with a randomly chosen initialization.
Using this tensor-based initialization ensures smaller model errors than when using a random initialization, and thus yields a simpler model, while being able to keep its accuracy as high as possible.\newline
This chapter is organized as follows: Section~\ref{sec_notations} states the problem and Section~\ref{sec_Method} gives an overview of the proposed initialization using tensor decompositions.
Further, Section~\ref{sec_Results} compares two initialization methods using simulated systems and measurements.
One of these test cases is examined in detail in Section~\ref{sec_caseStudy}.
Finally, Section~\ref{sec_conclusion} concludes this chapter.}

\section{Problem statement\label{sec_notations}}
In this chapter, we consider a static nonlinear model with~$m$ inputs and~$n$ outputs. The output of this model \f\ is defined by
$$
	\f\colon\ER^m\to\ER^n\colon \u\mapsto \W\g(\V^T \u),
$$ using the following three internal components (Figure~\ref{fig_graph_repr}):
\begin{itemize}
\item a first transformation matrix $[\v_1 \ldots \v_r] = \V\in\ER^{m\times r}$,
\item a set \g\ of $r$ nonlinear single-input single-output functions $g_1(\v^T_1\u)$, \ldots, $g_r(\v^T_r\u)$, also called \emph{branches}, and
\item a second transformation matrix $\W\in\ER^{n\times r}$. 
\end{itemize}
Although generalizations to other basis functions are possible, we focus our attention to polynomial branches. They thus have the form
$$
g_i(\v^T_i\u) = \sum_{k=0}^{d} c_{ij} (\v^T_i\u)^k,
$$
where $c_{ij}$ are the coefficients of the $i$-th polynomial branch~$g_i$ of degree $d$.

\begin{figure}[ht]
\centering
\includegraphics{chapter4/figures/nonlinear_system_update.pdf}
\caption{The nonlinear models in this chapter all have the same structure. They consist of two transformation matrices~$\V$ and $\W$, surrounding a set $\g$ of $r$ single-input single-output branches $g_1(\v^T_1\u)$, \ldots, $g_r(\v^T_r\u)$.}
\label{fig_graph_repr}
\end{figure}

The problem discussed in this chapter goes as follows: given a multiple-input multiple-output model as described above, find an approximate reduced description involving $\tilde r < r$ branches. 
That is, find appropriate transformation matrices $[\tilde\v_1 \ldots \tilde\v_{\tilde r}] = \Vt\in\ER^{m\times \tilde r}$ and $\Wt\in\ER^{n\times \tilde r}$ and nonlinear single-input single-output functions $\tilde g_1(\tilde\v^T_1\u), \ldots, \tilde g_{\tilde r}(\tilde\v^T_{\tilde r}\u)$ such that the reduced model $\tilde\f$ approximates the original model \f\ in terms of their output:
$$
\f(\u) = \W\g(\V^T \u) \approx  \Wt\tilde\g(\Vt^T \u) = \tilde\f(\u),
$$
for a given set of inputs $\u$ of a fixed domain of inputs. 
Formally, the following cost function is minimized, over the parameters of $\Vt$, $\Wt$ and $\tilde\g$:
\begin{equation}
\min_{\Vt,\Wt,\tilde\g}
\frac{
\sum_{i=1}^N \norm{ \W\g(\V^T \u^{(i)}) - \Wt\tilde\g(\Vt^T \u^{(i)}) }^2}
{
	\sum_{i=1}^N \norm{ \W\g(\V^T \u^{(i)}) }^2
}
.
\label{eq_costfunction}
\end{equation}
In the optimization problem~(\ref{eq_costfunction}), the norm is chosen as the Euclidian norm of vectors, and the cost function is normalized with the normed output of~\f. 
The search space in this optimization consists of the matrix elements of~$\Vt$ and $\Wt$, together with all the coefficients of the function~$\tilde\g$. 
The functions~$\tilde g_1, \ldots, \tilde g_{\tilde r}$ are chosen to be univariate polynomials of degree~$d$. 

Because of the nonlinear function $\tilde\g$, the cost function~(\ref{eq_costfunction}) is  nonlinear and non-convex. 
Any iterative method is sensitive to its initialization point, and might converge to poor local minima~\cite{Nocedal}.

To solve the local minima problem, one could select a best solution resulting from a set of randomly chosen candidate initial points. 
However, we propose an initialization method using tensor decompositions to guarantee smaller model errors.

\section{Finding the tensor-based initialization\label{sec_Method}}
The proposed initialization method makes use of tensor decompositions, and is created using Algorithm~\ref{algo_creation_initial_point}.
We show in the following sections that this proposed tensor-based initialization yields better results than a random initialization. 

\begin{algorithm}
\begin{itemize}
		\item Choose a number $N$ of sampling points $\u^{(1)}, \ldots, \u^{(N)}$ uniform randomly among a fixed data set;
		\item Evaluate the Jacobian matrices of \f\ in these sampling points and stack them together in a three-way tensor $\Jcal$ of dimensions $n\times m\times N$;
		\item Compute an approximate tensor decomposition of $\Jcal$ into a sum of~$\tilde r$ rank-one tensors;
		\item The different factors of this decompositions represent the initialization for the cost function~(\ref{eq_costfunction}). As in Chapter~\ref{chap_1}, the factor in the third dimension should first be interpolated and integrated, before it can represent the coefficients of the univariate functions $\tilde g_1, \ldots, \tilde g_{\tilde r}$.
	\end{itemize}
\caption{Creation of initial point for approximate decoupling.}
\label{algo_creation_initial_point}
\end{algorithm}


\section{Monte Carlo experiments and results\label{sec_Results}}
To compare the proposed tensor-based initialization with a random initialization, we proceeded with a set of Monte Carlo computer-based simulations. This section surveys the methodology (Section~\ref{sec_methodology}) and the results (Section~\ref{sec_discussion_results}).

\subsection{Methodology for comparing initializations\label{sec_methodology}}
\nobreak
\noindent As starting point for the simulations, we chose a set of 228 multiple-input multiple-output functions \f\ randomly.
These functions differ in one or multiple of the following eight ways:
\begin{enumerate}\label{page_bounds}
	\item A number of inputs $m$, where $3\le m \le 20$;
	\item A number of outputs $n$, where $3\le n \le 20$;
	\item A number $r$ of ``true'' internal branches, before the model is reduced ($3\le r \le 20$);
	\item Uniform randomly chosen univariate polynomials $g_1, \ldots, g_r$;
	\item A number $\tilde r$ of ``reduced'' internal branches, after the model reduction ($3\le \tilde r \le r$)---this number is chosen such that no identifiability issues arise~\cite{Comon2016};
	\item The maximal degree $d$ of the polynomials in the internal branches (fixed at $d = 3$);
	\item Two uniform randomly chosen transformation matrices~$\V$ and $\W$;
	\item Randomly chosen input samples $\u^{(1)},\ldots,\u^{(N)}$ ($N = 500$) among a given and fixed data set.
\end{enumerate}

Once these parameters are set, a study system is defined. 
Then, ten random initialization points are generated, and these represent all the parameters of initial parameters~$\Vt$, $\Wt$ and~$\gt$ in an approximate decoupled representation. 
Each of these ten initializations start two optimization procedures (Figure~\ref{fig_exp}):
\begin{enumerate}
	\item The initialization is passed to Algorithm~\ref{algo_creation_initial_point} (Section~\ref{sec_Method}) after which it is further optimized with the cost function~(\ref{eq_costfunction}) and a Jacobian gradient-descent method.
	\item The initialization is directly optimized using the cost function~(\ref{eq_costfunction}) and a Newton-like gradient-descent method.
\end{enumerate}
 
\begin{figure}
\centering\includegraphics[scale=0.8]{chapter4/figures/experiment_overview_update.pdf}
\caption{Two different optimization routes are proposed. The first computes the CP decomposition before continuing to optimize cost function~(\ref{eq_costfunction}). The second directly optimizes the latter cost function starting from a randomly chosen initialization.}
\label{fig_exp}
\end{figure}

At the end of one experiment, twenty results are obtained (ten for each optimization route) and each of them defines an approximate decoupled representation of \f. This approximation consists of the following information:
\begin{itemize}
\item Two transformation matrices $\Vt$ and $\Wt$;
\item A set of $\tilde r$ internal branches, polynomials $g_1(x_1), \ldots, g_{\tilde r}(x_{\tilde r})$, of degree~$d$;
\item The approximate model error given by the cost function~(\ref{eq_costfunction}).
\end{itemize}
The model errors are discussed further in the next section.

\subsection{Discussion of the model errors\label{sec_discussion_results}}
To discuss the results of the experiments in Section~\ref{sec_methodology}, we focus our attention to the minimal value of the cost function for each optimized approximate decoupled representation.
This choice comes from a practical point of view: in a system identification application, one could choose to solely work with the decoupled representation showing the smallest cost function error.
Accordingly, we have compared how the two alternatives relate to each other, by plotting the respective minimal cost values obtained after optimization (Figure~\ref{3fig_many_experiments}).
\begin{figure}[h!]
\centerline{\leavevmode\kern-2cm\includegraphics{chapter4/figures/results_update.pdf}}
\caption{All 228 experiments show a better (or equal) error after optimization using the CPD initialization instead of a random initialization. This difference is most significant when $\tilde r / r > 0.55$ ({\color{red}red crosses}), because these lie in the upper left part of the plot. This happens whenever $m$ or $n$ is quite large, as there is then more reduction possible. In case $\tilde r / r \le 0.55$ ({\color{blue}blue points}), the difference is less significant. 
The number $0.55$ is chosen such that it divides the points of this plot in two parts: most experiments in the upper-half portion of this plot satisfy $\tilde r / r > 0.55$. 
In other words, whenever the reduction in parameters is not too large, then the difference in initializations is probably high.}
\label{3fig_many_experiments} % C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\interplay_processing.m
\end{figure}

Adding the extra step with the tensor decomposition improves the resulting model (Figure~\ref{3fig_many_experiments}). 
Furthermore, this extra step does not add any significant computational time to the experiment, as CP decomposition algorithms exist with very little overhead time (decomposing a $20\times20\times1000$-tensor using~\cite{LaurentSorber2014} takes under three seconds to compute, while the optimization problem~(\ref{eq_costfunction}) may be relatively much slower). 
Also, even though we observed almost no direct influence of the number of inputs $m$ and outputs~$n$ on the final cost errors, we found better approximated models using the tensor-based initialization, in cases where $r/\tilde r$ was not too small.
Finally, in these experiments, no noise error was added, so the errors are solely due to model approximations. These errors can increase up to 50\% in the large reduction cases (so when $r/\tilde r$ is very small).

However, optimizing further with the tensor-based initialization seems to be important to always ensure smaller cost errors than the direct route: not adding this extra step does not always guarantee smaller cost function (Figure~\ref{3optimize_further}).
\begin{figure}[h!]
\centering\leavevmode\kern-2cm\includegraphics{chapter4/figures/compare_with_init.pdf}
\caption{To always guarantee smaller cost functions with the tensor-based initialization, it is necessary to optimize the cost function~
(\ref{eq_costfunction}) further. 
In short, both steps (CP decompositions followed by optimization), so the left route in Figure~\ref{fig_exp}, are necessary to create good approximate decoupled models.}
\label{3optimize_further}   % C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\interplay_processing.m
\end{figure}
Furthermore, even though existing CP decomposition implementations offer predefined initialization points \cite{Andersson2000,LaurentSorber2014}, these do not guarantee better results as when using a sample of ten random initializations, as other experiments have shown~\cite{Hollander2017}. 
Following these experiments, the choice of the initialization method for the CPD in the context of the optimization problem~(\ref{eq_costfunction}) does not seem to influence the results in a significant way. 
As a note, while it is known that finding the best rank-$\tilde r$ CP decomposition for tensors of order three or higher is in general an ill-posed problem \cite{Lim2009}, we are interested in workable approximations and we study their properties in this context.

In the following section, we focus our attention to two case systems and analyze them in detail.

\section{Case studies\label{sec_caseStudy}}
This section focuses on the analysis of two case studies. 
The parameters in the starting multivariate function~\f\ were chosen as follows:
\begin{itemize}
\item The matrix elements of \V\ and \W\ were chosen in the interval $[-1, 1]$;
\item The coefficients of $g_1, \ldots, g_{r}$ were chosen in the set $[-1,0.5] \cup [0.5,1]$.
\end{itemize}
Also, a set of 500 uniform randomly chosen inputs $\u^{(1)},\ldots,\u^{(500)}$ among a given data set were used to create the Jacobian tensor (Algorithm~\ref{algo_creation_initial_point}).

The first model \f\ was chosen to have 20 inputs and 20 outputs, and consists of the same number of internal branches. 
The second model, however, consists of more internal branches (20) than inputs and outputs (15 and 15). 
Both models were then approximately decoupled using an increasing number of branches, starting from 1 up to 20 (respectively Figure~\ref{fig_case_study1} and Figure~\ref{fig_case_study2}) and we compared the two optimization routes (Figure~\ref{fig_exp}), starting from ten random initializations.
Noteworthy, one could try a gradual reduction of the number of branches, starting with 20, and successively decreasing the number of branches to 19, 18, \ldots\ branches, each successive approximation based on the preceding one.
However, to compare every successive decoupled approximations independently of the other approximations and gain intuition and understanding on the approximation step itself, the model~\f\ was decoupled with a given number of branches $\rtilde$ (in the range 1 to 20), without considering other approximations (Figure~\ref{fig_case_study1} and~\ref{fig_case_study2}).

\begin{figure}
\centering\leavevmode\kern-2cm\includegraphics{chapter4/figures/testCase.pdf}
\caption{Starting from $\tilde r = 5$, the error using the the {\color{blue}CP-based initialization point ($\circ$)} is smaller than when using a {\color{red}randomly chosen initialization point ($\LargerCdot$)}. Moreover, the variations using ten different CP-based initializations are smaller than using ten random initializations.}
\label{fig_case_study1}
\end{figure}

\begin{figure}
\centering\leavevmode\kern-2cm\includegraphics{chapter4/figures/testCase2.pdf}
\caption{Starting from $\tilde r = 7$, the error using the the {\color{blue}CP-based initialization point ($\circ$)} is smaller than when using a {\color{red}randomly chosen initialization point ($\LargerCdot$)}. Moreover, the variations using ten different CP-based initializations are smaller than using ten random points.}
\label{fig_case_study2}
\end{figure}

In these case studies experiments, the extra CP decomposition step yields smaller cost function error, starting at a certain threshold value of $\tilde r$ ($\tilde r>5$ for case study~1 and $\tilde r>7$ for case study~2). 
Furthermore, the spread on the value of the cost function over different simulations is much smaller with the extra CP step, than with random initializations.
Also, only with the extra step is the original model found back for $\tilde r = r = 20$.
In practice, the cost function is in this case in the order of $10^{-11}$, which is not exactly machine precision.
This, however, is due to the CP algorithm, which may converge to a predefined threshold level.

The cost errors increase after the threshold of $\tilde r$ when using the direct optimization route, and can be explained as follows.
Exceeding the threshold may yield a too large search space for the optimization problem~(\ref{eq_costfunction}).
This, in turn, can lead to bad local minima when starting from random initial points, instead of using the tensor-based initialization.

\section{Conclusion\label{sec_conclusion}}
To reduce the model order of certain nonlinear model structures, we have to minimize the nonlinear and non-convex cost function~(\ref{eq_costfunction}). 
In this optimization, we can use an iterative method, that needs an initialization point. 
A randomly chosen initialization point may yield bad local minima, while first decomposing it using the Canonical Polyadic decomposition yields smaller approximation errors. 
With this extra step, the reduced models do approximate the original models better, and could be used in several applications (Chapter~\ref{chap_many_branches} and Chapter~\ref{chap_real_measurements}). 
Future work could investigate the influence of the number of inputs and outputs of the coupled function, with respect to the cost function errors. 
Also, generalizations to the X-rank decompositions could be investigated~\cite{Comon2016}.
