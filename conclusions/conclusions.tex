\chapter[Conclusions and future work]{Conclusions and \\future work\label{chap_conclusions_future_work}}
% \addcontentsline{toc}{chapter}{Conclusions and future work}
\abstracttext{%
In this final chapter of this thesis, the general contents of this work are linked to the research questions of the introduction (Section~\ref{sec_general_conclusions}).
As no research project can give answers to all possible questions, open question remain.
An overview of these open question ends this chapter (Section~\ref{sec_future_work}).
}

\section{Conclusions\label{sec_general_conclusions}}
During this thesis, we have tried to answer the research questions $Q_1$ and $Q_2$ (Chapter~\ref{chap_general_intro}).
In this section, we first discuss the proposed answers to the more specific question $Q_2$, and then continue to the answers of the more general question $Q_1$.

\begin{quotation}
	\noindent\it $Q_2$: For the three nonlinear model types described in this thesis (the block-oriented models, NARX models, and the nonlinear state-space models), how is it possible to generalize the existing tensor-based decoupling method in application contexts with real-life measured data?
\end{quotation}

Chapter~\ref{chap_1} proposed a weighted decoupling method as a generalization of the (former) state-of-the-art tensor-based decoupling method, which is based on the first-order derivative information of the coupled function~\cite{Dreesen2015}.
This weighted decoupling method incorporates the covariance information of the coupled multivariate function, resulting into a weighted tensor decomposition.
To remain in a controlled environment, the generalized weighted decoupling method was tested on various simulation data (Chapters~\ref{chap_2} and~\ref{chap_3}).
These tests showed smaller model errors with the proposed weighted decoupling method than with the original unweighted decoupling method~\cite{Hollander2016}.
However, considering a weighted decoupling is advantageous only in cases where the noise errors are larger than the model errors (Part~\ref{part_1}).

In Part~\ref{part_2} of this thesis, we considered model approximation problems, possibly inferring larger model errors than the noise errors.
For these model approximation problems, the unweighted decoupling method~\cite{Dreesen2015} was used as initialization point for a nonlinear and nonconvex cost function minimization, either for NARX models (Chapter~\ref{chap_many_branches}) or for nonlinear state-space models (Chapter~\ref{chap_real_measurements}).

In both cases, an additional polynomial constraint was considered during the decoupling process, yielding similar model errors as without the additional constraint.
Furthermore, these decoupled models were created and studied on real-life measured data, that originated from several benchmarks~\cite{Schoukens2009,MaartenSchoukens2014} and simulations~\cite{Noel2017}.

\begin{quotation}
	\noindent\it $Q_1$: How is it possible to construct a simpler nonlinear model, which approximates a given complex nonlinear model as well as possible?
\end{quotation}

Answering this general question sets the general direction during this research project, and every chapter of this thesis aims at answering it.
In particular, Chapter~\ref{chap_4} considers an already decoupled function with many decoupled branches and finds its approximations with fewer branches.
By considering approximated nonlinear decoupled models, the modeling power of nonlinear models is kept, but the complexity is reduced.


\section{Future work\label{sec_future_work}}
As with any research project, some open questions remain, which can form the basis for future work.
In this section, we formulate and discuss three open questions, and their relevance to the decoupling problem.

\begin{quotation}
	\noindent\it 1. In a noisy context, how many operating points are needed and how should they be chosen for the Jacobian-based decoupling method?
\end{quotation}
\noindent In a noiseless environment, the number of operating points needed to exactly recover the underlying decoupled representation of a given coupled function is dependent on the degree of this polynomial~\cite{Dreesen2015}.
However, in a noisy context, it is not yet clear how many operating points are needed to best find an approximate decoupled representation.

Therefore, depending on the considered cost function, we have used the following heuristic approach as a general intuition throughout this research project: the more operating points are considered, the better the decoupled model will approximate the coupled model, but the more computational intensive the decoupling process becomes.
This heuristic approach yielded good results with simulations (Chapters~\ref{chap_2} and~\ref{chap_3}) and real-life measurements (Chapters~\ref{chap_many_branches} and~\ref{chap_real_measurements}).

Furthermore, where exactly the operating points should be chosen remains rather vague.
While we have randomly selected some samples of the considered input signal of the coupled nonlinearity, perhaps a more structured choice could be used?
Accordingly, to ensure an appropriate approximated decoupled model, the operating points should be chosen in the considered region of interest.


\begin{quotation}
	\noindent\it 2. In what situation is a decoupled representation unique (except for scalings factors and interchanging decoupled branches)?
\end{quotation}
\noindent 
Even though in applications, one may be satisfied with only \emph{one} well approximated decoupled model, this question remains open.
Furthermore, in the case where several essentially non-unique decoupled representations might exist, these different decoupled model might offer additional intuitive information about the studied system, or they might lead to an extra degree of freedom, which is not yet taken into account.

Unfortunately, to our knowledge, very little to none is known about the uniqueness properties of decoupled representations on the level of the nonlinearity.
However, when considering the tensor level with the unweighted Jacobian-based tensor method~\cite{Dreesen2015}, some results are known in the noiseless case, but these are only sufficient conditions (Chapter~\ref{chap_introduction}).
Furthermore, in the noisy case, no uniqueness results have been reported, nor on the tensor level, nor on the nonlinearity level.
Studying the uniqueness properties of decoupled representations is the second proposed future work path.


\begin{quotation}
	\noindent\it 3. How can the current model instabilities be resolved?
\end{quotation}
\noindent 
When dealing with real-life measured data, instable models were found (Chapters~\ref{chap_many_branches} and~\ref{chap_real_measurements}).
As these unstable models cannot be used in practice, this instability issue should be resolved in the future.
One possible solution could be to consider other basis functions during the decoupling stage.
Particularly, functions with a bounded range might be of interest, for example sinusoidal functions or Arctan-like saturating functions, for which a coupled nonlinear state-space identification method has already been developed~\cite{Svensson2017}.
Alternatively, additional stability constraints could be considered during a final optimization after the decoupling stage.





% is a decoupled representation unique (in a non-Kruskal case)?

% when is a approximate tensor decomposition unique?



% what is the mathematical link between the two optimization problems, on functional level and on tensor level, with noise?

% where can we apply the weighted tensor decompositions in other fields?


