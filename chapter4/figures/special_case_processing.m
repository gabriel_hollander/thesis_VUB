clear all; close all;
direc = 'Z:\2016-03c WH Approximation\Case Study';
testCase = '15!20to12_4';
% testCase = '20!20to02_6';
% testCase = '17to12_1';
files = dir(fullfile(direc, testCase, '*out.mat'));
for iter = 1 : length(files)
    LogWindow(iter, length(files), 'iterr');
    temp = load(fullfile(direc, testCase, files(iter).name));
    costOptim(iter, :) = temp.costOptim;
    costRand(iter, :) = temp.costRand;
end
close;

%% Show all the 10 different starting points

clf
hold on
    h1 = plot(costOptim, 'ob', 'markersize', 3);
    h2 = plot(costRand, '.r', 'markersize', 5);
    text(20.5, 0.5, 'init 2', 'color', 'red')
    text(20.5, 0.01, 'init 1', 'color', 'blue')
    line([5 5], [0.24 0], 'color', 'black');
    line([0 4.85], [0.254 0.254], 'color', 'black');
hold off
set(gca, 'xtick', [0 5 20], 'ytick', [0 0.254 0.8], 'yticklabel', {0, 0.25, 0.8})
xlabel('$\tilde r$', 'interpreter', 'latex');
ylabel('cost');
title(['Test case 1'])

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\testCase.tex', 'width', '0.7\textwidth', 'height', '4.6cm', 'extraaxisoptions', 'ylabel style={rotate=-90}')

%% Show all the 10 different starting points (for test case 20to12_4)

clf
hold on
    h1 = plot(costOptim, 'ob', 'markersize', 3);
    h2 = plot(costRand, '.r', 'markersize', 5);
    text(20.5, 0.22, 'init 2', 'color', 'red')
    text(20.5, 0.01, 'init 1', 'color', 'blue')
    line([7 7], [0.13 0], 'color', 'black');
    line([0 6.81], [0.1407 0.1407], 'color', 'black');
hold off
set(gca, 'xlim', [0 20], 'xtick', [0 7 19], 'ytick', [0 0.1407 0.5], 'yticklabel', {0, 0.14, 0.5})
xlabel('$\tilde r$', 'interpreter', 'latex');
ylabel('cost');
title(['Test case 2'])

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\testCase2.tex', 'width', '0.7\textwidth', 'height', '4.6cm', 'extraaxisoptions', 'ylabel style={rotate=-90}')



%% 

G = temp.G;
maxx = 1;
xx = linspace(-maxx, maxx, 100);
clf; hold on
for iter = 1 : length(G)
    plot(xx, polyval(G(iter,:), xx), 'color', hsv2rgb([RandomReal([0 1], [1 1]) 1 1]))
end
hold off

for iter = 1 : length(G)
    text1 = sprintf('f(x_{%d}) &=', iter);
    text2 = strrep(sprintf('%.4f x^%d_{%d}  +', [G(iter,:); 3:-1:0; repmat(iter, 1, size(G,2))]), '+-', '-');
    disp([text1 text2])
end






