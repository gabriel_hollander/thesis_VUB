\chapter{Identification and decoupling of parallel Wiener-Hammerstein systems}\label{chap_3}

\abstracttext{%
\noindent In the field of nonlinear system identification, a parallel Wiener-Hammer\-stein model offers a good balance between modeling power and complexity~\cite{Schoukens2015}. 
To identify such a model, the following two-step method can be used: first, identify a coupled model and approximate the covariance matrix of the coupled nonlinearity. 
Then, use the proposed approximate decoupling techniques of Chapter~\ref{chap_1} to obtain a decoupled model.
The two steps of this methods have been tested in the past (\cite{SchoukensDecember10--132013} and Chapter~\ref{chap_2}), but they should be combined and tested during a total identification procedure.
\newline
This chapter gives an overview of this two-step full identification method on a computer simulated case study. Section~\ref{2sec_problem_statement} states the identification problem and Section~\ref{2sec_overview} surveys the coupled identification scheme. 
Further, Section~\ref{sec_analysis} details the identification of a computer-simulated case system, from start to finish, and Section~\ref{2sec_conclusion} concludes this chapter and proposes further work directions.\newline
}

%%%%%%%%%%%%%%%%% Problem statement 
\section{Problem statement\label{2sec_problem_statement}}
Given the input and output measurements of a dynamic system, we wish to identify it using a parallel Wiener-Hammerstein model. 
More precisely, given the measured inputs $\u=(u_1, \ldots, u_N)$ and outputs $\y=(y_1, \ldots, y_N)$ of a system, and the number of branches $r$, we wish to find linear time-invariant filters $L_1, \ldots, L_r, R_1, \ldots, R_r$ and univariate functions~$\g_1,$ \ldots, $\g_r$, the former surrounding the latter (Figure~\ref{fig_decoupled}),
\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{chapter3/figures/WH_system_rbranches.pdf}
	\caption{A parallel Wiener-Hammerstein model consists of $r$ parallel branches, each of which composed of a single-input single-output univariate function $\g_i$ surrounded by linear time-invariant filters $L_i$ and $R_i$ ($1\le i\le r$).}
	\label{fig_decoupled}
\end{figure}
such that the following cost function~(\ref{2eq_cost_function}) is minimized:
\begin{equation}
\min_{\mathrm{L}_i, f_i, \mathrm{R}_i} 
\frac{\norm{\y - \hat \y}^2}{\norm{\y}^2}, \quad i\in\{1, \ldots, r\}
\label{2eq_cost_function}
\end{equation}
In the optimization problem~(\ref{2eq_cost_function}), $\hat \y$ represents the modeled output of the parallel Wiener-Hammerstein model. 
Accordingly, the cost function's parameters are those of the model's components, also called blocks: the linear filters and the coefficients of the nonlinear functions~$f_1, \ldots, f_r$. 
Throughout this chapter, these nonlinear functions are assumed to be univariate polynomials. 

%%%%%%%%%%%%%% Overview of coupled identification
\section{Overview of the identification scheme\label{2sec_overview}}

The proposed identification scheme of parallel Wiener-Hammerstein models consists of two parts:
\begin{enumerate}
\item Identifying the linear filters and a coupled nonlinearity, yielding a coupled model (Section~\ref{sec_couplingStage}),
\item Finding a decoupled representation of the nonlinearity, yielding a decoupled model (Section~\ref{sec_decouplingStage}).
\end{enumerate}
The second part is based on the results of Chapter~\ref{chap_1}.

%%%%%%%%%%%%%%%%%%% Subsection: Overview of the coupled identification
\subsection{Overview of the coupled identification\label{sec_couplingStage}}
In the past, an algorithm was proposed for identifying parallel Wiener-Hammerstein models with a coupled nonlinearity \cite{Schoukens2015,SchoukensDecember10--132013,Schoukens2015a}. 
This algorithm uses the concept of Best Linear Approximation (BLA) \cite{Pintelon2012} and is summarized in the following algorithm:

\vskip0.5cm
\begin{algor}{Identification of parallel Wiener-Hammerstein system~\cite{Schoukens2015}}
	\begin{itemize}
		\item Estimate the BLA of the system at different operating points;
		\item Parametrize the BLA using a common denominator model;
		\item Decompose the BLA over the different linear dynamical blocks in the system;
		\item Estimate the static nonlinear function.
	\end{itemize}
	\nopagebreak 
\end{algor}

The resulting model, however, has some identifiability issues, because nonsingular matrices $\mathrm{T_L}$ and $\mathrm{T_R}$ (and their inverses) can be multiplied on both sides of the nonlinear functions. These operations change the representation of the model, but keep its output~$\hat\y$ unaltered (Figure~\ref{fig_identifiability}).
\begin{figure}[h!t]
	\centering
	\centerline{\includegraphics[width=\textwidth]{chapter3/figures/WH_system_identifiability_rbranches.pdf}}
	\caption{When identifying a parallel Wiener-Hammerstein model, identifiability issues arise~\cite{Schoukens2015a}. Multiplication with nonsingular matrices~$\mathbf{T}_\mathrm{L}$, $\mathbf{T}_\mathrm{R}$ and their inverses on both sides of the nonlinear functions~$f_1$ and $f_r$ leaves the input-output relation unchanged. However, the filters and the nonlinear functions are transformed (Figure~\ref{fig_coupled}).}
	\label{fig_identifiability}
\end{figure}

In general, when combining the input (respectively output) filters with~$\mathbf{T}_\mathrm{L}$ (respectively $\mathbf{T}_\mathrm{R}^{-1}$) and the innermost model blocks $\mathbf{T}_\mathrm{L}^{-1}$, $f_1$, \ldots, $f_r$ and $\mathbf{T}_\mathrm{R}$, the end model consists of linearly transformed input and output filters (respectively $\overbar{L_1}, \ldots, \overbar{L_r}$ and $\overbar{R_1}$, \ldots, $\overbar{R_r}$) and a so-called \emph{coupled nonlinear function} $\overline{\f}$ (Figure~\ref{fig_coupled}).
More formally, we define the following transformed filters
$$
\begin{bmatrix}
	\overbar{L_1}(z) \\
	\vdots \\
	\overbar{L_r}(z)
	\end{bmatrix}
	= 
	\mathbf{T}_L
	\begin{bmatrix}
	L_1(z) \\
	\vdots \\
	L_r(z)
	\end{bmatrix}
\qquadtext{and}
\begin{bmatrix}
	\overbar{R_1}(z) \\
	\vdots \\
	\overbar{R_r}(z)
\end{bmatrix}
	= 
	(\mathbf{T}_R^{-1})^T}
\begin{bmatrix}
	R_1(z) \\
	\vdots \\
	R_r(z)
\end{bmatrix}
$$
and the coupled function~$\overbar{\f}$ of input $\z$
$$
\f(\z) = \mathbf{T}_R
\begin{bmatrix}
	f_1\bigl(\mathbf{T}_{\mathrm{L}}^{-1}(\z) \bigr) \\
	\vdots \\
	f_r\bigl(\mathbf{T}_{\mathrm{L}}^{-1}(\z) \bigr)
\end{bmatrix}.
$$
\begin{figure}[h!t]
	\centering
	\includegraphics[width=\textwidth]{chapter3/figures/WH_coupled_system_rbranches.pdf}
	\caption{The end result of the coupled identification process of a parallel Wiener-Hammerstein model consists of a coupled nonlinear function in the middle of the model~\cite{Schoukens2012,SchoukensDecember10--132013,Schoukens2015a}.}
	\label{fig_coupled}
\end{figure}
This coupled function~$\overline{\f}$ may possess many parameters, and could offer little  intuitive or physical understanding in certain applications. 
Therefore, we wish to find an (approximate) decoupled representation of \f\ in the second part of the identification process. 

%%%%%%%%%%%%%%%%%%% Subsection: Overview of the decoupling stage
\subsection{Overview of the decoupling stage\label{sec_decouplingStage}}
\nobreak\noindent
The second step in the identification process consists of decoupling the end result of Section~\ref{sec_couplingStage}, using different available decoupling techniques. 
We will compare the unweighted decoupling~\cite{Dreesen2015} with two of the three decoupling methods described in Chapter~\ref{chap_1}, so we will discuss the three following decoupling methods based on first-order derivative information of \f:
\begin{itemize}\setlength\itemsep{1pt}
	\item Unweighted decoupling;
	\item Weighted decoupling with slice-wise weight matrix;
	\item Weighted decoupling with dense weight matrix.
\end{itemize}
To compute the weighted decoupled representation, one needs a weight matrix~$(\L^T\L)^{-1}$ (Chapter~\ref{chap_1}).
One possible choice for~$(\L^T\L)^{-1}$ is the inverse of a carefully constructred covariance matrix of the Jacobian elements during the tensor decomposition process (Chapter~\ref{chap_1}).
To compute this covariance matrix~$\L^T\L$, the covariance matrix~$\Sigmaf$ of the coefficients of~\f\ is needed and it is based on the stochastic properties of standard Least Squares estimation \cite{Ljung1999}.
This covariance matrix~$\Sigmaf$ can be approximated with the theoretical formula
$$
\Sigmaf = \sigma^2 (\mathbf{K}^T\mathbf{K})^{-1},
$$
where $\sigma^2$ is the covariance of the added noise on the output $\y$, and $\mathbf{K}$ is the regressor matrix used in the approximation of the coupled function~$\f$.

%%%%%%%%%%%%%%%%%%%% Case study
\section{Case study example identification\label{sec_analysis}}
This section focuses on a specific computer simulated case study system and identifies it using the method of Section~\ref{2sec_overview}. We start with input and output signals~$\u$ and $\y$, obtained from a known two-branch parallel Wiener-Hammerstein model, and add independent identically distributed (i.i.d.) Gaussian noise to the output, with a variance of 1\% of the noiseless output's variance (Figure~\ref{fig_coupledNoise}). 
Using the identification scheme of Section~\ref{2sec_overview}, we search back for a decoupled representation of the model.
\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{chapter3/figures/WH_system_withNoise.pdf}
	\caption{We assume the case study system for this chapter to be a two-branch Wiener-Hammerstein system, from which we simulate the output and add i.i.d. random noise.
	The variance of the added Gaussian noise is 1\% of the noiseless output.}
	\label{fig_coupledNoise}
\end{figure}

\subsection{Underlying system and signals\label{sec_real_system}}
For this case study, the linear filters have been chosen as different second-order lowpass Chebyshev filters (Figure~\ref{fig_lin_filters}). Furthermore, the nonlinear functions~$f_1$ and~$f_2$ are chosen to be third-order polynomial functions (Figure~\ref{fig_nonlins}). We note that $f_1$ contains a less pronounced nonlinearity than~$f_2$. 

The system is excited with a random-phase multisine defined in discrete time~\cite{Pintelon2012} without DC term:
$$
\u(t) = \sum_{k=1}^{F} A_k \sin(2\pi f_0 k t + \varphi_k).
$$
Here, $A_k$ is the amplitude of the $k$-th excited frequency line, chosen equal for all excited lines. 
These amplitudes are chosen such that the specified RMS values of the input is equal to a predefined value, and these are needed during the coupled model estimation stage~\cite{Schoukens2015a}.
Furthermore,~$\varphi_k$ is the phase of the $k$-th excited frequency line, drawn from a uniform distribution in the interval $[-\pi,\pi[$. 
The simulations are run using a sample frequency of $f_s = 2^{10} \,\mathrm{Hz}$ and the base frequency $f_0=1\,\mathrm{Hz}$. 
The number of excited frequencies $F$ is equal to 170, which implies that the maximum excited frequency is approximatively $f_s/6$.
Since the degree of the coupled nonlinearity is three, this ensures that all frequencies of the output are correctly captured (Nyquist theorem).
Finally, i.i.d. noise is added to the output, the standard deviation of which is chosen to be 1\% of the standard deviation of the simulated (noiseless) output data.

%%%%%%%%%%%%%%%%%%%% Coupled model
\subsection{Coupled model\label{sec_coupling}}
\nobreak
\noindent Using Section~\ref{sec_couplingStage}, a coupled model is found with four linear filters,~$\overbar{\mathrm{L}_1}$ and $\overbar{\mathrm{L}_2}$ on the left and $\overbar{\mathrm{R}_1}$ and $\overbar{\mathrm{R}_2}$ on the right of a multivariate polynomial~$\overline{\f}$ (Figure~\ref{fig_lin_filters2} and~\ref{fig_coupled_nonlin}). 
The filters look quite different from those in the original decoupled model (Figure~\ref{fig_lin_filters}), due to the presence of similarity transforms~\cite{Schoukens2012}.
Also, these are identified as fourth-order filters, as specified in the algorithm~\cite{Schoukens2015a}, and will remain so during the decoupling stage (Section~\ref{sec_decouplings}).
By multiplying with the appropriate factor and correctly correcting the nonlinearity~$\hat\f$, we normalized the DC gain of the filters to one (Figures~\ref{fig_lin_filters}--\ref{fig_bdfWeight_decoupling}), to get an easier comparison between the models
 % even though it models very accurately the underlying model (Table~\ref{tab_error}).

Noteworthy, although the underlying linear filters are all second order (Figure~\ref{fig_lin_filters}), the filters resulting from algorithm~\cite{Schoukens2015a} are all fourth order.
This is a result of an intrinsic behavior of this identification method, and happens because all the poles of the systems are divided between the left ($\overbar{\mathrm{L}_1}$ and $\overbar{\mathrm{L}_2}$) and right ($\overbar{\mathrm{R}_1}$ and~$\overbar{\mathrm{R}_2}$) filters.
Also, during the identification process, the left filters on the one hand, and the right filters on the other hand, are chosen to have the same poles, and therefore the same denominator (Figure~\ref{fig_lin_filters2}), all with four poles.
Although the results are fourth-order filters in this noisy setting, theoretically, pole-zero cancellation should occur in a noiseless setting~\cite{Schoukens2015a}, revealing the original second-order filters.



%%%%%%%%%%%%%%%%%%%%%% Decoupled models
\subsection{Decoupled models\label{sec_decouplings}}
The coupled model from Section~\ref{sec_coupling} is used as a starting point for the decoupling method (\cite{Hollander2016} and Chapter~\ref{chap_1}). This is done according to the following four steps:
\begin{enumerate}
	\item In 100 randomly chosen linearization points, chosen from the observed signals, the first order derivative information of $\overbar\f$ is stacked into a three-dimensional tensor.
	\item This three-dimensional tensor is decomposed using a (weighted) tensor decomposition. In the case of a weighted tensor decomposition, the weight matrix is constructed from the covariance matrix of the coefficients of $\overbar\f$.\label{item_wCPD}
	\item The tensor decomposition yields three factors. Two of them represent for transformation matrices and transform the linear filters into a new set of filters. The third factor represents the derivatives of the decoupled nonlinear functions evaluated in the randomly chosen linearization points (Algorithm~\ref{algor_exact_decoupling} on page~\pageref{algor_exact_decoupling}).
	\item The decoupled model, obtained from the weighted tensor decomposition, is further optimized using~\eqref{2eq_cost_function}, with respect to the measured output~\y. The cost function (\ref{2eq_cost_function}) of the optimization updates all the parameters defining the linear and nonlinear blocks of the parallel Wiener-Hammerstein model (Figure~\ref{fig_decoupled}).
\end{enumerate}

Every block of the created (weighted) decoupled models of the parallel Wiener-Hammerstein system can be compared with the original underlying system. The weighted decoupled models have been created using the following weight matrices during the tensor decomposition process:
\begin{enumerate}
	\item The first tensor decomposition uses the identity matrix as the weight matrix. 
	This boils down to the unweighted tensor decomposition technique~\cite{Dreesen2015} (Section~\ref{sec_unweighted_decoupling} and Figure~\ref{fig_noWeight_decoupling}).
	\item The second tensor decomposition uses the block-structured approximation of the covariance matrix. 
	Here, all the covariance information between the coefficients inside a same Jacobian matrix is kept, while the rest of the weight matrix is set to zero (Section~\ref{sec_sparse_approximations} and Figure~\ref{fig_bdWeight_decoupling}).
	\item The third tensor decomposition uses the full covariance matrix as a dense weight matrix (Section~\ref{sec_constructing_dense_covariance_matrix} and Figure~\ref{fig_bdfWeight_decoupling}).
\end{enumerate}






%%%%%%%%%%%%%%%%%% Comparing the results
\subsection{Comparing the results\label{results}}
To quantify the results, we validated the models by comparing their outputs with a validation signal in the same signal class for which the BLA was estimated. 
The smallest relative errors are obtained when using the block-diagonal and full weighted decoupling methods (Table~\ref{tab_error}).
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{r|l}
		Type of model 			& relative RMS of error \\
		\hline
	    coupled model 			& 0.0100 \\
	    decoupled no weight 	& 0.0080 \\ 
	    decoupled block-diag. 	& 0.0066 \\
	    decoupled full weight matrix & 0.0067 \\
	    decoupled with~\cite{Tiels2013} & 0.0309 \\
	    \end{tabular}
	\end{center}
	\caption{The smallest relative RMS errors is obtained when decoupling with a block-diagonal and full weight matrix.
	Also, the alternative tensor-based decoupling method~\cite{Tiels2013} yielded higher model errors, than the coupled model~\cite{SchoukensDecember10--132013}.}
	\label{tab_error}
\end{table}

Furthermore, we have repeated the identification procedure using~57 different  random-phase multisines as excitation signals.
Over these experiments, the full weighted decoupled representations seem to offer the smallest overall RMS errors (Figure~\ref{fig_repeated}). 
Also, the errors of the decoupled representations are smaller than the coupled one, which could imply that the coupled model is overfitting a little, as it contains extra parameters with its cross-terms. 
Finally, the proposed weighted decoupling method was compared with another tensor-based decoupling method~\cite{Tiels2013,MaartenSchoukens2014}, which showed higher model errors.
This alternative decoupling method uses tensors at its core, which increase in order in function of the degree of the coupled polynomials.
Furthermore, it has been studied without an added weight matrix, and it works only in the case of a coupled polynomial, instead of other basis functions.
\begin{figure}[ht!p]
	\centering
		\includegraphics[width=\textwidth]{"chapter3/figures/Repeated case systems/repeatedSimulations_2"} 
	\caption{When repeating 57 experiments with randomly created input excitations, the RMS errors of the decoupled representations are overall smaller than the coupled representation. The experiments are ordered by increasing error of decoupled representation.}
	\label{fig_repeated}
\end{figure}

Noteworthy, the overfitting of the coupled model would probably not be present in one of the two following cases:
\begin{enumerate}
	\item The underlying system in the simulation is of higher order, and is modeled by a coupled polynomial model of degree three. 
	In this case, the coupled model will contain cross-terms of degree three, but no monomials of higher degree, present in the underlying system.
	\item The underlying system is unknown, in the case of real-life measurements (Chapter~\ref{chap_real_measurements} in the case of nonlinear state-space models).
	In this situation, one cannot compare the monomials of the underlying model with the coupled model.
\end{enumerate}

Finally, the decoupled representation reduced the number of nonlinear parameters significantly: the coupled model contains seven nonlinear coefficients, while the decoupled models contain each contain four nonlinear coefficients.
This parameter reduction is even more striking with more complex models (Chapter~\ref{chap_4}).


%%%%%%%%%%%%%%%%%%%%%%% Conclusion
\section{Conclusion\label{2sec_conclusion}}
Using the two-step approach of this chapter, we have been able to identify a parallel Wiener-Hammerstein model, starting with simulated input and output data.
Using the block-diagonal and full weights during the decoupling stage results in the smallest model errors.
Using these weight matrices, we keep track of the internal correlations, which can better model the system.

Instead of modeling a system based on simulated data, the current results could be applied to real-life measurements.
This application is discussed in Chapter~\ref{chap_real_measurements} for another model structure: the nonlinear state-space models.

For general applications of the proposed method of Part~\ref{part_1}, the following rules of thumb can be given:
\begin{itemize}
	\item If a good approximation of the covariance matrix of the coefficients of the coupled function could be found, then it is worthwhile to try out the proposed weighted decoupling method with slice-wise or full weight.
	Furthermore, a large enough set of inputs (and outputs) in the range of interest should be tested, to minimize possible individual differences due to randomized noise.
	\item If no good approximation of the covariance matrix can be found, then the unweighted decoupled method~\cite{Dreesen2015} can always be tested.
	In some cases, depending on the uncertainty of the approximated covariance matrix, the proposed element-wise weighted decoupling method can also be tested.
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%% Figures
\begin{figure}[p!]
Underlying system's details:
	\begin{center}
		\includegraphics{chapter3/figures/underlying_system/magnLeft.pdf}
		\includegraphics{chapter3/figures/underlying_system/phaseLeft.pdf}
	\end{center}
		\leavevmode\kern1.9cm\begin{tabular}{l}
		$\color{red}\mathrm{L}_1$: $\displaystyle\frac{1.2\, 10^{-1} z^2+2.4\, 10^{-1} z+1.2\, 10^{-1}}{1.0 z^2-1.1 z+5.7\, 10^{-1}}$ \\[10pt]
		$\color{blue}\mathrm{L}_2$: $\displaystyle\frac{8.0\, 10^{-2} z^2+1.6\, 10^{-1} z+8.0\, 10^{-2}}{1.0 z^2-1.4 z+7.1\, 10^{-1}}$
	  \end{tabular}
\vskip1.5cm
		\begin{center}
		\includegraphics{chapter3/figures/underlying_system/magnRight.pdf} 
		\includegraphics{chapter3/figures/underlying_system/phaseRight.pdf}
		\end{center}
		\leavevmode\kern1.9cm\begin{tabular}{l}
		$\color{green}\mathrm{R}_1$: $\displaystyle\frac{4.8\, 10^{-1} z^2-7.3\, 10^{-1} z+4.8\, 10^{-1}}{1.0 z^2-1.4 z+6.2\, 10^{-1}}$ \\[10pt]
		$\color{black}\mathrm{R}_2$: $\displaystyle\frac{8.5\, 10^{-3} z^2+5.7\, 10^{-3} z+8.5\, 10^{-3}}{1.0 z^2-1.8 z+8.0\, 10^{-1}}$
		\end{tabular}
	\caption{The linear filters of the underlying system are chosen to be  second-order Chebychev filters.}
	\label{fig_lin_filters}
\end{figure}

\begin{figure}[p!]
	\centering
	\includegraphics[height=5.5cm]{chapter3/figures/underlying_system/nonlins.pdf}
		${\color{red}f_1(x_1)} = 3.55\, 10^{-3} x_1^3+2.51\, 10^{-2} x_1^2+7.08\, 10^{-1} x_1$
		\par\medskip
		${\color{blue}f_1(x_2)} = 1.78\, 10^{-2} x_2^3+3.16\, 10^{-3} x_2^2+5.62\, 10^{-1} x_2$
	\caption{The nonlinear functions $f_1$ and $f_2$ in the underlying system are chosen to be third-order polynomial functions.}
	\label{fig_nonlins}
\end{figure}


\begin{figure}[p]
Coupled systems's details:
		\begin{center}
		\includegraphics{chapter3/figures/coupled_model/magnLeft.pdf}
		\includegraphics{chapter3/figures/coupled_model/phaseLeft.pdf}
		\end{center}
		\leavevmode\kern1.7cm\begin{tabular}{l}
			$\color{red}\overbar{\mathrm{L}_1}$: $\displaystyle\frac{1.1\, 10^{-1} z^4+7.9\, 10^{-2} z^3-1.2\, 10^{-1} z^2+6.7\, 10^{-3} z+7.9\, 10^{-2}}{1.0 z^4-2.5 z^3+2.8 z^2-1.6 z+4.0\, 10^{-1}}$ \\[10pt]
			$\color{blue}\overbar{\mathrm{L}_2}$: $\displaystyle\frac{-4.2\, 10^{-2} z^4+4.2\, 10^{-2} z^3-2.6\, 10^{-1} z^2+1.1\, 10^{-1} z-1.2\, 10^{-3}}{1.0 z^4-2.5 z^3+2.8 z^2-1.6 z+4.0\, 10^{-1}}$
		\end{tabular}
		\vskip1.5cm
		\begin{center}
		\includegraphics{chapter3/figures/coupled_model/magnRight.pdf} 
		\includegraphics{chapter3/figures/coupled_model/phaseRight.pdf}
		\end{center}	
		\leavevmode\kern1.7cm\begin{tabular}{l}
		$\color{green}\overbar{\mathrm{R}_1}$: $\displaystyle\frac{4.5\, 10^{-1} z^4-1.5 z^3+2.0 z^2-1.4 z+3.6\, 10^{-1}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$ \\[10pt]
			$\color{black}\overbar{\mathrm{R}_2}$: $\displaystyle\frac{2.8\, 10^{-2} z^4-7.1\, 10^{-2} z^3+9.5\, 10^{-2} z^2-6.8\, 10^{-2} z+2.1\, 10^{-2}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$
			\end{tabular}
	\caption{The linear filters after the first part of the identification method already ressemble the filters of the underlying model. These filters surround the coupled nonlinearity~$\overbar\f$ (Figure~\ref{fig_coupled_nonlin}).}
	\label{fig_lin_filters2}
\end{figure}

\begin{figure}[p]
	\centering
		\includegraphics{chapter3/figures/coupled_model/coupledfunction1.pdf}
			$$\begin{aligned}
			\overbar{f_1}(u_1, u_2) &= 
			 -1.4\, 10^{-3} + 7.9\, 10^{-1}u_1 + 5.6\, 10^{-2}u_2 \\
			 &\phantom{=}+ 3.1\, 10^{-2}u_1^2 + 4.3\, 10^{-3}u_1u_2 + 2.4\, 10^{-4}u_2^2 + 4.4\, 10^{-3}u_1^3 \\
			 &\phantom{=}+ 1.3\, 10^{-3}u_1^2u_2 + -1.6\, 10^{-4}u_1u_2^2 + -6.1\, 10^{-6}u_2^3
			 \end{aligned}$$
			 		\vskip1cm
		\includegraphics{chapter3/figures/coupled_model/coupledfunction2.pdf}
			$$\begin{aligned}		
			\overbar{f_2}(u_1, u_2) &= 
				0.0 + 3.8\, 10^{-1}u_1 + -1.5\, 10^{-1}u_2 + -4.1\, 10^{-4}u_1^2 \\
				 &\phantom{=} + -1.6\, 10^{-3}u_1u_2 + 2.7\, 10^{-4}u_2^2 + 5.9\, 10^{-3}u_1^3 \\
				 &\phantom{=} -8.7\, 10^{-3}u_1^2u_2 + 3.4\, 10^{-3}u_1u_2^2 + -3.7\, 10^{-4}u_2^3
			 \end{aligned}$$
	\caption{After the first part of the identification process, the coupled multivariate polynomial~$\overline{\f}$ is modeled. In order to compare it with the underlying model, it should be decoupled (Figures~\ref{fig_noWeight_decoupling}---\ref{fig_bdfWeight_decoupling}).}
	\label{fig_coupled_nonlin}
\end{figure}

\begin{figure}[p!]
Unweighted decoupled model's details:
	\begin{center}
		\includegraphics{"chapter3/figures/decoupling noWeight/magnLeft"} 
		\includegraphics{"chapter3/figures/decoupling noWeight/phaseLeft"}
		\end{center}
			\leavevmode\kern1.7cm\begin{tabular}{l}
	$\color{red}\mathrm{L_1}$: $\displaystyle\frac{1.7\, 10^{-5} z^4+1.2\, 10^{-5} z^3-1.8\, 10^{-5} z^2+4.8\, 10^{-7} z+1.1\, 10^{-5}}{1.5\, 10^{-4} z^4-3.7\, 10^{-4} z^3+4.1\, 10^{-4} z^2-2.2\, 10^{-4} z+5.7\, 10^{-5}}$ \\[10pt]
	$\color{blue}\mathrm{L_2}$: $\displaystyle\frac{3.1\, 10^{-5} z^4-7.0\, 10^{-6} z^3-2.4\, 10^{-5} z^2+3.5\, 10^{-5} z-1.2\, 10^{-5}}{1.5\, 10^{-4} z^4-3.7\, 10^{-4} z^3+4.1\, 10^{-4} z^2-2.2\, 10^{-4} z+5.7\, 10^{-5}}$
			\end{tabular}
\vskip1.5cm
	\begin{center}
		\includegraphics{"chapter3/figures/decoupling noWeight/magnRight"}
		\includegraphics{"chapter3/figures/decoupling noWeight/phaseRight"}
	\end{center}
	\leavevmode\kern1.7cm\begin{tabular}{l}
			$\color{green}\mathrm{R_1}$: $\displaystyle\frac{2.9\, 10^{-1} z^4-9.5\, 10^{-1} z^3+1.3 z^2-8.7\, 10^{-1} z+2.3\, 10^{-1}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$ \\[10pt]
			$\color{black}\mathrm{R_2}$: $\displaystyle\frac{-1.3\, 10^{-2} z^4+5.6\, 10^{-2} z^3-6.6\, 10^{-2} z^2+3.4\, 10^{-2} z-5.3\, 10^{-3}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$
	\end{tabular}
\end{figure}

\begin{figure}[p!]
		\centering
		\includegraphics{"chapter3/figures/decoupling noWeight/nonlins"}
		${\color{red}f_1(x_1)} = 6.69\, 10^{-3} x_1^3+4.22\, 10^{-2} x_1^2+1.27\,  x_1-1.08\, 10^{-3}$
		\par\medskip
		${\color{blue}f_2(x_2)} = 1.32\, 10^{-2} x_2^3-8.70\, 10^{-3} x_2^2-1.25\, 10^{-6} x_2 \kern1cm$
	\caption{When decoupling the coupled model without any weight, the resulting linear filters (left) ressemble to the underlying filters, but the nonlinearities (up) have room for improvement.}
	\label{fig_noWeight_decoupling}
\end{figure}


\begin{figure}[hp!]
Block-diagonal weighted decoupled model's details:
	\begin{center}
		\includegraphics{"chapter3/figures/decoupling bdWeight/magnLeft"} 
		\includegraphics{"chapter3/figures/decoupling bdWeight/phaseLeft"}
	\end{center}
	\leavevmode\kern1.7cm\begin{tabular}{l}
			$\color{red}\mathrm{L_1}$: $\displaystyle\frac{1.2\, 10^{-1} z^4+7.5\, 10^{-2} z^3-1.5\, 10^{-1} z^2+2.8\, 10^{-2} z+8.1\, 10^{-2}}{9.7\, 10^{-1} z^4-2.5 z^3+2.8 z^2-1.6 z+4.2\, 10^{-1}}$ \\[10pt]
			$\color{blue}\mathrm{L_2}$: $\displaystyle\frac{2.1\, 10^{-1} z^4-2.3\, 10^{-1} z^3+2.4\, 10^{-1} z^2-1.6\, 10^{-1} z+8.8\, 10^{-2}}{9.7\, 10^{-1} z^4-2.5 z^3+2.8 z^2-1.6 z+4.2\, 10^{-1}}$
		\end{tabular}
\vskip1.5cm
	\begin{center}
		\includegraphics{"chapter3/figures/decoupling bdWeight/magnRight"}
		\includegraphics{"chapter3/figures/decoupling bdWeight/phaseRight"}
	\end{center}
	\leavevmode\kern1.7cm\begin{tabular}{l}
			$\color{green}\mathrm{R_1}$: $\displaystyle\frac{4.9\, 10^{-1} z^4-1.6 z^3+2.2 z^2-1.5 z+3.9\, 10^{-1}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$ \\[10pt]
			$\color{black}\mathrm{R_2}$: $\displaystyle\frac{-1.5\, 10^{-3} z^4+1.8\, 10^{-2} z^3-1.7\, 10^{-2} z^2+1.8\, 10^{-3} z+3.7\, 10^{-3}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$
			\end{tabular}
\end{figure}

\begin{figure}[p!]
	\centering
		\includegraphics{"chapter3/figures/decoupling bdWeight/nonlins"}
		${\color{red}f_1(x_1)} = 3.77\, 10^{-3} x_1^3+2.46\, 10^{-2} x_1^2+7.03\, 10^{-1} x_1-2.95\, 10^{-4}$
		\par\medskip
		${\color{blue}f_2(x_2)} = 1.78\, 10^{-2} x_2^3+3.30\, 10^{-3} x_2^2+5.67\, 10^{-1} x_2+5.33\, 10^{-4}$
	\caption{When decoupling using the block-diagonal weight matrix, the resulting model approximates well the underlying system (linear filters left; decoupled polynomials up).}
	\label{fig_bdWeight_decoupling}
\end{figure}

\begin{figure}[hp!]
Full weighted decoupled model's details:
	\begin{center}
		\includegraphics{"chapter3/figures/decoupling bdfWeight/magnLeft"} 
		\includegraphics{"chapter3/figures/decoupling bdfWeight/phaseLeft"}
	\end{center}
		\leavevmode\kern1.7cm\begin{tabular}{l}
			$\color{red}\mathrm{L_1}$: $\displaystyle\frac{1.2\, 10^{-1} z^4+7.5\, 10^{-2} z^3-1.5\, 10^{-1} z^2+2.8\, 10^{-2} z+8.1\, 10^{-2}}{9.7\, 10^{-1} z^4-2.5 z^3+2.8 z^2-1.6 z+4.2\, 10^{-1}}$ \\[10pt]
			$\color{blue}\mathrm{L_2}$: $\displaystyle\frac{2.1\, 10^{-1} z^4-2.3\, 10^{-1} z^3+2.4\, 10^{-1} z^2-1.6\, 10^{-1} z+8.8\, 10^{-2}}{9.7\, 10^{-1} z^4-2.5 z^3+2.8 z^2-1.6 z+4.2\, 10^{-1}}$
		\end{tabular}
\vskip1.5cm
	\begin{center}
		\includegraphics{"chapter3/figures/decoupling bdfWeight/magnRight"}
		\includegraphics{"chapter3/figures/decoupling bdfWeight/phaseRight"}
	\end{center}
		\leavevmode\kern1.7cm\begin{tabular}{l}
			$\color{green}\mathrm{R_1}$: $\displaystyle\frac{4.9\, 10^{-1} z^4-1.6 z^3+2.2 z^2-1.5 z+3.9\, 10^{-1}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$ \\[10pt]
			$\color{black}\mathrm{R_2}$: $\displaystyle\frac{-1.5\, 10^{-3} z^4+1.8\, 10^{-2} z^3-1.7\, 10^{-2} z^2+1.8\, 10^{-3} z+3.7\, 10^{-3}}{1.0 z^4-3.2 z^3+3.9 z^2-2.2 z+4.9\, 10^{-1}}$
		\end{tabular}
\end{figure}

\begin{figure}[p!]
		\centering
		\includegraphics{"chapter3/figures/decoupling bdfWeight/nonlins"}
		${\color{red}f_1(x_1)} = 3.77\, 10^{-3} x_1^3+2.46\, 10^{-2} x_1^2+7.03\, 10^{-1} x_1-9.74\, 10^{-4}$
		\par\medskip
		${\color{blue}f_2(x_2)} = 1.78\, 10^{-2} x_2^3+3.30\, 10^{-3} x_2^2+5.67\, 10^{-1} x_2+1.21\, 10^{-3}$
	\caption{When decoupling using the full weight matrix, the resulting model approximates well the underlying system (linear filters left; decoupled polynomials up). This resulting model is very similar than the one using a block-diagonal weight matrix (Figure~\ref{fig_bdWeight_decoupling}).}
	\label{fig_bdfWeight_decoupling}
\end{figure}
\afterpage{\clearpage}
