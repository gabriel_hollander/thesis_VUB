\documentclass{article}
\usepackage[a4paper,top=1in,left=1in,right=1.5in,bottom=1in]{geometry}
\usepackage{url}
\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{definedvariables}
\DeclarePairedDelimiter{\floor}{\lfloor}{\rfloor}
\usepackage{dsfont}
\usepackage[parfill]{parskip}
\usepackage{xcolor}
\usepackage[margin=2cm]{caption}
\captionsetup[figure]{font=small,labelfont=small}
\linespread{1.2}
\newenvironment{answer}{\newline\noindent\ignorespaces\color{blue}}{\noindent\ignorespacesafterend}
\newcommand{\ans}[1]{\begin{answer}#1\end{answer}}
\newcommand{\action}[1]{\textbf{Action:} #1}
\newcommand{\oldnewpage}[2]{\marginpar{{\color{black}#1} / {\color{red}#2}}}

\begin{document}

\sloppy

\section*{Response to the jury and changes to the text}

Dear jury members,

Thank you for your feedback on my PhD thesis. 
Our discussion during my private defense helped me to improve the text.
Below, I have attempted to address your questions, and I have indicated the changes made in the text.
\oldnewpage{start page old text}{start page new text}The page numbers in the margin indicate the start page numbers in the previous text (black) and in the new text (red).

\subsection*{Requested changes}
\subsubsection*{Main changes}
\begin{itemize}

\item The idea of adding polynomial constraints (Chapter 8) is one of the main contributions of the thesis. This should be more explicitly stated in the introduction (Section 1.4).
\ans{\oldnewpage{7}{7}%
I have added this contribution to the list of contributions in this thesis (Section~1.4) the following extra bullet:

{\color{red} Adding a polynomial constraint during the tensor decomposition, imposing a polynomial structure (Chapters~7 and~8).}
}

\item The problem statement (3.5) on page 37 is not clear and should be reformulated. In particular, the phrase `for certain validation signal' is not mathematical enough. The input is a set of vectors, which is not clear from the formulation. 

More importantly, it should be clarified why different cost functions are considered, which one is in fact optimized and what is the difference between it and the desired one. Which cost function does the available software optimize?
\ans{\oldnewpage{37}{39}%
In Section~3.2, we are considering the decoupling problem for ``noisy'' multivariate coupled polynomials, i.e., polynomials with no exact decoupled representation.
For these noisy coupled polynomials, we wish to find the best approximate decoupled representation, where the approximation is made using the norm on the function outputs.

More detailed, the starting point for the problem in this section is an underlying multivariate polynomial vector function $\mathbf{f}_0 : \mathbb{R}^m \to \mathbb{R}^n$, having an exact decoupled representation $\mathbf{f}_0(\mathbf{u}) = \mathbf{W}_0 \, \mathbf{g}_0(\mathbf{V}_0^T \mathbf{u})$,
with certain transformation matrices $\V_0\in\ER^{m\times r}$, $\W_0\in\ER^{n\times r}$ and with a set of~$r$ univariate scalar functions $\g_0(\x) = [g_{01}(x_1), \ldots, g_{0r}(x_r)]^T$ in the components~$x_i$ of $\mathbf{x} = \mathbf{V}_0^T \mathbf{u}$.
The coefficients of $\f_0$ are noted as $\c_0$, so the output of $\f_0$ can be written as
$$
\f_0(\u) = \B(\u) \c_0,
$$
where $\B(\u)$ is a block-Vandermonde matrix containing monomials in the components of~$\u$.
To these coefficients~$\c_0$ of the underlying function~$\f_0$, correlated noise is added to create the coefficients~$\c$ of the ``noisy'' function~$\f$:
$$
\mathbf{c} = \c_0 + \mathbf{N} \mathbf{e},
$$
where $\mathbf{e}$ is normally distributed i.i.d.\ noise with zero mean and unit variance, and $\mathbf{N}\mathbf{N}^T = \mathbf{\Sigma}_\mathbf{c}$ is a given covariance matrix associated with the noisy coefficients $\mathbf{c}$.

Even though not possible in general, because the access to $\f_0$ is not given, we wish to retrieve the original decoupled representation $\mathbf{f}_0(\u)=\mathbf{W}_0 \, \mathbf{g}_0(\mathbf{V}_0^T \mathbf{u})$ from the coupled function~$\f$.
However, instead we consider the weighted optimization problem
\begin{equation}
\underset{\V,\W, \g}{\minimize} \sum_{k=1}^N \norm{\f(\u^{(k)}) - \W\g( \V^T \u^{(k)})}^2_{\Sigmaf(\u^{(k)})^{-1}},
\label{eq_cost_with_weight1}
\end{equation}
Note that this covariance matrix~$\Sigmaf$ can be related to the covariance matrix $\Sigmac$.
Observe that the function evaluation $\mathbf{f}(\u)$ is linear in the coefficients $\mathbf{c}$: the function evaluation can be expressed as $\mathbf{f} (\u) = \B(\u) \mathbf{c}$, where $\B(\u)$ is a block-Vandermonde matrix containing monomials in the components of $\u$.
Then the covariance matrix $\Sigmaf$ is related to $\mathbf{\Sigma}_\mathbf{c}$ through $\Sigmaf(\u) = \B(\u) \Sigmac \B(\u)^T$. 

Unfortunately, even the cost function~\eqref{eq_cost_with_weight1} may be too hard to minimize, due to local minima. 
Therefore, we propose to consider the following extension of the first-order derivative method~\cite{Dreesen2015}:
\begin{equation}
\underset{\V,\W, \g}{\minimize} \sum_{k=1}^N \norm{\J(\u^{(k)}) - \W \operatorname{diag}(\g'( \V^T \u^{(k)})) \mathbf{V}^T}^2_{\SigmaJ(\u^{(k)})^{-1}},
\label{eq_jacwithweight1}
\end{equation}
where a covariance matrix $\SigmaJ(\u^{(k)})$ on the Jacobian evaluation is considered.
Accordingly, the results of the optimization problem~\eqref{eq_jacwithweight1} can be used as initial values for the original optimization problem~\eqref{eq_cost_with_weight1}, which is the decoupling problem on the level of the nonlinear function~$\f$.

In the designed software, and in the proposed method, it is the cost function~\eqref{eq_jacwithweight} which is optimized, and for which we have considered the so-called \emph{weighted Alternating Least Squares} method. 
In my thesis, starting on page~37, the following text has been added:

{\color{red}  The decomposition task for noisy polynomials can be stated as follows. 
We assume that an underlying multivariate polynomial vector function $\mathbf{f}_0 : \mathbb{R}^m \to \mathbb{R}^n$
 exists, having a decoupled representation $\mathbf{f}_0(\mathbf{u}) = \mathbf{W}_0 \, \mathbf{g}_0(\mathbf{V}_0^T \mathbf{u})$,
with certain transformation matrices $\V_0\in\ER^{m\times r}$, $\W_0\in\ER^{n\times r}$ and with a set of $r$ univariate scalar functions $\g_0(\x) = [g_{01}(x_1), \ldots, g_{0r}(x_r)]^T$ in the components~$x_i$ of $\mathbf{x} = \mathbf{V}_0^T \mathbf{u}$.

The decoupling problem entails finding, from the given function description, its decoupled representation, i.e., we wish to find the matrices~$\mathbf{V}_0$, $\mathbf{W}_0$ and the $r$ univariate scalar functions $g_{0i}$.  

In the current chapter, we consider the decoupling problem in an approximate and noisy setting. 
In particular, we study the case where the coefficients~$\c$ of the function~\f\ are given in function of the coefficients~$\c_0$ of the function~$\f_0$:
\begin{equation}
\mathbf{c} = \c_0 + \mathbf{N} \mathbf{e},
\label{eq_noise_added_on_coeff}	
\end{equation}
where $\mathbf{e}$ is normally distributed i.i.d.\ noise with zero mean and unit variance, and $\mathbf{N}\mathbf{N}^T = \mathbf{\Sigma}_\mathbf{c}$ is a given covariance matrix associated with the noisy coefficients $\mathbf{c}$.
It is with respect to the noise added in Equation~\eqref{eq_noise_added_on_coeff} that the coupled function~\f\ is called ``noisy'' throughout this thesis.
Note that the assumption of having access to $\mathbf{\Sigma}_\mathbf{c}$ is reasonable, for example, system identification procedures typically return both a parameter vector as well as its covariance matrix~\cite{Ljung1999}. 

From the given `noisy' parameters $\mathbf{c}$ and the covariance matrix $\mathbf{\Sigma}_\mathbf{c}$, one would ideally desire to retrieve the original decoupled representation $\mathbf{f}_0=\mathbf{W}_0 \, \mathbf{g}_0(\mathbf{V}_0^T \mathbf{u})$. 
Due to the noisy nature of the given coefficients, this is however, not possible in general. 
It is meaningful to consider a weighted least-squares cost function in the outputs of the function as 
\begin{equation}
\underset{\V,\W, \g}{\minimize} \sum_{k=1}^N \norm{\f(\u^{(k)}) - \W\g( \V^T \u^{(k)})}^2_{\Sigmaf(\u^{(k)})^{-1}}.
\label{eq_cost_with_weight}
\end{equation}
where the inverse covariance matrix of the outputs is used as a weighting matrix.
Note that this covariance matrix can be related to the covariance matrix $\Sigmac$.
Observe that the function evaluation $\mathbf{f}(\u)$ is linear in the coefficients $\mathbf{c}$: the function evaluation can be expressed as $\mathbf{f} (\u) = \B(\u) \mathbf{c}$, where $\B(\u)$ is a block-Vandermonde matrix containing monomials in the components of $\u$.
Then the covariance matrix $\Sigmac(\u)$ is related to $\mathbf{\Sigma}_\mathbf{c}$ through $\Sigmaf(\u) = \B(\u) \Sigmac \B(\u)^T$. 

In this chapter, we develop an extension of the first-order method \cite{Dreesen2015}, which tackles the decoupling task by performing a tensor-based decomposition method on the evaluations of the Jacobian of the function $\mathbf{f}$ instead of the function values. 
This can be represented in terms of a cost function 
\begin{equation}
\underset{\V,\W, \g}{\minimize} \sum_{k=1}^N \norm{\J(\u^{(k)}) - \W \operatorname{diag}(\g'( \V^T \u^{(k)})) \mathbf{V}^T}^2_{\SigmaJ(\u^{(k)})^{-1}},
\label{eq_jacwithweight}
\end{equation}
where a covariance matrix $\SigmaJ(\u^{(k)})$ on the Jacobian evaluation is considered. 
In this way, the task reduces to solving a tensor canonical polyadic decomposition, and can be solved using an alternating least squares (ALS) approach.
In our approximate setting, this allows for incorporation of the inverse covariance weighting in the ALS iterations. 

The choice of developing the first-order approach is three-fold.
Firstly, in the exact case, the first-order approach is equivalent to a cost function involving the function evaluations, or a cost function involving the exact coefficients.  
Secondly, the cost function in terms of the function evaluations~(\ref{eq_cost_with_weight}) can only be solved using gradient descent-like nonlinear optimization, and is prone to local minima.  
Thirdly, the first-order approach provides adequate solutions (or initialization points for further optimization) in extensive numerical simulations.}
}

\item In the algorithmic part of the thesis, it is meant that the noise is added on the coefficients of the given polynomial. However, in the simulations, the noise is on the output of the system, which propagates to the coefficients of the polynomial in a nonlinear way. Are we still in the framework of maximum likelihood?
\ans{\oldnewpage{63}{65}%
When considering a finite number of output samples during the approximation of the coefficients of the coupled polynomial function, their distribution is not Gaussian and therefore, we are outside the scope of maximum likelihood using a least-squares cost function.
However, as the coefficient distribution is asymptotic Gaussian, for increasing numbers of sample points, we assumed in Section~4.4 that we are still in the framework of maximum likelihood.
Accordingly, we solely consider the first and second moments in the covariances, instead of higher-order moments.

In my thesis, the following text has been added:

{\color{red} Noteworthy, the assumption of having Gaussian distributed coefficients of~\f\ is asymptotically satisfied for large numbers of samples.
In such an asymptotic case, the proposed weighted decoupled methods (Section~3.4) lies in the framework of maximum likelihood.
Within the current section of system identification, the same assumption of Gaussian distribution is made on the coefficients of~\f.}
}

\item It would be nice to have more structured observations from the simulations, that do not depend so much on the particular setup.
\ans{\oldnewpage{81}{83}%
To give general structured observations, we propose two rules of thumb at the end of the two Parts of this thesis.
These following rule of thumbs were added in the text:

{\color{red} For general applications of the proposed method of Part~I, the following rule of thumb can be given:
\begin{itemize}
	\item If a good approximation of the covariance matrix of the coefficients of the coupled function could be found, then it is worthwhile to try out the proposed weighted decoupling method with slice-wise or full weight.
	Furthermore, a large enough set of inputs (and outputs) in the range of interest should be tested, to minimize possible individual differences due to randomized noise.
	\item If no good approximation of the covariance matrix can be found, then the unweighted decoupled method~\cite{Dreesen2015} can always be tested.
	In some cases, depending on the uncertainty of the approximated covariance matrix, the proposed element-wise weighted decoupling method can also be tested.
\end{itemize}
}
\oldnewpage{147}{149}%
{\color{red} As general rules of thumbs for the results of Part~II, the following proposed rules can be given:
\begin{itemize}
	\item When modeling real-life measurements or computer simulations with NARX or nonlinear state-space models, the coupled models can be decoupled starting with one branch.
	Consequently, the number of decoupled branches can be increased until the output error is below a user defined level, depending on every application.
	\item When decoupling NARX or nonlinear state-space models, the unconstrained decoupling method~\cite{Dreesen2015} can be compared to the proposed constrained decoupling method (Section~8.2.3).
	Accordingly, the best result can be used as decoupled model representation.
\end{itemize}
}
}

\item A unified view of the performed research is missing. It would be nice to have more links between the different parts.
\ans{\oldnewpage{3}{3}%
I have tried my best to give a unified view of my research in Chapter 1 (Introduction) of my thesis. 
More detailed, the two central (and general) questions are described in Section 1.2 (page 3), while Section 1.3 (page 4) gives a broad overview of all the different Chapters and their research subjects. 
Finally, a visual and unified representation of the links of the different parts of my thesis is given on page 6 (Figure 1).
\begin{figure}[th!]
	\centerline{\includegraphics[width=0.8\textwidth]{figures/summary_chapters.pdf}}
	\caption{Unified view of the performed research during my PhD project.}
\end{figure}
}

\item As software is an important part of research nowadays, it should be made explicitly clear which parts of the Matlab code related to the experiments in the thesis are publicly available
and where.
\ans{\oldnewpage{44}{47}%
The following statement has been added to the thesis:

{\color{red} Noteworthy, the Matlab software described in this section is available at \url{https://gitlab.com/gabriel_hollander/decoupling-website}.}
}

\end{itemize}

\subsubsection*{Additional changes}
\begin{itemize}
\item  The sentence ``In this thesis, we focus our attention to the subfield of nonlinear dynamical system identification, which considers mathematical models which underlying equations are nonlinear in the parameters.'' (page 2) should be reformulated. The phrase ``nonlinear in the parameters'' is inaccurate.
\ans{\oldnewpage{2}{2}%
Indeed, the models I have considered are nonlinear in the inputs, but they are linear in the parameters, as I have considered polynomials as basis functions.
I thus corrected the sentence in the thesis as follows: 

{\color{red} In this thesis, we focus our attention to the subfield of nonlinear dynamical system identification that considers mathematical models, in which the underlying equations are nonlinear in the inputs}.
}

\item  Unlike matrices, some tensor decompositions have uniqueness properties, which are quite useful for system identification. This should be clarified in the overview of tensors (Section 2.1).
\ans{\oldnewpage{15}{16}%
The following text has been added to the end of Section~2.1:

{\color{red}
Noteworthy, unlike multi-dimensional tensors, matrices do not contain uniqueness properties, without any additional constraint.
For example, when computing the singular value decomposition of a matrix, the decomposition is unique if the left and right singular vectors are mutually orthonormal, and if the middle diagonal matrix gives an order to the singular values it contains.
However, these additional constraints may not be wished when working with applications, where these constraints may not be present.
Therefore, considering multi-dimensional tensors and their decompositions has received much attention in several application disciplines, among which nonlinear system identification.
}
}

\item  It is assumed throughout the thesis that the reader is familiar with the alternating least squares method (ALS). This is, however, not always the case and a description of this method should be provided.
\ans{\oldnewpage{15}{15}%
The following algorithm has been added in the Preliminaries:

{\color{red}
\begin{algor}{Alternating Least Squares scheme for computing the CPD}\label{algor_als_scheme}
Consider the three-dimensional tensor~$\mathcal{A}$, for which a representation as a sum of rank-one tensors is searched for.
The Alternating Least Squares scheme for computing the CPD~$\cpdgen\A\B\C$ of the tensor~$\mathcal{A}$ is given as follows:
\begin{enumerate}
	\item Start with initial estimates for the factors $\A$, $\B$ and $\C$.
	These can be chosen randomly, except if better estimates are available.
	\item Solve the equation $\mathcal{A}_{(1)} = \A (\C \odot \B)^T$ for the factor $\A$, by keeping the two other factors $\B$ and $\C$ constant.
	\item Solve the equation $\mathcal{A}_{(2)} = \B (\C \odot \A)^T$ for the factor $\B$, by keeping the two other factors $\A$ and $\C$ constant.
	\item Solve the equation $\mathcal{A}_{(3)} = \C (\B \odot \A)^T$ for the factor $\C$, keeping the two other factors $\A$ and $\B$ constant.
	\item Repeat steps 2--4 until a stopping criterion is satisfied, for example: the relative change of the successive factors lies under a given threshold, or a given maximum number of iterations is reached.
	\end{enumerate}
\end{algor}
}
}

\item  It should be clarified that Fig. 2.5 (page 18) is not meant as a Venn diagram.
\ans{\oldnewpage{18}{19}%
In the updated version of the thesis, the following text was added at the end of the figure caption:

{\color{red} Noteworthy, the representation of the different nonlinear models in this figure should not be interpreted as disjunct Venn diagrams, as, for example, any NARX model could be rewritten as a nonlinear state-space model.}
}

\item The typos in Fig. 2.8 (page 20) should be fixed.
\ans{\oldnewpage{21}{22}%
These typos, as others in the thesis, were corrected. 
The updated version of Fig. 2.8 is as follows:
\begin{center}
	\includegraphics{figures/narx_model.pdf}	
\end{center}
}

\item The decoupling problem could be illustrated with a simple example, e.g., a homogeneous polynomial of degree two and the Cholesky factorization of the corresponding matrix. This relates to a number of concepts of linear algebra, on which the thesis is largely based.
\ans{\oldnewpage{25}{25}%
We have added to following paragraph, considering a simple decoupling case, for which no tensor decomposition methods are required.

{\color{red} The most simple decoupling problem for nonlinear polynomials can be illustrated with the following example, a homogeneous polynomial of degree two:
$$
f(u_1,u_2) = a_1 u_1^2 + 2 a_2 u_1 u_2 + a_3 u_2^2.
$$
This homogeneous polynomial $f$ can be rewritten in matrix form as
$$
f(u_1,u_2) = 
\begin{bmatrix}
	u_1 & u_2
\end{bmatrix}
\underbrace{\begin{bmatrix}
	a_1 & a_2 \\
	a_2 & a_3
\end{bmatrix}}_\A
\begin{bmatrix}
	u_1 \\
	u_2
\end{bmatrix},
$$
and the matrix $\A$ can be factorized with the Cholesky decomposition into $\A = \L^T \L$.
Therefore, the output of $f$ can be rewritten as
\begin{equation}
f(u_1,u_2) = (\L \u)^T (\L \u),
\label{eq_degree2}	
\end{equation}
with $\u = (u_1,u_2)$, and Equation~\eqref{eq_degree2} is precisely the decoupled representation of $f$, with the following values for the decoupling parameters:
\begin{itemize}
	\item $\V = \L^T$,
	\item $\g(x_1, x_2) = (x_1^2, x_2^2)$,
	\item $\W = \begin{bmatrix}
		1 \\
		1
	\end{bmatrix}$.
\end{itemize}
Generalizing this simple decoupling problem to higher-degree (non-homogeneous) polynomials with possibly more outputs require a different set of techniques.}
}

\item Step 4 of Algorithm 1 (page 34) should be described in more detail and made self-contained.
\ans{\oldnewpage{34}{35}%
To make Algorithm~1 more self-contained, more details were given for the integration and interpolation step.
The following paragraph was adapted in the thesis:

{\color{red}
Starting from the vectors $\h_1,\ldots,\h_r$, reconstructing the internal univariate functions~$g_1(\v_1^T\u),\ldots,g_r(\v_r^T\u)$ amounts to solving a linear system in the exact case (Section~2.4 of~\cite{Dreesen2015}). 
However, when generalizing to the noisy case (Section~3.1.3), we prefer to use all available information, and propose the fitting and integration procedure explained in Section~II.C of~\cite{Dreesen2015a}. 
This procedure consists of the following two parts: in the first part, the vectors $\h_1,\ldots,\h_r$ are fitted with polynomials using Vandermonde matrices, as we have $\h_{j}(i) = g'_j(\v^T_j\u^{(i)})$.
Hence, these fitted polynomials represent the derivatives $g'_1,\ldots,g'_r$ of the internal functions $g_1,\ldots,g_r$.
In the second part, a symbolic integration of each derivative function $g'_1,\ldots,g'_r$ yield the internal functions $g_1,\ldots,g_r$.
Furthermore, the correct integration constants are recovered by evaluating the internal functions $g_1,\ldots,g_r$ in the operating points.
}
}

\item The issue of uniqueness of the tensor decomposition has been discussed in the thesis and during the private defense. It would be good to have an example in the thesis, demonstrating the notion of `not too large' number of branches (see page 35).
\ans{\oldnewpage{38}{40}%
A simple example for the notion of ``too large'' is given in Figure~\ref{fig_kruskal}.
For a multivariate polynomial with two inputs and two outputs, and with three decoupled branches, the uniqueness condition is unsatisfied.
Although, the uniqueness condition is not a necessary condition for the uniqueness of tensor decompositions, this case with an unsatisfied uniqueness condition is a good place to find multivariate polynomials with ``too many'' branches.

For different values of $m$ and $n$ between 1 and 20, the following table shows the smallest values for $r$ for which the uniqueness condition for tensor decompositions is unsatisfied:
$$
\left[
\begin{array}{c|cccccccccccccc}
 m\backslash ^{\displaystyle n} & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 12
   & 14 & 16 & 18 & 20 \\
   \hline
 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 13 &
   15 & 17 & 19 & 21 \\
 3 & 4 & 5 & 7 & 9 & 11 & 12 & 14 & 16 & 17 & 21
   & 24 & 28 & 31 & 35 \\
 4 & 5 & 7 & 10 & 12 & 14 & 17 & 19 & 22 & 24 &
   29 & 34 & 39 & 44 & 49 \\
 5 & 6 & 9 & 12 & 15 & 18 & 22 & 25 & 28 & 31 &
   37 & 44 & 50 & 56 & 63 \\
 6 & 7 & 11 & 14 & 18 & 22 & 26 & 30 & 34 & 38 &
   46 & 53 & 61 & 69 & 77 \\
 7 & 8 & 12 & 17 & 22 & 26 & 31 & 35 & 40 & 44 &
   54 & 63 & 72 & 81 & 90 \\
 8 & 9 & 14 & 19 & 25 & 30 & 35 & 41 & 46 & 51 &
   62 & 72 & 83 & 94 & 104 \\
 9 & 10 & 16 & 22 & 28 & 34 & 40 & 46 & 52 & 58
   & 70 & 82 & 94 & 106 & 118 \\
 10 & 11 & 17 & 24 & 31 & 38 & 44 & 51 & 58 & 65
   & 78 & 92 & 105 & 118 & 132 \\
 12 & 13 & 21 & 29 & 37 & 46 & 54 & 62 & 70 & 78
   & 94 & 111 & 127 & 143 & 159 \\
 14 & 15 & 24 & 34 & 44 & 53 & 63 & 72 & 82 & 92
   & 111 & 130 & 149 & 168 & 187 \\
 16 & 17 & 28 & 39 & 50 & 61 & 72 & 83 & 94 &
   105 & 127 & 149 & 171 & 193 & 215 \\
 18 & 19 & 31 & 44 & 56 & 69 & 81 & 94 & 106 &
   118 & 143 & 168 & 193 & 217 & 242 \\
 20 & 21 & 35 & 49 & 63 & 77 & 90 & 104 & 118 &
   132 & 159 & 187 & 215 & 242 & 270 \\
\end{array}
\right].
$$

\begin{figure}[t!]
\centerline{\includegraphics[width=0.8\textwidth]{figures/He_matrix_non_kruskal}}
\caption{\color{blue}Example of the notion ``too large'' number of branches. When considering a nonlinear function~\f\ with 2 inputs, 3 branches and 2 outputs, the uniqueness condition is unsatisfied. In this case, the columns $h_i$ of the factor $\H$ after the tensor decomposition has a noisy behavior, when plotting agains $\v_i^T\u$, even though no noise was added on the coefficients of the nonlinearity.}\
\label{fig_kruskal}
\end{figure}
}

\item The term ``noisy polynomial'' should be explained, as it can be interpreted in different ways.
\ans{\oldnewpage{37}{41}%
The following text was added to the thesis text:

{\color{red}In this thesis, the term ``noisy polynomial'' is understood as a coupled multivariate polynomial~\f\ with coefficients $\c$, which does not have an exact decoupled representation.
In Section~3.2, we assume the noise is correlated with a known covariance matrix ($\mathbf{\Sigma}_\mathbf{c} = \mathbf{N}\mathbf{N}^T $) and is added on the level of the polynomial coefficients, to the coefficients $\c_0$ of an underlying (and inaccessible) multivariate polynomial $\f_0$ in decoupled form.
}
}

\item The derivation of (3.10) is not completely clear. The explanation that comes around (3.15) should be moved earlier, possibly into a new subsection (before Section 3.4.1).
\ans{\oldnewpage{44}{46}%
As is described in the first few paragraphs of Section~3.4, this section consists of the following four subsection:
\begin{description}
	\item[3.4.1] Defining the cost function for the weighted CPD. 
	In this subsection, the cost function is defined in its most general form with a weight matrix in Equation~(3.9).
	Accordingly, the more specific cost function, using the specific weight matrix for the weighted CPD is given in Equation~(3.12), as a conclusion to the first subsection.
	I consider it clear and logical to keep the general and specific cost functions (for the same optimization problem) in the same subsection.
	\item[3.4.2] Having defined the cost function for the weighted CPD, the next subsection gives the proposed method for minimizing it.
	This method is a generalization of the ALS scheme for the CPD.
	\item[3.4.3] Looking back to the definition of the weighted cost function in Equation~(3.12), the importance of the two sets of equations in the weight matrix is now explained.
	Hence, the dimensions of the factors are analyzed, and a set of new equations is proposed in Equation~(3.18).
	\item[3.4.4] General remarks on the weighted ALS are given in the last subsection of Section 3.4.
\end{description}

Even though new orderings of this subsection could be made, I am not sure how it can help the reader during a first reading.
Moreover, in the current ordering, references to later subsections are made in the crucial points, to help the reader during a first reading:
\begin{itemize}
	\item Second paragraph under Section~3.4 refers to the two sets of equations which will be discussed later: 
	{\sl Hence, in this section, we propose a new weighted cost function on
the Jacobian tensor level, using two sets of equations (Section 3.4.1), \ldots
Both sets of equations are equally important for the
discussed problem, and must be considered together to create a low-rank
weighted CPD (Section 3.4.3).}
	\item Additionally, in Subsection~3.4.1, under Equation~(3.10), a direct reference to the future discussion is stated:
	{\sl How both matrix parts of $\SigmaJ$ contribute in the definition of \L, and why this two-part definition of \L\ is necessary, is analyzed in Section 3.4.3.}
	\item  A discussion for the newly introduced hyperparameter~$\lambda$ is also postponed to a later subsection, as it deserves a (portion of a) subsection separately: 
	{\sl For the weighted cost function~(3.9), we define the matrix~\L\ as the following combination of the two submatrices $\Vs^{(1)}$ and $\Vs^{(2)}$ of~\SigmaJ, and introduce the hyperparameter $\lambda$ in the second part of the definition of the matrix~\L.}
\end{itemize}

In conclusion, I have tried my best to deliver a consistent discussion about the definition of the cost function for the weighted CPD, using several subsections and interconnections between them.
I feel these interconnections may help the reader during a first reading of Section 4.3.
}

\item It should be clarified if the choice of $\lambda$ on page 60 is based on a single experiment or on averaged performance (over how many runs?) for each considered value.
\ans{\oldnewpage{60}{62}%
The choice for $\lambda$ was made for one experiment per possible value of $\lambda$, to have an initial estimate of $\lambda$.
However, once the value of $\lambda$ was chosen, 200 different noise realizations were considered to gain intuition on an average performance.
A more elaborate tuning mechanism of~$\lambda$ could be considered, but is out of scope for this thesis.

The following sentence was added to the thesis:

{\color{red} This value for $\lambda$ is based on a single experiment for each~$\lambda$ and will be used in the rest of this section.
}
}

\item The reference within Algorithm 3 (page 73) should be verified.
\ans{\oldnewpage{73}{75}%
The reference was updated to \cite{Schoukens2015}.
}

\item The derivations in Section 7.2.2 concern matrices (not higher-order tensors) and relate to the so-called structured low-rank approximation (SLRA) problem, and in particular the work of Ivan Markovsky and of Mariya Ishteva. This should be acknowledged.
\ans{\oldnewpage{111}{113}%
The following text has been added to the start of Section~7.2.2:

{\color{red}
The relaxed ALS described in this section related to the so-called structured low-rank approximation (SRLA) problem~\cite{Markovsky2008}.
Other methods for finding structured low-rank approximations of matrices have been studied, for example using regularization~\cite{Ishteva2014} or variable projections~\cite{Usevich2014a}.
}
}

\item When dealing with polynomial constraints, the term ALS is less appropriate, given that the nonlinearity at the V-update is ignored. Another name could be used, e.g., `relaxed ALS'.
\ans{\oldnewpage{110}{112}%
The proposed term ``relaxed ALS'' was introduced in the Chapters~5 and~6, and replaces all occurances of the former term ``ALS''.
}

\item Smaller remarks and typos will be communicated separately by the jury members directly to the candidate and should be also be fixed.
\ans{The remarks and typos throughout the thesis text were corrected.
}

\item 
{\color{blue} To better clarify the interplay between the different cost functions in this thesis, I added the following figure and its caption in the introduction:}
\oldnewpage{25}{28}%

{\color{red}
\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/interplay_costs.pdf}
\end{center}
\caption{\color{red}Central in this thesis' research, is the interplay between three levels of complexities, and their associated cost functions.
The outermost level lies at level of nonlinear system identification, and its cost function~$V_1$ is defined considering measured inputs and outputs of an unknown system to be identified.
In an intermediary level, the coupled nonlinear function should be decoupled, and its cost function~$V_2$ is defined in terms of the unknown internal variables $\V, \g$ and $\W$.
Finally, the innermost level is defined on the level of the Jacobian and their elements. This level corresponds with a third cost function~$V_3$ which can be optimized using tensor decomposition methods.
The interplay between the levels is created by consecutively use the optimized parameters of an inner level as the initialization of the next outer level.}
\label{fig_interplay_cost_functions}
\end{figure}
}

\end{itemize}

\renewcommand{\oldnewpage}[2]{\marginpar{{\color{black}\small#1} / {\color{red}\small#2}}}

\newpage
\def\thispagestyle#1{}
\bibliographystyle{plain}
\bibliography{bibliography}


\end{document}