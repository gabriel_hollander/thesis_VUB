addpath('Z:\2016-03c WH Approximation');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 27/4/2014
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all; 
close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% transformation matrices V and W
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
m=2; %inputs
r=2; %branches
n=2; %outputs
N=200; % linearization points
maxdeg=3; % max degree of polynomials g_i

d = maxdeg*ones(r,1);
%d = randi([1,maxdeg],r,1); % degree vector of polynomials g_i

% mixing matrices
V = randn(m,r);
W = randn(n,r);

if rank(V)~=min([r m]), error('rank V error'); end
if rank(W)~=min([n r]), error('rank W error'); end
if r~=size(W,2), error('size W error'); end
if r~=size(V,2), error('size V error'); end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% declaring symbolic variables x and u 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% trick: apostrophe as string
ap = '''';

% declaring symbolic vars xi
x = sym(zeros(r,1));
for i = 1:r,
    % xi = sym('xi');
    eval([ 'x' num2str(i) ' = sym(' ap 'x' num2str(i) ap ');' ]);
    % x(i) = xi;
    eval([ 'x(i) = x' num2str(i) ';' ]);
end

% declaring symbolic vars ui
u = sym(zeros(m,1));
for i = 1:m,
    % ui = sym('ui');
    eval([ 'u' num2str(i) ' = sym(' ap 'u' num2str(i) ap ');' ]);
    % u(i) = ui;
    eval([ 'u(i) = u' num2str(i) ';' ]);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% internal univariate mappings gi(xi)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:r,
%     d = randi([maxdeg,maxdeg],r,1); 
    % gi = randn(di+1,1).'*vdmvec(xi,di);
    eval(['g' num2str(i) ' = randn(d(i)+1,1).' ap ' * vdmvec(x' num2str(i) ',d(i));']);
end

%g1 = 3*x1^2-x1+5;
%g2 = -5*x2^2+3*x2-7;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% declare vectorization G in x and u
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Gx = sym(zeros(r,1));
Gu = sym(zeros(r,1));
dGx = sym(zeros(r,1));

for i = 1:r,
    % Gx(i) = gi;
    eval([ 'Gx(i) = g' num2str(i) ';' ]); 
    
    % dGx(i) = diff(gi,xi);
    eval([ 'dGx(i) = diff(Gx(i), x' num2str(i) ');']);
    
    % Gu(i) = subs( gi, xi, V(:,i).'*u);
    eval([ 'Gu(i) = subs(g' num2str(i) ', x' num2str(i) ',V(:,i).' ap '*u);' ]);
    Gu(i) = expand(Gu(i));
end

% combining W*gi(v_i^T*u) to form f=[fi]
f = sym(zeros(n,1));
for i = 1:n,
    % fi = W(i,:)*Gu;
    eval([ 'f' num2str(i) ' = W(i,:)*Gu;' ]);
    eval([ 'f(i) = f' num2str(i) ';' ]);
end

tic
f_mf = matlabFunction( f, 'vars', u );
toc

% Jacobian (symbolic)
Ju = jacobian (f, u);

tic
Ju_mf = matlabFunction( Ju, 'vars', u );
toc

% Ju = sym(zeros(n,m)); 
% 
% for i = 1:n,
%     for j = 1:m,
%         %Ju(i,j) = diff(fi,uj);
%         eval(['Ju(' num2str(i) ',' num2str(j) ') = diff(f' ...
%            num2str(i) ',u' num2str(j) ');' ]);
%     end
% end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% small signal experiments
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ustar=zeros(m,N);


% ystar = repmat(f,[1 N]);
% Jstar=repmat(Ju,[1 1 N]);
% zstar=repmat(Gu,[1 N]);

ystar = zeros(n,N);
Jstar = zeros(n,m,N);

% H = sym(zeros(N,r));

for k=1:N,
    % progress
    if mod(k,10)==0, disp(k), end;
    
    % uniform dist
    a=-1;
    b=1;
    ustar(:,k) = a + (b-a).*rand(m,1);
   
    % evaluating f and Jf in ustar(k)
    utmp = num2cell(ustar(:,k));
    Jstar(:,:,k) = Ju_mf(utmp{:});
    ystar(:,k) = f_mf(utmp{:});
    
%      for i=1:m,
%           eval([ 'ystar(:,k) = subs(ystar(:,k), u' num2str(i) ',ustar(i,k));' ]);
%           eval([ 'Jstar(:,:,k) = subs(Jstar(:,:,k), u' num2str(i) ',ustar(i,k));' ]);
% %           eval([ 'zstar(:,k) = subs(zstar(:,k), u' num2str(i), ',ustar(i,k));' ]);
%      end
%      % evaluating H(.,.)
%      for j=1:r,
%         %H(k,:) = subs(dGx(k),xk,V(:,k).'*ustar(:,k));
%         eval(['H(k,j) = subs(dGx(j),x' num2str(j) ',V(:,j).'*ustar(:,k));']);
%      end
end


ystar=double(ystar);
Jstar=double(Jstar);
% H=double(H);

% zstar=double(zstar);
xstar=V.'*ustar;

% Jstartmp=cpdgen({W,V,H});
% norm(vec(Jstartmp-Jstar))



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Canonical Polyadic Decomposition
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% rankestimation correct?!
rkest=rankest(Jstar)
if (rkest~=r), 
    warning('rank est not correct! overriding by correct one'); 
end

re=r; 


% perform CPD
its = 10;
goon = 1;
while goon & goon < its ,
    WVH=cpd(Jstar,re);
    relerr=norm(vec(cpdgen(WVH)-Jstar))/norm(vec(Jstar))
    if relerr < 1e-7, goon = 0; end
end

if relerr > 1e-7, warning('all your relerr CPD are belong to us!!!'); end

We = WVH{1};                    
Ve = WVH{2};                    
He = WVH{3};


% check reconstruction of V/W/H (normalized)
disp('reconstruction V')
V*diag(1./V(1,:))
Ve*diag(1./Ve(1,:))

disp('reconstruction W')
W*diag(1./W(1,:))
We*diag(1./We(1,:))

% disp('reconstruction W')
% H*diag(1./H(1,:))
% Ge*diag(1./Ge(1,:))

%% Plot the He matrix with respect to Ve.'*ustar (noiseless case)
% color = 'brgmk';
% clf;
% hold on;
% for iter = 1 : size(He,2)
% plot(TakeRow(Ve.'*ustar,iter), He(:,iter), [color(iter) '.'], 'markersize', 3)
% end
% text('string', '$h_1$', 'pos', [3 0], 'interpreter', 'latex', 'color', 'r');
% text('string', '$h_2$', 'pos', [5 -0.3], 'interpreter', 'latex', 'color', 'b');
% hold off;
% set(gca, 'xtick', [-10 0 10], 'ytick', [-0.8 0 0.1], 'ylim', [-0.8 0.1])
% xlabel('$\mathbf{v}_i^T \mathbf{u}$', 'interpreter', 'latex');
% ylabel('$h_i$', 'interpreter', 'latex');

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\He_matrix_without_noise.tex', 'width', '0.8\textwidth', 'height', '5cm');


%% Plot the He matrix with respect to Ve.'*ustar (noiseless case)
% color = 'brgmk';
% clf;
% hold on;
% for iter = 1 : size(He,2)
% plot(TakeRow(Ve.'*ustar,iter), He(:,iter), [color(iter) '.'], 'markersize', 3)
% end
% text('string', '$h_1$', 'pos', [3 0], 'interpreter', 'latex', 'color', 'g');
% text('string', '$h_2$', 'pos', [-2 2], 'interpreter', 'latex', 'color', 'r');
% text('string', '$h_3$', 'pos', [2.5 2.5], 'interpreter', 'latex', 'color', 'b');
% hold off;
% set(gca, 'xtick', [-4 0 4], 'ytick', [-1.5 0 3])
% xlabel('$\mathbf{v}_i^T \mathbf{u}$', 'interpreter', 'latex');
% ylabel('$h_i$', 'interpreter', 'latex');
% 
% % CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\He_matrix_non_kruskal.tex', 'width', '0.8\textwidth', 'height', '5cm');









