%% Create the permutation matrices
% cd('Z:\2015-11-17c Experiment with a lot of systems_fW decoupling');
addpath('Z:\2016-03c WH Approximation');

m = 3;
n = 3;
N = 4;


T = reshape(1:m*n*N, [n,m,N]); I = eye(m*n*N);
reorder_1 = vec(reshape(T, [n, m*N]).');
P_1 = I(reorder_1, :);      % due to the code of Nway toolbox (and Kolda)
reorder_2 = vec(reshape(permute(T, [2, 1, 3]), [m, n*N]).');
P_2 = I(reorder_2, :);    % due to Kolda, so using a MATLAB-friendly matricizing order (following the three dimensions), and not a "mathematical"-friendly way (with cyclic permutations)
reorder_3 = vec(reshape(permute(T, [3, 1, 2]), [N, m*n]).');
P_3 = I(reorder_3, :);    % due to the code of Nway toolbox (and Kolda)

%% Create the grid of numbers
numbers = fliplr(1:m*n*N);
grid = NaN(m*n*N);
counter = 1;

for iter = 1 : m*n*N
    grid(iter:m*n*N, iter) = [counter: counter+numbers(iter)-1];
    grid(iter, iter:m*n*N) = [counter: counter+numbers(iter)-1];
    counter = counter + numbers(iter);
end

grid_1 = P_1*grid*P_1.';
grid_2 = P_2*grid*P_2.';
grid_3 = P_3*grid*P_3.';

%% Create the colored grids

Ncolors = sum(numbers);
colors = hsv2rgb([linspace(0,0.95,Ncolors).' repmat(0.7,Ncolors,1) repmat(1,Ncolors,1) ]);

grid2plot = fliplr(grid);
grid2plot_1 = fliplr(grid_1);
grid2plot_2 = fliplr(grid_2);
grid2plot_3 = fliplr(grid_3);

sideRect = 0.4;
epsi = 0.005;

clf;
figure(1);
% figure('units', 'pixels', 'pos', [2200 400 800 800], 'color', 'white');
ax0 = axes('units', 'norm', 'pos', [0 0 1 1], 'xlim', [0 1], 'ylim', [0 1], 'visible', 'off');
ax1 = axes('units', 'norm', 'pos', [0.5-0.35/2 0.65 0.35 0.35], 'xlim', [0 m*n*N+1], 'ylim', [0 m*n*N+1], 'visible', 'off'); axis square;
ax2 = axes('units', 'norm', 'pos', [0.05 0.12 0.4 0.4], 'xlim', [0 m*n*N+1], 'ylim', [0 m*n*N+1], 'visible', 'off'); axis square;
ax3 = axes('units', 'norm', 'pos', [0.5-0.2 0.12 0.4 0.4], 'xlim', [0 m*n*N+1], 'ylim', [0 m*n*N+1], 'visible', 'off'); axis square;
ax4 = axes('units', 'norm', 'pos', [0.55 0.12 0.4 0.4], 'xlim', [0 m*n*N+1], 'ylim', [0 m*n*N+1], 'visible', 'off'); axis square;

for iter1 = 1 : m*n*N
    iter1
    for iter2 = 1 : m*n*N
        rectangle('parent', ax1, 'position', [iter1-sideRect, iter2-sideRect, 2*sideRect 2*sideRect], 'facecolor', colors(grid2plot(iter1,iter2),:));
        text('parent', ax1, 'position', [iter1, iter2], 'string', grid2plot(iter1,iter2), 'hor', 'center', 'vert', 'middle')
        rectangle('parent', ax2, 'position', [iter1-sideRect, iter2-sideRect, 2*sideRect 2*sideRect], 'facecolor', colors(grid2plot_1(iter1,iter2),:));
        text('parent', ax2, 'position', [iter1, iter2], 'string', grid2plot_1(iter1,iter2), 'hor', 'center', 'vert', 'middle')
        rectangle('parent', ax3, 'position', [iter1-sideRect, iter2-sideRect, 2*sideRect 2*sideRect], 'facecolor', colors(grid2plot_2(iter1,iter2),:));
        text('parent', ax3, 'position', [iter1, iter2], 'string', grid2plot_2(iter1,iter2), 'hor', 'center', 'vert', 'middle')
        rectangle('parent', ax4, 'position', [iter1-sideRect, iter2-sideRect, 2*sideRect 2*sideRect], 'facecolor', colors(grid2plot_3(iter1,iter2),:));
        text('parent', ax4, 'position', [iter1, iter2], 'string', grid2plot_3(iter1,iter2), 'hor', 'center', 'vert', 'middle')
    end
end
text('parent', ax1, 'string', '$\mathbf{\Omega}$', 'units', 'norm', 'position', [0.5 -0.005], 'hor', 'center', 'interpreter','latex' );
text('parent', ax2, 'string', '$\mathbf{\Omega}_{(1)}$', 'units', 'norm', 'position', [0.5 -0.005], 'hor', 'center', 'interpreter','latex' );
text('parent', ax3, 'string', '$\mathbf{\Omega}_{(2)}$', 'units', 'norm', 'position', [0.5 -0.005], 'hor', 'center', 'interpreter','latex' );
text('parent', ax4, 'string', '$\mathbf{\Omega}_{(3)}$', 'units', 'norm', 'position', [0.5 -0.005], 'hor', 'center', 'interpreter','latex' );
annotation('textarrow',[0.5-epsi 0.25], [0.63 0.51],'String','')
annotation('textarrow',[0.5 0.5], [0.63 0.51],'String','')
annotation('textarrow',[0.5+epsi 1-0.25], [0.63 0.51],'String','')


%% convert to TikZ
matlab2tikz('figurehandle', gcf, 'filename', 'Z:\2015-08 SIMAX journal paper\Version of 2015-11-16\Figures\coloredGrids.tex', ...
            'width', '20cm', 'standalone', true)

