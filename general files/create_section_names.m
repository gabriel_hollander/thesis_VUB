%% working directly with the toc file
% clc;
width_wrapping = 40;
tocfile = fileread(fullfile('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis', 'main.toc'));
tocfile = strrep(tocfile, '\discretionary {-}{}{}', '');
toc_split = strsplit(tocfile, '\n');
toc_iter = 0; toc_el = 0;
for iter = 1 : length(toc_split)
%     regexp(toc_split{iter},    '\\contentsline\s\{(?<sec_type>\w*)\}\{\\numberline\s\{(?<chapter_number>\d*)(?<sec_number>[\d.]*)\}(?<sec_title>[\w*\s\\\~\-\{\}]*)\}\{(?<sec_page>\d*)\}', 'names')
    temp_splitted = regexp(toc_split{iter}, '\\contentsline\s\{(?<sec_type>\w*)\}\{\\numberline\s\{(?<sec_number>[\d.]*)\}(?<sec_title>[\w*\s\\\~\-\{\}]*)\}\{(?<sec_page>\d*)\}', 'names');
    if ~isempty(temp_splitted) 
        if strcmp(temp_splitted.sec_type, 'chapter')
            toc_iter = toc_iter + 1;
            toc_el = 0;
        end
        toc_el = toc_el + 1;
        toc{toc_iter}{toc_el} = temp_splitted;
        toc{toc_iter}{toc_el}.sec_title = split_text(toc{toc_iter}{toc_el}.sec_title, width_wrapping);
    end
end



%% create plot with section names
clf;
current_chapter = 1;
vheight = 5;
vskip_chap = 1;
vskip_sec = 0.5;
vskip_subsec = 0.5;
vskip_line = 0.2;
current_vpos = vheight;
hnum = 0;
hsec = 0.1;
hcolumn = 0;
hindent = 0.1;
column_width = 3;

for iter_chap = 2 : 7
    if iter_chap == 5, current_vpos = vheight; hcolumn = hcolumn + column_width; end
    text('string', toc{iter_chap}{1}.sec_number, 'pos', [hcolumn + hnum, current_vpos]);
    current_vpos = write_text_cell(toc{iter_chap}{1}.sec_title, hcolumn + hnum + hsec, hindent, current_vpos, vskip_line);
    
    current_vpos = current_vpos - vskip_chap;
end

set(gca, 'ylim', [-3 7], 'xlim', [-1 5], 'visible', 'off')





%% create plot with section names
current_chapter = 1;

column = [repmat(-10, 1,3) repmat(0, 1,3)];
clf;
hindent = 0.2;
vpos = 2;
vpos_chapter = 0;
vpos_sec = 4;
vpos_subsec = 3;
for iter_chapter = 2 : 7
    if iter_chapter == 5, vpos_col1 = vpos; vpos = 2; end
    vpos = vpos - 1;
    text('string', ['\tiny\bf' toc{iter_chapter}{1}.sec_number ' ' toc{iter_chapter}{1}.sec_title], 'pos', [column(iter_chapter-1), vpos], 'interpreter', 'latex');
    if iter_chapter == current_chapter + 1
        for iter_current_chap = 2 : length(toc{iter_chapter})
            text_section = [toc{iter_chapter}{iter_current_chap}.sec_number ' ' toc{iter_chapter}{iter_current_chap}.sec_title ' \hskip1cm' toc{iter_chapter}{iter_current_chap}.sec_page];
            hpos = column(iter_chapter-1) + hindent*length(strsplit(toc{iter_chapter}{iter_current_chap}.sec_number, '.'));
            switch toc{iter_chapter}{iter_current_chap}.sec_type
                case 'section'
                    vpos = vpos - vpos_sec;
                case 'subsection'
                    vpos = vpos - vpos_subsec;
            end
            text('string', sprintf(split_text(text_section)), 'pos', [hpos vpos], 'interpreter', 'latex');
        end
    end
end
set(gca,'xlim', [-10 10], 'ylim', [min([vpos_col1, vpos]-1) 10], 'visible', 'off')

CurrentFigure2pdf(fullfile('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis', ['chapter' num2str(current_chapter)], 'figures', ['overview_chapter' num2str(current_chapter) '.tex']), ...
    'width', '20cm', 'height', '12cm');










