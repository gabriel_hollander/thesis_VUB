uiopen('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\FreqDomainErrors6.fig',1);
set(get(gca, 'ylabel'), 'string', 'prediction output (dB of magnitude)')
set(gca, 'xticklabel', {0, sprintf('%.3f', 1/6)})
% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\fig_output_sls_with_errors.tex', 'width', '0.8\textwidth', 'height', '6cm');