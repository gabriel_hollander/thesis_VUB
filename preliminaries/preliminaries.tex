% \renewcommand*\thesection{\arabic{section}}
% \setcounter{chapter}{-1}
\chapter{Preliminaries}\label{chap_introduction}
% \addcontentsline{toc}{chapter}{Introduction}

\abstracttext{%
\noindent The general introduction of Chapter~\ref{chap_general_intro} surveyed the general context and research questions of this thesis.
However, as the work in this different is mathematical in essence, a more detailed and technical overview is needed. \newline
This chapter presents the mathematical details used extensively, and it is organized as follows: Section~\ref{sec_tensors_and_decompositions} presents the world of tensors, which lies at the core of almost every algorithm of this thesis.
Further, Section~\ref{sec_nonlinear_model_types} gives a detailed overview of the field of nonlinear system identification and focuses on the three nonlinear model types considered in this thesis.
Finally, Section~\ref{sec_decoupling_related_work} relates the decoupling problem of coupled nonlinearities to existing literature and earlier work.
}

\section{Tensors and tensor decompositions\label{sec_tensors_and_decompositions}}
In many scientific and engineering fields, multilinear algebra is becoming increasingly important.
General overviews of this field can be found in the excellent works~\cite{Cichocki2015, Comon2014, Grasedyck2013, Kolda2009, Landsberg2012, Oseledets2009, Oseledets2010,Sidiropoulos2016, Smilde2004}.

Multilinear algebra studies properties of so-called tensors, which are multidimensional arrays of numbers.
For example, the tensor in three dimensions
$$
\mathcal{A} = [A(i,j,k)]_
\begin{matrix}
	\scriptstyle i=1,\ldots, I \\
	\scriptstyle j=1,\ldots, J \\
	\scriptstyle k=1,\ldots, K
\end{matrix},
$$
with $I,J,K \in \EN$, consists of an array of two-dimensional matrices stacked together (Figure~\ref{fig_third_order_tensor}).
\begin{figure}[ht]
\centering
\includegraphics{preliminaries/figures/third_order_tensor.pdf}
\caption{In this thesis, a tensor specifies a multidimensional array of numbers.
Three-dimensional tensors can be represented by a box and will be used throughout this work.}
\label{fig_third_order_tensor}
\end{figure}
As it is customary, tensors of at least three dimensions (order three or larger) are noted with a scripted letter, for example~$\mathcal{A}, \mathcal{B}, \mathcal{C}, \ldots$
Matrices, which can be viewed as tensors of order two, are written as boldface capital letters $\mathbf{A}, \mathbf{B}, \mathbf{C}, \ldots$, while boldface lowercase letters represent vectors, such as $\mathbf{a}, \mathbf{b}, \mathbf{c}, \ldots$
Lowercase non-bold letters stand for scalars, for example~$a, b, c, \ldots$

In this thesis, the properties of the normed vector space of tensors are used, with the element-wise sum of tensors of equal dimensions, and the scalar product of a scalar and a tensor.
The norm of a tensor is usually defined as the so-called Frobenius norm
$$
\normf{\mathcal{A}} = \sqrt{
	\sum_{i = 1}^I \sum_{j = 1}^J \sum_{k = 1}^K a_{ijk}^2,
}
$$
for a three-way tensor $\mathcal{A}$ of dimensions $I\times J\times K$ consisting of the element~$a_{ijk}$ at the position $(i,j,k)$.
Accordingly, the Frobenius norm is equivalent the Euclidean norm of the \emph{vectorized} tensor,
$$
\normf{\mathcal{A}} = \norm{\myvec{\mathcal{A}}},
$$
where $\myvec{\mathcal{A}}$ is created by stacking the vertical fibers of $\mathcal{A}$ on top of each other.
These fibers are the higher-order analogue of matrix rows and columns, and are defined by fixing every index except one~\cite{Kolda2009}.
Although the specific order of the vertical stacked fibers is unimportant as long as it is chosen consistently throughout the thesis, we have chosen the same order as the one given with MATLAB's command~\verb!A(:)!.

A core concept used throughout this work is the decomposition of a tensor into a sum of rank-one tensors.
Although this concept can be defined for tensors of arbitrary dimensions, we will restrict the following definitions for the third-order case, as only tensors up to order three are used throughout this thesis.
However, many general recent overviews about tensors and their decompositions are available in the literature~\cite{Cichocki2015, Comon2014, Grasedyck2013, Kolda2009, Landsberg2012, Oseledets2009, Oseledets2010, Sidiropoulos2016, Smilde2004}.

A third-order tensor $\mathcal{A}\in\ER^{I\times J\times K}$ is said to be of rank one, if it can be written as the outer product of three vectors ($\circ$ indicates the outer product):
$$
\mathcal{A} = \mathbf{a} \circ \mathbf{b} \circ \mathbf{c}.
$$
On the level of the tensor elements, every element of a rank-one tensor can be written as the product of the corresponding vector elements (Figure~\ref{fig_rank_one_tensor}):
$$
\mathcal{A}(i,j,k) = a_i b_j c_k.
$$
Inherently, a tensor of rank one consists of less degrees of freedom than a general tensor: the former is dependent on $I+J+K-2$ parameters (scaling invariances should be taken into account), while the latter on~$IJK$.
\begin{figure}[ht]
\centering
\includegraphics{preliminaries/figures/rank_one_outer_product.pdf}
\caption{A tensor $\mathcal{A}$ is said to be of rank one, if it can be written as the outer product of three vectors $\mathbf{a}$, $\mathbf{b}$ and $\mathbf{c}$. 
In this case, the element of $\mathcal{A}$ on position $(i,j,k)$ is the product of the corresponding elements of the vectors $a_i b_j c_k$.}
\label{fig_rank_one_tensor}
\end{figure}

Hence, rank-one tensors are seen as the basic \emph{building blocks} of all tensors.
In this view, every tensor can be written as a sum of rank-one tensors (Figure~\ref{fig_higher_rank_tensor}), maximally consisting of $\min\{IJ,IK,JK\}$ terms~\cite{Kruskal1989}.
\begin{figure}[ht]
\centerline{
\includegraphics[width=1.1\textwidth]{preliminaries/figures/rank_three_outer_product.pdf}}
\caption{Any tensor can be written as the sum of rank one tensors.
The smallest number $R$ of terms for which equality holds is called the \emph{rank} of the tensor.}
\label{fig_higher_rank_tensor}
\end{figure}
However, the smallest number $R$ of rank-one tensors that generate the original tensor, is called the \emph{rank} of the tensor:
\begin{equation}
	\mathcal{A} = \sum_{r = 1}^R \mathbf{a}_r \circ \mathbf{b}_r \circ \mathbf{c}_r
	\label{eq_cp_decomp}
\end{equation}
The process~\eqref{eq_cp_decomp} of writing a higher ranked tensor as a sum of rank-one tensors goes with several names in the literature: the Canonical Polyadic Decomposition (CPD), or Parallel Factors (PARAFAC), or the Canonical Decomposition (CANDECOMP), among others.
The decomposition~\eqref{eq_cp_decomp} is also noted as $\cpdgen{\mathbf{A}}{\mathbf{B}}{\mathbf{C}}$, where the matrix $\mathbf{A}$ consists of the vectors~$\mathbf{a}_1, \ldots, \mathbf{a}_R$ column-wise stacked together, and equivalently for the matrices~$\B$ and $\C$.

Unfortunately, no straightforward algorithm exists to determine the rank of a given tensor, except for a few special cases~\cite{Kolda2009}.
Therefore, in practice, the rank is often chosen by the user, and (possibly) low-rank \emph{approximations} are considered, i.e. the tensor $\mathcal{A}$ is written approximately as a sum of rank-one tensors:
$$
\mathcal{A} \approx \sum_{r = 1}^R \mathbf{a}_r \circ \mathbf{b}_r \circ \mathbf{c}_r.
$$
The error of this \emph{approximated CP decomposition} heavily depends on each specific application scenario.
For example, when decoupling polynomials with the technique~\cite{Dreesen2015}, the rank $R$ corresponds to the number of branches in the decoupled representation.

Computing the CPD is mathematically defined as optimizing the following nonlinear problem
\begin{equation}
	\minimize_{\A,\B,\C} \normf{\mathcal{A} - \cpdgen\A\B\C}^2.
	\label{eq_cpd_cost_function_prelim}
\end{equation}
This optimization problem is nonconvex and thus prone to local minima when using a gradient-descent approach.
Furthermore, this optimization is also ill-posed~\cite{ComonSeptember2009}, as the set of rank-$r$ tensors is generally not closed, unless $r=1$ or $r$ is maximal.
However, as numerical problems would arise in practice during iterative methods, this property of the optimization problem~\ref{eq_cpd_cost_function_prelim} is not discussed in this thesis.

In practice, the workhorse algorithm for computing the CPD is the so-called Alternating Least Squares (ALS) iterative algorithm, which is described as follows.
This algorithm works by considering three matricizations of $\mathcal{A}$, one for each dimension:
\begin{equation}
	\begin{aligned}
		\mathcal{A}_{(1)} &= \A (\C \odot \B)^T, \\
		\mathcal{A}_{(2)} &= \B (\C \odot \A)^T, \\
		\mathcal{A}_{(3)} &= \C (\B \odot \A)^T.
	\end{aligned}
	\label{eq_als_scheme}
\end{equation}
In these matricizations, $\odot$ stands for the Khatri-Rao product and it is defined as follows~\cite{Kolda2009}: if $\A\in\ER^{I\times K}$ and $\B\in\ER^{J\times K}$ are two matrices with the same number of columns, then $\A\odot\B$ is defined as
$$
\A\odot\B 
= 
\begin{bmatrix}
	a_{11} \b_1 & \cdots & a_{1J} \b_J \\
	\vdots & \ddots & \vdots \\
	a_{I1} \b_1 & \cdots & a_{IJ} \b_J \\
\end{bmatrix},
$$
where $a_{ij}$ is the element on position $(i,j)$ of $\A$ and $\b_i$ is the $i$-th column of $\B$.

The matricizations~\eqref{eq_als_scheme} render the three-dimensional tensor $\mathcal{A}$ into three different matrices, depending on the stacking order of its columns or rows.
Even though the exact matricization orders is unimportant, as long as it is constant during a computation, we have followed the order used in MATLAB throughout this thesis.
The following example~\cite{Kolda2009} shows these matricizations on the tensor $\mathcal{A}\in\ER^{4\times4\times2}$:
$$
\mathcal{A}
=
\begin{bmatrix}
	1 & 4 & 7 & 10 \\
	2 & 5 & 8 & 11 \\
	3 & 6 & 9 & 12
\end{bmatrix}
% \vskip-0.5cm
\raisebox{0.3cm}{$\displaystyle\color{gray}
\hskip-1.8cm
\begin{bmatrix}
	13 & 16 & 19 & 22 \\
	14 & 17 & 20 & 23 \\
	15 & 18 & 21 & 24
\end{bmatrix}
$}.
$$
In \matlab\, the tensor~$\mathcal{A}$ can be created using the command 
$$
\verb!A = reshape(1:24, [3 4 2])!.
$$
For this tensor, the three matricizations are defined as follows:
\begin{align*}
	\mathcal{A}_{(1)} &= 
	\begin{bmatrix}
		1 & 4 & 7 & 10 & 13 & 16 & 19 & 22 \\
		2 & 5 & 8 & 11 & 14 & 17 & 20 & 23 \\
		3 & 6 & 9 & 12 & 15 & 18 & 21 & 24
	\end{bmatrix}
	\\
	& \texttt{reshape(A, [3 8])}, 
\intertext{stacking columns of $\mathcal{A}$ next to each other,} 
	\mathcal{A}_{(2)} &= 
	\begin{bmatrix}
		1 & 2 & 3 & 13 & 14 & 15 \\
		4 & 5 & 6 & 16 & 17 & 18 \\
		7 & 8 & 9 & 19 & 20 & 21 \\
		10 & 11 & 12 & 22 & 23 & 24
 	\end{bmatrix}
	\\
	& \texttt{reshape(permute(A, [2 1 3]), [4 6])}, 
\intertext{stacking the transpositions of the rows of $\mathcal{A}$ next to each other,}
	\mathcal{A}_{(3)} &= 
	\setcounter{MaxMatrixCols}{20}
	\begin{bmatrix}
		1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11 & 12 \\
		13 & 14 & 15 & 16 & 17 & 18 & 19 & 20 & 21 & 22 & 23 & 24
	\end{bmatrix} \\ 
	& \texttt{reshape(permute(A, [3 1 2]), [2 12])}, 
\intertext{stacking the so-called fibers in the third dimension of $\mathcal{A}$ next to each other.}
\end{align*}

\vskip-1cm
With these matricizations, the ALS scheme~\eqref{eq_als_scheme} updates iteratively the matrices~\A, \B\ and \C, while keeping the two other constant.
As every matricizized equation~\eqref{eq_als_scheme} is linear in its parameters, the ALS scheme successfully changes the nonlinear and nonconvex optimization problem~\eqref{eq_cpd_cost_function_prelim} into three consecutive linear optimization problems~\eqref{eq_als_scheme}.
A summary of the ALS scheme is given in Algorithm~\ref{algor_als_scheme}.

\begin{algor}{Alternating Least Squares scheme for computing the CPD}\label{algor_als_scheme}
Consider the three-dimensional tensor~$\mathcal{A}$, for which a representation as a sum of rank-one tensors is searched for.
The Alternating Least Squares scheme for computing the CPD~$\cpdgen\A\B\C$ of the tensor~$\mathcal{A}$ is given as follows:
\begin{enumerate}
	\item Start with initial estimates for the factors $\A$, $\B$ and $\C$.
	These can be chosen randomly, except if better estimates are available.
	\item Solve the equation $\mathcal{A}_{(1)} = \A (\C \odot \B)^T$ for the factor $\A$, by keeping the two other factors $\B$ and $\C$ constant.
	\item Solve the equation $\mathcal{A}_{(2)} = \B (\C \odot \A)^T$ for the factor $\B$, by keeping the two other factors $\A$ and $\C$ constant.
	\item Solve the equation $\mathcal{A}_{(3)} = \C (\B \odot \A)^T$ for the factor $\C$, keeping the two other factors $\A$ and $\B$ constant.
	\item Repeat steps 2--4 until a stopping criterion is satisfied, for example: the relative change of the successive factors lies under a given threshold, or a given maximum number of iterations is reached.
	\end{enumerate}
\end{algor}

Even with its attractiveness and high use in the tensor community, the ALS scheme for computing the CPD still holds many questions regarding its convergence~\cite{Mohlenkamp2013}.
For example, even if the ALS cost function decreases monotonically, some theoretical cases could be thought of where the algorithm does not converge.
However, for this thesis, ALS and its weighted derivatives (Chapter~\ref{chap_1}) yielded good results for the applications considered.

Another open question in the field of tensor decompositions, is related to the \emph{uniqueness} of tensor decompositions.
Due to the algebraic properties of~\eqref{eq_cp_decomp}, every decomposition can be transformed by scaling factors of product~1 and permuting the factor columns.
These types of transformations do not alter the tensor~$\mathcal{A}$ and are considered as equal decompositions.
However, certain conditions can guarantee unique decompositions in general (except for the earlier mentioned transformations), where the most classic result is Kruskal's condition~\cite{Kruskal1977}:
\begin{equation}
	k_\A + k_\B + k_\C \ge 2 R + 2,
	\label{eq_kruskal_condition}	
\end{equation}
where $k_\A$ represents the so-called Kruskal rank of the matrix~\A.
This notion is defined as the maximum value of $k_\A$ such that any $k_\A$ columns of the matrix $\A$ are linearly independent (likewise for the other matrices $\B$ and $\C$)~\cite{Kolda2009}.
However, care should be taken, as Equation~\eqref{eq_kruskal_condition} gives a sufficient condition for a unique tensor decomposition, but it is not necessary.
Even though other sufficient conditions for uniqueness are known~\cite{Comon2017}, to our knowledge, no sufficiant and necessary conditions for unique tensor decompositions are known today.

Noteworthy, unlike multi-dimensional tensors, matrices do not contain uniqueness properties, without any additional constraint.
For example, when computing the singular value decomposition of a matrix, the decomposition is unique if the left and right matrices are assumed orthogonal and if the middle diagonal matrix gives an order to the singular values it contains.
However, these additional constraints may not be wished when working with applications, where these constraints may not be present.
Therefore, considering multi-dimensional tensors and their decompositions has received much attention in several application disciplines, among which nonlinear system identification.

The world of tensors and their decompositions has been rapidly growing in the last decades, with many open questions remaining, on a fundamental mathematical level, as well as on several application levels.
During this research, we have used tensors mainly as a tool---the tool at the core of the proposed algorithms---for the decoupling problem of multivariate polynomials.


\section[Overview of system identification]{Overview of system identification and\\ three nonlinear model types\label{sec_nonlinear_model_types}}
This section starts by surveying the field of system identification (Section~\ref{sec_overview_system_id}).
Afterwards, three nonlinear model types and their state-of-the-art identification methods are discussed:
\begin{enumerate}
	\item Block-oriented models (Section~\ref{sec_block_oriented_models}),
	\item Nonlinear autoregressive exogenous models (NARX) (Section~\ref{sec_narx_models}),
	\item Nonlinear state-space models (Section~\ref{sec_state_space_models}).
\end{enumerate}
In this thesis, we will consider these three model types to apply different decoupling methods.

\subsection{Overview of system identification\label{sec_overview_system_id}}
The field of system identification aims to make mathematical models of dynamical systems, based on its measured inputs and its outputs (Figure~\ref{fig_system_identification}).
\begin{figure}[ht]
\centering
\includegraphics{preliminaries/figures/general_systemID.pdf}
\caption{In the field of system identification, one tries to answer to following question: given the measured inputs and outputs of an unknown system, find a model which miminizes the difference between the modeled output $\hat\y$ and the measured output $\y$.}
\label{fig_system_identification}
\end{figure}
These dynamical systems relate input signals $\u$ to output signals~$\y$, and their models $G$, which map the inputs signals to the output signals, can be categorized into two types:
\begin{enumerate}
	\item The model of the system is linear~\cite{Systems1980,Ljung1999,Oppenheim1983,Pintelon2012}. In this case, their underlying differential equations are linear in $\u$ and $\y$.
	Accordingly, the linear model satisfies the superposition property:
	$$
		G(\alpha \u_1 + \beta \u_2) = \alpha G(\u_1) + \beta G(\u_2),
	$$
	for all inputs $\u_1, \u_2$ and scalars $\alpha, \beta \in \ER$.
	\item The model of the system is nonlinear. In this case, the underlying differential equations are not linear, and the superposition property of the model $G$ does not hold.
\end{enumerate}
In this thesis, we focus our attention to nonlinear models and study \emph{nonlinear system identification}.
General overviews and state-of-the-art techniques of various subfields have been gathered throughout the years~\cite{Ljung1999,Sjoberg1995}.

Because the class of nonlinear models is so large, we limit our attention to three special nonlinear model types: the block-oriented models, the nonlinear autoregressive exogenous models and the nonlinear state-space models (Figure~\ref{fig_general_thesis_problem}).
The state-of-the-art techniques for identifying these three model types deliver coupled multivariate polynomials, which may contain a large number of parameters, and thus become intuitively and physically hard to interpret.
\begin{figure}[p!]
\centerline{%
\includegraphics{preliminaries/figures/general_thesis_problem.pdf}}
\caption{Because a nonlinear model is defined ``negatively'', as a \emph{non linear} model, many different nonlinear model types can be considered and studied.
In this thesis, we focused our attention on three special model types (block-oriented models, NARX models and nonlinear state-space models), for which all state-of-the-art identification techniques yield coupled multivariate polynomials.
We propose several ways of decoupling these coupled polynomials, by using tensor decompositions as the main tool.
Noteworthy, the representation of the different nonlinear models in this figure should not be interpreted as disjunct Venn diagrams, as, for example, any NARX model could be rewritten as a nonlinear state-space model.}
\label{fig_general_thesis_problem}
\end{figure}

\subsection{Block-oriented models\label{sec_block_oriented_models}}
The class of block-oriented models is a general nonlinear model class consisting of a set of interconnected \emph{blocks}~\cite{Giri2010}.
Two types of blocks form the basic building blocks for block-oriented models:
\begin{enumerate}
	\item Linear time invariant (LTI) blocks are linear blocks $G$ which also satisfy the time invariance property: if the output at time $t$ for the input $u(t)$ is given by $y(t)$, then a delayed input $u(t-T)$ is also mapped to the same delayed output $y(t-T)$:
	$$
		G\bigl(u(t-T)\bigr) = y(t-T),
	$$
	for all delays $T\in\ER$.
	The major characteristic of LTI systems, is that they can be uniquely defined by the so-called \emph{transfer function} (for single-input single-output systems) and \emph{transfer function matrix} (for multiple-inputs multiple-outputs systems), which is the Laplace transform of the impulse response of the system~\cite{Oppenheim1983}.
	\item Static nonlinear (SNL) blocks $f\colon\ER\to\ER$ map solely current time values $u(t)$ to outputs $y(t)$ at the same time instant $t$.
\end{enumerate}
Combining these two types of blocks is possible in many different ways~\cite{Schoukens2016} and deliver block-oriented models, with varying degrees of complexity.
One specific interconnection of LTI and SNL deserves special attention, as they are universal approximators for fading memory systems~\cite{Palm1979,Schetzen2006}: the so-called parallel Wiener-Hammerstein models (Figure~\ref{fig_WH_system_rbranches_preliminaries}).
\begin{figure}[ht]
\centerline{
\includegraphics{preliminaries/figures/WH_system_rbranches.pdf}}
\caption{A parallel Wiener-Hammerstein consists of several parallel branches of so-called Wiener-Hammerstein structures.
Every branch surrounds a SNL block by two LTI blocks.
Parallel Wiener-Hammerstein models deserve special attention, as they are universal approximators for fading memory systems~\cite{Palm1979,Schetzen2006}.}
\label{fig_WH_system_rbranches_preliminaries}
\end{figure}
This type of model consists of several branches of Wiener-Hammerstein models, connected in parallel.
Any branch consists of a SNL block surrounded by two LTI blocks, connected in series.

Unfortunately, the state-of-the-art technique for identifying a parallel Wiener-Hammerstein model could not retrieve and identify its separate branches in one step~\cite{MaartenSchoukens2014}.
Instead, a \emph{coupled} static nonlinear function is modeled, together with LTI blocks surrounding it (Figure~\ref{fig_WH_coupled_system_rbranches_preliminaries}).
\begin{figure}[ht]
\centerline{\includegraphics{preliminaries/figures/WH_coupled_system_rbranches.pdf}}
\caption{The state-of-the-art identification method of parallel Wiener-Hammerstein models yields a coupled SNL function, surrounded by modeled LTI blocks.
In this thesis, we studied decoupled representations of this coupled nonlinearity, to find a decoupled parallel Wiener-Hammerstein model.}
\label{fig_WH_coupled_system_rbranches_preliminaries}
\end{figure}
Even though the resulted coupled models yielded small model errors, these models are hard to interpret, and have combinatorially many parameters.
In this thesis, we seek to find a \emph{decoupled} representation of this coupled static nonlinearity, in order to retrieve a \emph{decoupled parallel Wiener-Hammerstein model}.

\subsection{NARX models\label{sec_narx_models}}
The second nonlinear model type considered in this thesis are the so-called \emph{nonlinear autoregressive models with exogenous inputs} (NARX models)~\cite{Billings2013}.
The NARX models are a special case of the more general class of nonlinear autoregressive moving average models with exogenous inputs (NARMAX) and they are defined in discrete time, so where the time $t$ depends on the time instant $k$ and on fixed sample time: $t = k T_{\mathrm{s}}$.
Accordingly, the output~$y_k$ on time instant $k$ of a NARMAX model is given by a nonlinear function~$f$ of the following expressions:
\begin{itemize}
 	\item past outputs $y_{k-1}, \ldots, y_{k-n_y}$ ($n_y$ is the number of output lags),
 	\item the current and past inputs $u_{k-n_k}, \ldots, u_{k-n_k-n_u-1}$ ($n_u$ is the number of input lags and these are lagged by $n_k$),
 	\item and the noise sequence $e_{k-1}, \ldots, e_{k-n_e}$ ($n_e$ is the number of noise lags).
 \end{itemize}
In short, the output $y_k$ is given by
\begin{equation}
\begin{split}
y_k = f(&y_{k-1}, \ldots, y_{k-n_y}, \\
		&u_{k-n_k}, \ldots, u_{k-n_k-n_u-1}, \\
		&e_{k-1}, \ldots, e_{k-n_e})
		+ e_k.
\end{split}
\label{eq_narmax_models}
\end{equation}
In practice, several choices of the lag numbers $n_y$, $n_y$, and~$n_k$ are tested to find a good model candidate.
Furthermore, if we assume the noise in~\eqref{eq_narmax_models} to be i.i.d. and purely additive to the model, then NARMAX models boil down to NARX models (Figure~\ref{fig_narx_model}),
$$
y_k = f\bigl(y_{k-1}, \ldots, y_{k-n_y}, u_{k-n_k}, \ldots, u_{k-n_k-n_u-1}
\bigr)
+ e_k,
$$
which we consider in this thesis.
\begin{figure}[ht]
\centerline{
\includegraphics{preliminaries/figures/narx_model.pdf}}
\caption{Nonlinear autoregressive model with exogenous inputs contain a feedback loop, as the current output $y_k$ depends on preceding inputs and outputs.
In this model's name, this feedback loop $\hat y_k$, is reflected in the ``exogenous'' part.}
\label{fig_narx_model}
\end{figure}

The current state-of-the-art method of identifying a NARX (and NARMAX) model, starting from input and output measurements, uses an Orthogonal Least Squares (OLS) algorithm to choose which nonlinear terms should be added in the model~\cite{Billings2013,Billings1989,Billings1988}.
Choosing some terms to add in the models, while leaving others out, is important to keep the NARX model manageable: if the nonlinearity is a multivariate polynomial of degree~$d$, then the number of model terms (monomials) is given by 
$$
\ell = \binom{n+d}{n},
$$
where $n = n_y + n_u$ is the total number of input and output lags.
Therefore, the number of model terms grows combinatorially fast, and OLS is used to deliver usable (and often sparser) models.

The OLS algorithm works as follows, once the number of lags $n_y$, $n_u$ and $n_k$ are chosen:
\begin{enumerate}
	\item Create a set of orthogonal basis functions $\w_1, \ldots, \w_\ell$, using a Gram-Schmidt or Householder procedure~\cite{Chen1989};
	\item Choose the orthogonal monomial $\w_i$ which maximizes the so-called Error Reduction Ratio:
	$$
		\mathrm{ERR}_i = \frac{(\y \cdot \w_i)^2}{(\y \cdot \y)^2 (\w_i \cdot \w_i)^2},
	$$
	where $\y$ is the measured output of the system.
	\item Continue this process by choosing the next orthogonal monomial, over all remaining orthogonal monomials, which maximizes the Error Reduction Ratio, until a small enough model error is achieved.
	\item By linearly transforming the obtained model, the original (not orthogonal) model can be retrieved.
\end{enumerate}
To ensure that the order of the basis functions is independent during the OLS stage, all the unselected model terms thus far are searched for at each consecutive step.
This general search is called Forward Regression, and is the state-of-the-art NARX modeling technique known as Forward Regression Orthogonal Least Squares (FROLS)~\cite{Billings2013,Billings1989,Billings1988}.

The FROLS identification scheme yields usable NARX models, which often are sparse.
However, they can still contain a large number of parameters, and we studied a decoupling algorithm to represent a NARX model as linearly transformed single-input single-output functions.

Noteworthy, care should be taken when validating a model with exogenous inputs, like a NARX model, as two following model errors might be used.
When modeling the next output $\hat y_{k+1}$, one needs to use preceding outputs $y_k, \ldots, y_{k-n_y}$ in one of the following two ways:
\begin{enumerate}
	\item If one replaces the past outputs by the measured outputs 
	$$
	y_k, \ldots, y_{k-n_y},
	$$
	then the output error is called a \emph{prediction error}.
	However, in this case, the model only predicts the output one time step at a time, which is called \emph{one-step-ahead prediction}.
	This type of error is often smaller than the next one.
	\item If one replaces the past outputs by the simulated outputs 
	$$
	\hat y_k, \ldots, \hat y_{k-n_y},
	$$
	then the output error is called a \emph{simulation error}.
	This type of error is often more difficult to minimize, as the errors propagate in the simulated output signal.
\end{enumerate}
When using NARX models in this thesis, we will make the distinction between both types of modeling errors.

\subsection{Nonlinear state-space models\label{sec_state_space_models}}
The third and last model type studied and identified in this thesis is the so-called \emph{nonlinear state-space model}.
This model type is a nonlinear generalization of the state-space model: instead of considering solely linear transformations between inputs $\u$, states $\x$ and outputs $\y$~\cite{Systems1980}
$$
\left\{
	\begin{aligned}
		\x(t+1) &= \A \x(t) + \b u(t) \\
		\hat y(t) 	&= \c^T \x(t) + d u(t),
	\end{aligned}
\right.
$$
the nonlinear state-space model also considers nonlinear functions $\f_\x$ in the state equations, and $f_y$ in the output equations,
\begin{equation}
	\left\{
		\begin{aligned}
			\x(t+1) &= \A \x(t) + \b u(t) + \f_\x\bigl(\x(t), u(t)\bigr) \\
			\hat y(t) 	&= \c^T \x(t) + d u(t) + f_\y\bigl(\x(t), u(t)\bigr).
		\end{aligned}
		\right.
\label{eq_def_nl_state_space}
\end{equation}
Although more general formulations are possible, the equations~\eqref{eq_def_nl_state_space} consider solely single-input single-output nonlinear state-space models, so the matrix $\b\in\ER^{n_\x\times1}$ is a column vector, the matrix $\c^T\in\ER^{1\times n_\x}$ is a row vector, and the feedforward parameter~$d\in\ER$ is a scalar.
The number of states is denoted by~$n_\x$.

Several options for the basis functions of the nonlinear functions~$\f_\x$ and $f_y$ are possible, for example:
\begin{itemize}
	\item splines, piecewise linear functions, or sigmoids~\cite{Marconato2014},
	\item sinus functions~\cite{Svensson2017},
	\item polynomials~\cite{Paduart2010}.
\end{itemize}
In this thesis, we will focus the attention to polynomial basis functions, for which the state-of-the-art identification~\cite{Paduart2010,Paduart2006} technique is outlined below, and for which a MATLAB toolbox is implemented~\cite{Tiels2016,Tiels2016a}.

Identifying a polynomial state-space model makes use of the so-called Best Linear Approximation~\cite{Pintelon2012} (BLA), which is also the basis step of the parallel Wiener-Hammerstein identification~\cite{Schoukens2015,MaartenSchoukens2014,SchoukensDecember10--132013} (Section~\ref{sec_block_oriented_models}).
The best Linear Approximation of a nonlinear model is the linear model which best approximates the measured output, in least-squares sense, for a given class of input signals.
With a subspace-based method~\cite{McKelvey1996}, a parametric linear state-space model is estimated from this BLA model~\cite{Paduart2008}.
Finally, this linear state-space model becomes the starting point of a nonlinear optimization problem, which minimizes the nonlinear state-space model's output error, relative to the measured output.
In this last step, the degree of the polynomials are chosen by the user before the optimization process.

Even though the polynomial state-space models created with this identification method yield good results, a major drawback is the \emph{coupled} nonlinear function contained therein.
As for block-oriented models (Section~\ref{sec_block_oriented_models}) and NARX models (Section~\ref{sec_narx_models}), the goal is to create a \emph{decoupled} representation of state-space models.

A general formulation of the decoupling problem, as well as its links to other scientific and mathematical work, is the subject of the next section.

\section{Decoupling problem and related work\label{sec_decoupling_related_work}}
Central in the state-of-the-art nonlinear identification algorithms for the model types of this thesis, is a \emph{coupled multivariate polynomial} as current end product.
Unfortunately, working with coupled nonlinear functions can suffer from the following two disadvantages:
\begin{enumerate}
	\item When the number of nonlinear parameters increases, any physical or intuitive understanding of the model can be lost.
	Even though one may be interested in black-box models for certain applications, understanding the considered physical system is an important feature of models.
	\item When the number of nonlinear parameters increases, computational difficulties may arise.
	Even though computer speed increases with time, some applications need short computation times, and thus simpler models, yet keeping the modeling power as much as possible.
\end{enumerate}
Finding a decoupled representation of a coupled multivariate polynomial in the context of nonlinear system identification is the proposed approach to solve these disadvantages, and is the central subject of this thesis.

The decoupling problem for polynomials is defined as follows (Figure~\ref{fig_decoupling_process}): consider a multivariate polynomial $\f\colon\ER^m\to\ER^n$ of $m$ inputs and consisting of $n$ outputs: $\f(\u) = \bigl(f_1(\u), \ldots, f_n(\u)\bigr)$.
For this coupled function~$\f$, find two transformation matrices~$\V\in\ER^{m\times r}$ and~$\W\in\ER^{n\times r}$ and a set of single-input single-output functions~$\g = (g_1, \ldots, g_r)$, such that
\begin{equation}
	\f(\u) = \W \g(\V^T \u),
	\label{eq_decoupled_representation}
\end{equation}
for all inputs $\u$.
Equation~\eqref{eq_decoupled_representation} defines a \emph{decoupled representation} of~\f\ as linear transformations of univariate polynomials in linear forms of the input variables (Figure~\ref{fig_decoupling_process}).
Furthermore, the number~$r$ of so-called decoupled branches is in practice chosen by the user.
\begin{figure}[ht]
\centerline{
\includegraphics[width=\textwidth]{preliminaries/figures/decoupling.pdf}}
\caption{The decoupling process: given \f, find the matrices \V\ and \W, and the univariate functions~$g_1, \ldots, g_r$, such that $\f(\u) = \W \g(\V^T \u)$.}
\label{fig_decoupling_process}
\end{figure}

The most simple decoupling problem for nonlinear polynomials can be illustrated with the following example, a homogeneous polynomial of degree two:
$$
f(u_1,u_2) = a_1 u_1^2 + 2 a_2 u_1 u_2 + a_3 u_2^2.
$$
This homogeneous polynomial $f$ can be rewritten in matrix form as
$$
f(u_1,u_2) = 
\begin{bmatrix}
	u_1 & u_2
\end{bmatrix}
\underbrace{\begin{bmatrix}
	a_1 & a_2 \\
	a_2 & a_3
\end{bmatrix}}_\A
\begin{bmatrix}
	u_1 \\
	u_2
\end{bmatrix},
$$
and the matrix $\A$ can be factorized with the Cholesky decomposition into $\A = \L^T \L$.
Therefore, the output of $f$ can be rewritten as
\begin{equation}
f(u_1,u_2) = (\L \u)^T (\L \u),
\label{eq_degree2}	
\end{equation}
with $\u = (u_1,u_2)$, and Equation~\eqref{eq_degree2} is precisely the decoupled representation of $f$, with the following values for the decoupling parameters:
\begin{itemize}
	\item $\V = \L^T$,
	\item $\g(x_1, x_2) = (x_1^2, x_2^2)$,
	\item $\W = \begin{bmatrix}
		1 \\
		1
	\end{bmatrix}$.
\end{itemize}
Generalizing this simple decoupling problem to higher-degree (non-homogeneous) polynomials with possibly more outputs require a different set of techniques.

To our knowledge, several solutions to the decoupling problem have been proposed in the literature, but only under additional assumptions, discussed in this paragraph.
The interconnections between these latest existing tensorization methods in the context of decoupling multivariate polynomials have been summarized in~\cite{Usevich2017}.
For example, one single homogeneous polynomial can be decoupled with a symmetric tensor or its unfoldings~\cite{Brachat2010, Comon2008}. 
Also, in the non-homogeneous case, it is possible to decompose one single polynomial in the exact case~\cite{Bialynicki-Birula2008,Schinzel2002,Usevich2014}, while decoupling multiple non-homogeneous polynomials has been proposed by considering tensors filled with the polynomial coefficients~\cite{MaartenSchoukens2014,Tiels2013}.
Unfortunately, the last technique has the following two disadvantages:
\begin{enumerate}
	\item Because the coefficients of the polynomials are directly converted to a high-order tensor, only polynomials can be considered for the decoupling process.
	\item The order of the tensor is directly related the degree of the multivariate polynomials.
	With this method, tensors can easily have a very high order, which makes them difficult to compute and to represent.
\end{enumerate}
The state-of-the-art decoupling method, which solves these two disadvantages, makes use of the first-order derivative information of the coupled function, which is stacked together in a third-order tensor~\cite{Dreesen2015}.
If it is assumed that an exact decomposition exists, and under some constraints on the number of decoupled branches, this method is able to retrieve a decoupled representation of multivariate polynomials.
Furthermore, this promising method can theoretically be extended to non-polynomial nonlinearities as sinusoids, piecewise linear and saturating functions, and will be used as the starting point for this thesis' research (Figure~\ref{fig_interplay_cost_functions}).
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.6\textwidth]{preliminaries/figures/interplay_costs.pdf}
\end{center}
\caption{Central in this thesis' research, is the interplay between three levels of complexities, and their associated cost functions.
The outermost level lies at level of nonlinear system identification, and its cost function~$V_1$ is defined considering measured inputs and outputs of an unknown system to be identified.
In an intermediary level, the coupled nonlinear function should be decoupled, and its cost function~$V_2$ is defined in terms of the unknown internal variables $\V, \g$ and $\W$.
Finally, the innermost level is defined on the level of the Jacobian and their elements. This level corresponds with a third cost function~$V_3$ which can be optimized using tensor decomposition methods.
The interplay between the levels is created by consecutively use the optimized parameters of an inner level as the initialization of the next outer level.}
\label{fig_interplay_cost_functions}
\end{figure}

In the field of algebraic geometry, the task of decoupling a multivariate polynomial is related to the so-called Waring problem~\cite{Brachat2010,Comon1996,Iarrobino1999,Oeding2013}: given a homogeneous multivariate polynomial $f$ of degree~$d$ in~$m$ variables, find its decomposition as a linear combination of powers of multivariate polynomials:
$$
f(u_1, \ldots, u_m) = \sum_{i=1}^r  w_i \Bigl(\sum_{j=1}^m v_j u_j \Bigr)^d .
$$
In this decomposition, the minimal number of terms $r$ is called the Waring rank, and is equal to the tensor rank of the corresponding symmetric coefficient tensor \cite{Brachat2010}.

Also, in the field of machine and deep learning, the structure of decoupled representations is closely related to neural networks with one hidden layer~\cite{Rojas1996,Bishop1995}.
However, even though neural networks exist in practice with several types of activation functions, polynomials are rarely used.
Instead, neural networks tend to use sinusoids or arctan-like saturating functions as activation functions, with little to no parameters inside the hidden layer.
In the decoupling problem of multivariate polynomials, each decoupled branch contains sums of monomials, with corresponding parameters to be tuned.

Finally, variations of the decomposition have been investigated in the context of approximation theory~\cite{Logan1975,Osolkov2002}, machine learning~\cite{Andoni2014,Shin1995,Rendle2010}, and signal processing~\cite{Comon2015,Fantinato2017,Deville2015}.


