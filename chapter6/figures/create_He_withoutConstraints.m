load('Z:\2016-10-11 WH Benchmark\results with validation and coeffs\3branches.mat');
%% Plot 1
clf;
iter = 23;
eps = 0.001;
epsy = 2;

for branch = 1 : 3
% branch = 3;

xe = TakeRow(Ve(:,:,iter).' * uOperatingPoints, branch);
HeCol =  He(:,branch,iter);
subplot(3,1, branch);
plot(xe, HeCol, '.' , 'markersize', 3)
set(gca, 'xtick', [min(xe), max(xe)], 'xlim', [min(xe)-eps, max(xe)+eps], ...
    'xticklabel', {sprintf('%.2f', min(xe)), sprintf('%.2f', max(xe))}, ...
    'ytick', [min(HeCol), max(HeCol)], ...
    'yticklabel', {sprintf('%.2f', min(HeCol)), sprintf('%.2f', max(HeCol))}, ...
    'ylim', [min(HeCol)-epsy, max(HeCol)+epsy])
% title(['Branch ' num2str(branch)], 'Interpreter','LaTex')
xlabel(['$\mathbf{v_' num2str(branch) '}^T \mathbf{u}_{\mathbf{f}}$'], 'Interpreter','LaTex')
ylabel(['$\mathbf{h}_' num2str(branch) '$'], 'Interpreter','LaTex', 'rot',0)


end

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\He.tex', 'width', '0.8\textwidth', 'height', '8cm');
% , 'extraaxisoptions', 'ylabel style={rotate=-90}'

%% Plot 2
clf;
iter = 23;
eps = 0.1;
epsy = 0.01;

for branch = 1 : 3
% branch = 3;

xe = TakeRow(VeCoeffs(:,:,iter).' * uOperatingPoints, branch);
HeCol =  HeCoeffs(:,branch,iter);
subplot(3,1, branch);
plot(xe, HeCol, '.' , 'markersize', 3)
set(gca, 'xtick', [min(xe), max(xe)], 'xlim', [min(xe)-eps, max(xe)+eps], ...
    'xticklabel', {sprintf('%.2f', min(xe)), sprintf('%.2f', max(xe))}, ...
    'ytick', [min(HeCol), max(HeCol)], ...
    'yticklabel', {sprintf('%.2f', min(HeCol)), sprintf('%.2f', max(HeCol))}, ...
    'ylim', [min(HeCol)-epsy, max(HeCol)+epsy])
title(['Branch ' num2str(branch)], 'Interpreter','LaTex')
xlabel(['$\mathbf{v_' num2str(branch) '}^T \mathbf{u}_{\mathbf{f}}$'], 'Interpreter','LaTex')
ylabel(['$\mathbf{h}_' num2str(branch) '$'], 'Interpreter','LaTex', 'rot',0)


end

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\HeCoeffs.tex', 'width', '0.8\textwidth', 'height', '8cm');
% , 'extraaxisoptions', 'ylabel style={rotate=-90}'

%% Plot a non-monotonuous cost function
load('Z:\2016-10-11 WH Benchmark\results with validation and coeffs\5branches.mat');
plot(JvalCoeffs)
eps = 20;
ylabel('Cost function', 'Interpreter','LaTex', 'rot',0)
xlabel('Iteration step', 'Interpreter','LaTex')
set(gca, 'xtick', [0 length(JvalCoeffs)], ...
    'xlim', [-eps length(JvalCoeffs)+eps], ...
    'ytick', [0.45 0.65], ...
    'ylim', [0.45 0.65])

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\CostFunctionIncreases.tex', 'width', '0.4\textwidth', 'height', '4cm');
% , 'extraaxisoptions', 'ylabel style={rotate=-90}'

%% Plot a monotonuous cost function
load('Z:\2016-10-11 WH Benchmark\results with validation and coeffs\1branches.mat');
plot(JvalCoeffs)
eps = 20;
ylabel('Cost function', 'Interpreter','LaTex')
xlabel('Iteration step', 'Interpreter','LaTex')
set(gca, 'xtick', [0 length(JvalCoeffs)], ...
    'xlim', [-eps length(JvalCoeffs)+eps], ...
    'ytick', [0.7 0.8], ...
    'ylim', [0.7 0.8])

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\CostFunctionMonotonouous.tex', 'width', '0.4\textwidth', 'height', '4cm');
% , 'extraaxisoptions', 'ylabel style={rotate=-90}'








