% In this script, I process the data coming from the interplay experiments,
% so in the folder Z:\2016-03c WH Approximation\approximations. I want to
% see how the cost function looks like when using a random initial point or
% the CPD as initial point. 

% Gather the data
direc = 'Z:\2016-03c WH Approximation\approximations_sameInitValues';
expers = dir(fullfile(direc, '*.mat'));
for iter = 1 : length(expers)
   	LogWindow(iter, length(expers), 'iter');
    temp = load(fullfile(direc, expers(iter).name));
    costInit(iter,:)           = temp.costInit;
    costOptim(iter,:)          = temp.costOptim;
    costRand(iter,:)           = temp.costRand;
    r(iter)                    = temp.r;
    rtilde(iter)                = temp.rtilde;
%     costInitTensorlab(iter,:)  = temp.costInitTensorlab;
%     costOptimTensorlab(iter,:) = temp.costOptimTensorlab;
%     costInitNway(iter,:)       = temp.costInitNway;
%     costOptimNway(iter,:)      = temp.costOptimNway;
end
close;


%%
% Plot the data (all the data for all the experiments)
clf; hold on
for iter = 1 : length(expers)
    plot( costOptim(iter,:), costRand(iter,:), 'color', hsv2rgb([RandomReal([0 1], [1 1]) 1 1]), 'linestyle', '-', 'marker', 'o')
%     p = patch([costOptim(iter,1) costOptim(iter,end) costOptim(iter,end) costOptim(iter,1)], [costRand(iter,1) costRand(iter,1) costRand(iter,end) costRand(iter,end)], hsv2rgb([RandomReal([0 1], [1 1]) 1 1])); set(p, 'facealpha', 0.2);
    text(costOptim(iter,end), max(costRand(iter,:)), First(strsplit(expers(iter).name, '.')), 'hor', 'center', 'vert', 'bot', 'color', 'red');
end
plot([0 0.7], [0 0.7]);
hold off
xlabel('cost function with CPD intput');
ylabel('cost function with rand input');
title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])

%% Plot the data (the minima of every experiment)
clf; hold on
for iter = 1 : length(expers)
    plot( costOptim(iter,1), costRand(iter,1), 'linestyle', '-', 'marker', 'o')
%     p = patch([costOptim(iter,1) costOptim(iter,end) costOptim(iter,end) costOptim(iter,1)], [costRand(iter,1) costRand(iter,1) costRand(iter,end) costRand(iter,end)], hsv2rgb([RandomReal([0 1], [1 1]) 1 1])); set(p, 'facealpha', 0.2);
%     text(costOptim(iter,end), max(costRand(iter,:)), First(strsplit(expers(iter).name, '.')), 'hor', 'center', 'vert', 'bot', 'color', 'red');
end
plot([0 0.5], [0 0.5]);
hold off
xlabel('cost function with CPD intput');
ylabel('cost function with rand input');
title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])

%% Plot the data (the minima of every experiment)
clf; hold on
for iter = 1 : length(expers)
    plot( costInit(iter,:), repmat(costInitTensorlab(iter),1,10), 'linestyle', '-', 'marker', 'o')
%     p = patch([costOptim(iter,1) costOptim(iter,end) costOptim(iter,end) costOptim(iter,1)], [costRand(iter,1) costRand(iter,1) costRand(iter,end) costRand(iter,end)], hsv2rgb([RandomReal([0 1], [1 1]) 1 1])); set(p, 'facealpha', 0.2);
%     text(costInit(iter,1), costInitTensorlab(iter), First(strsplit(expers(iter).name, '.')), 'hor', 'center', 'vert', 'bot', 'color', 'red');
end
plot([0 0.7], [0 0.7]);
hold off
xlabel('cost function with CPD wTx intput');
ylabel('cost function with tensorlab input');
title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])


%% Plot the data (the minima of every experiment) and color the points
clf; hold on
for iter = 1 : length(expers)
    if rtilde(iter)/r(iter) > 0.55
        plot( costOptim(iter,1), costRand(iter,1), 'linestyle', '-', 'marker', 'x', 'color', 'red');
    else
        plot( costOptim(iter,1), costRand(iter,1), 'linestyle', '-', 'marker', '.', 'color', 'blue', 'markersize', 5);
    end
%     p = patch([costOptim(iter,1) costOptim(iter,end) costOptim(iter,end) costOptim(iter,1)], [costRand(iter,1) costRand(iter,1) costRand(iter,end) costRand(iter,end)], hsv2rgb([RandomReal([0 1], [1 1]) 1 1])); set(p, 'facealpha', 0.2);
%     text(costOptim(iter,end), min(costRand(iter,:)), First(strsplit(expers(iter).name, '.')), 'hor', 'center', 'vert', 'bot', 'color', 'red');
end
plot([0 0.5], [0 0.5]);
text(0.13, 0.36, '$\frac{\tilde r}{r} > 0.55$', 'interpreter', 'latex', 'color', 'red')
text(0.2, 0.16, '$\frac{\tilde r}{r} < 0.55$', 'interpreter', 'latex', 'color', 'blue')
hold off
set(gca, 'xlim', [0 0.4], 'ylim', [0 0.4], 'XTick', [0 0.4], 'yTick', [0 0.4])
xlabel('cost with CPD init');
ylabel('cost with rand init', 'rot',0,'position', [-0.01 0.2]);
% title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\results_update.tex', 'width', '0.8\textwidth', 'height', '6cm', 'extraaxisoptions', 'ylabel style={rotate=-90}');

%% Compare before the optimization with the CPD initialiation
clf; hold on
for iter = 1 : length(expers)
    if costInit(iter,1) < costRand(iter,1)
        plot( costInit(iter,1), costRand(iter,1), 'linestyle', '-', 'marker', '.', 'color', 'black', 'markersize', 5);
    else
        plot( costInit(iter,1), costRand(iter,1), 'linestyle', '-', 'marker', '.', 'color', 'black', 'markersize', 5);
    end
%     p = patch([costOptim(iter,1) costOptim(iter,end) costOptim(iter,end) costOptim(iter,1)], [costRand(iter,1) costRand(iter,1) costRand(iter,end) costRand(iter,end)], hsv2rgb([RandomReal([0 1], [1 1]) 1 1])); set(p, 'facealpha', 0.2);
%     text(costInit(iter,end), min(costRand(iter,:)), First(strsplit(expers(iter).name, '.')), 'hor', 'left', 'vert', 'bot', 'color', 'red');
end
plot([0 0.5], [0 0.5], 'k');
hold off
xlabel('cost with CPD init without optimizing further');
ylabel('cost with rand init', 'rot',0,'position', [-0.01 0.25]);
set(gca, 'xlim', [0 0.5], 'ylim', [0 0.5], 'XTick', [0 0.5], 'yTick', [0 0.5])
% title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\compare_with_init.tex', 'width', '0.8\textwidth', 'height', '6cm', 'extraaxisoptions', 'ylabel style={rotate=-90}');


