%% test example

V = [1 2;
     3 4];
W = [-2 3;
      1 -1];

u = sym('u', [2 1]);

x = V.'*u;

f = expand(W*[2*x(1)^2 + x(1) - 3;
-x(2)^2 - 3*x(2) + 1]);




%% decoupling without noise
% [We, Ve, He, ystar, relerr, it] = ...
%     CPdecoupleInOperatingPoints_weighted(uOperatingPoints, approximatedCoeffs, monomials, varSym, modelInfo, [], 'fullmatrix', oldLoad, lambda_weightForSecondSetOfEqs, [stepSizeTol_wTx, maxNwTx, wTxCommandWindowLog]);

%% Create plots for the nonlinear branches
NOperatingPoints = 20;
uOperatingPoints = RandomReal([-1,1], [2 NOperatingPoints]);
f_mf = matlabFunction(f, 'vars', u);

[X,Y] = meshgrid(-1:2/NOperatingPoints:1,-1:2/NOperatingPoints:1);
for k1 = 1 : size(X,1)
    for k2 = 1 : size(X,2)
        uTemp = num2cell([X(k1,k2), Y(k1,k2)].');
        ystar_one(k1, k2) = First(f_mf(uTemp{:}));
        ystar_two(k1, k2) = Last(f_mf(uTemp{:}));
    end
end


subplot(2,2,1)
surf(X,Y,ystar_one)
set(gca, 'xtick', [-1 1], 'ytick', [-1 1], 'ztick', [min(min(ystar_one)) max(max(ystar_one))], 'zticklabel', [round(min(min(ystar_one))) round(max(max(ystar_one)))])
title('$f_1(u_1,u_2)$', 'interpre', 'latex')
xlabel('$u_1$', 'interpre', 'latex')
ylabel('$u_2$', 'interpre', 'latex')
subplot(2,2,2)
surf(X,Y,ystar_two)
set(gca, 'xtick', [-1 1], 'ytick', [-1 1], 'ztick', [min(min(ystar_two)) max(max(ystar_two))], 'zticklabel', [round(min(min(ystar_two))) round(max(max(ystar_two)))])
title('$f_2(u_1,u_2)$', 'interpre', 'latex')
xlabel('$u_1$', 'interpre', 'latex')
ylabel('$u_2$', 'interpre', 'latex')

subplot(2,2,3)
plot(-5:2/NOperatingPoints:5, -1*(-5:2/NOperatingPoints:5).^2 -3* (-5:2/NOperatingPoints:5) - 1)
title('$g_1(x_1)$', 'interpre', 'latex')
xlabel('$x_1$', 'interpre', 'latex')
set(gca, 'xlim', [-4 0.5], 'xtick', [-4 0.5], 'ytick', [-6 2]);
subplot(2,2,4)
plot(-5:2/NOperatingPoints:5, 2*(-5:2/NOperatingPoints:5).^2 + (-5:2/NOperatingPoints:5) - 3)
set(gca, 'xlim', [-3 1.5], 'xtick', [-3 1.5], 'ytick', [-5 15] );
title('$g_2(x_2)$', 'interpre', 'latex')
xlabel('$x_2$', 'interpre', 'latex')

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\noiseless_decoupling_example.tex', 'width', '0.8\textwidth', 'height', '10cm');


