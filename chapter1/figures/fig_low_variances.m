cwd;
maxx = 7;
maxx2 = 3;
N = 400;
sigma = 0.06;
theta_uncorr = 1.2*randn(N,2);
theta_corr = randn(N,1);
theta_corr2 = [theta_corr 0.7*theta_corr];
theta_corr = [theta_corr 0.7*theta_corr + sigma*randn(N,1)];



%% plot1
plot(theta_uncorr(:,1), theta_uncorr(:,2), '.', 'markersize', 3)
set(gca, 'xlim', [-maxx maxx], 'ylim', [-maxx maxx], 'xtick', [], 'ytick', [])
xlabel('$\theta_1$', 'interpret', 'latex');
ylabel('$\theta_2$', 'interpret', 'latex');
axis square;
% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\fig_low_variances_1.tex', 'width', '4cm', 'height', '4cm');

%% plot2
plot(theta_corr(:,1), theta_corr(:,2), '.', 'markersize', 3)
set(gca, 'xlim', [-maxx2 maxx2], 'ylim', [-maxx2 maxx2], 'xtick', [], 'ytick', [])
xlabel('$\theta_1$', 'interpret', 'latex');
ylabel('$\theta_2$', 'interpret', 'latex');
axis square;

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\fig_low_variances_2.tex', 'width', '4cm', 'height', '4cm');


%% plot3
plot(theta_corr2(:,1), theta_corr2(:,2), '.', 'markersize', 3)
set(gca, 'xlim', [-maxx2 maxx2], 'ylim', [-maxx2 maxx2], 'xtick', [], 'ytick', [])
xlabel('$\theta_1$', 'interpret', 'latex');
ylabel('$\theta_2$', 'interpret', 'latex');
axis square;

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\fig_low_variances_3.tex', 'width', '4cm', 'height', '4cm');