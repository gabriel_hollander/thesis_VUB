function vpos = write_text_cell(text_cell, hpos, hindent, vpos, vskip_line)
    for iter_lines = 1 : length(text_cell)
        text('string', text_cell{iter_lines}, 'pos', [hpos, vpos]);
        vpos = vpos - vskip_line;
        if iter_lines == 1, hpos = hpos + hindent; end
    end
end
