%% This script plots the intnal functions g_i for the Bouc-Wen, (parallel) Wiener-Hammerstein models (Chapter 6)
% Loading 

Nbranches = 2;
load(['Z:\2016-10-27 Parallel WH Benchmark\results with cCoeffs\' num2str(Nbranches) 'branches.mat']);

%%
clf;
color = ['r', 'g', 'b'];
ksi = modelOptimizedCoeffs_decoupled.Vd.' * uOperatingPoints;


for iter = 1 : size(ksi, 1)
    hold on
        g(iter,:) = polyval(modelOptimizedCoeffs_decoupled.gcoef(iter,:), ksi(iter,:));
        plot(ksi(iter,:), g(iter,:), [color(iter) '.'], 'markersize', 3)
    hold off
end
xlabel('$\mathbf{v}^T_i \mathbf{u}_{\mathbf{f}}$', 'interpreter', 'latex')
ylabel('$g_i(\mathbf{v}^T_i \mathbf{u}_{\mathbf{f}})$', 'interpreter', 'latex')
set(gca, 'xlim', [min(min(ksi)) max(max(ksi))], 'xtick', [min(min(ksi)) 0 max(max(ksi))], ...
    'xticklabel', arrayfun(@(x)(sprintf('%.0f', x)), [min(min(ksi)) 0 max(max(ksi))], 'uni', 0), ...
    'ylim', [min(min(g)) max(max(g))], 'ytick', [min(min(g)) 0 max(max(g))], ...
    'yticklabel', arrayfun(@(x)(sprintf('%.3f', x)), [min(min(g)) 0 max(max(g))], 'uni', 0))

% CurrentFigure2pdf(['C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\pWH_gi_' num2str(Nbranches) 'branches.tex'], 'width', '0.8\textwidth', 'height', '3cm');



