% In this script, I process the data coming from the interplay experiments,
% so in the folder Z:\2016-03c WH Approximation\approximations. I want to
% see how the cost function looks like when using a random initial point or
% the CPD as initial point. 

% Gather the data
direc = 'Z:\2016-03c WH Approximation\approximations_sameInitValues';
expers = dir(fullfile(direc, '*.mat'));
for iter = 1 : length(expers)
   	LogWindow(iter, length(expers), 'iter');
    exper{iter} = load(fullfile(direc, expers(iter).name));
%     costInit(iter,:)  = temp.costInit;
%     costOptim(iter,:) = temp.costOptim;
%     costRand(iter,:)  = temp.costRand;
end
close;


%%
% Plot the data
clf; hold on
for iter = 1 : length(expers)
    plot(exper{iter}.costOptim(1), exper{iter}.costRand(1), 'marker', iff(Rescale(exper{iter}.rtilde/exper{iter}.m, [0 1]) < 0.55, '.', 'x'), ...
    'markersize', 4, ...
    'color', iff(Rescale(exper{iter}.rtilde/exper{iter}.m, [0 1]) < 0.55, 'red', 'blue'), ...
    'linesmoothing', 'on')
%     text(exper{iter}.costOptim(1), exper{iter}.costRand(1), num2str(Rescale(exper{iter}.rtilde/exper{iter}.m, [0 1])), 'hor', 'center', 'vert', 'bot', 'color', 'red');
end
% plot([0 0.5], [0 0.5]);
text(0.07, 0.48, '$\frac{\tilde r}{r} > 0.55$', 'interpreter', 'latex')
text(0.3, 0.26, '$\frac{\tilde r}{r} < 0.55$', 'interpreter', 'latex')
hold off
set(gca, 'xtick', [0 0.5], 'ytick', [0 0.5])
xlabel('cost with init 1');
ylabel('cost with init 2');
axis square;
% title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])

% CurrentFigure2pdf('Z:\2016-07-22 LVA ICA\Paper\Figures\results.tex', 'width', '0.9\textwidth', 'height', '6cm', 'extraaxisoptions', 'ylabel style={rotate=-90}')
% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter4\figures\results.tex', 'width', '0.9\textwidth', 'height', '6cm')

%%
% Plot the data
clf; hold on
for iter = 1 : length(expers)
    plot(exper{iter}.costOptim(1), exper{iter}.costRand(1), 'marker', '.', 'markersize', 20, ...
    'color', hsv2rgb([Rescale(exper{iter}.rtilde/exper{iter}.m, [0 1]), 1 1]), 'linesmoothing', 'on')
    text(exper{iter}.costOptim(1), exper{iter}.costRand(1), num2str(Rescale(exper{iter}.rtilde/exper{iter}.m, [0 1])), 'hor', 'center', 'vert', 'bot', 'color', 'red');
end
plot([0 0.5], [0 0.5]);
Nrect = 200;
rectH = linspace(0.25, 0.45, Nrect);
rectV = 0.1;
rectHeight = 0.005;
iterr = 0;
for pos = rectH
    iterr = iterr + 1;
    rectangle('position', [rectH(iterr), rectV, diff(rectH(1:2)), rectHeight], 'linestyle', 'none', 'facecolor', hsv2rgb([Rescale(pos, [rectH(1) rectH(end)]), 1 1]))
end
text(rectH(1), rectV + rectHeight, 'st', 'vert', 'bottom')
hold off
xlabel('cost function with CPD intput');
ylabel('cost function with rand input');
title([ num2str(length(expers)) ' experiments, 10 random starting points and 1 CPD startnig point'])



