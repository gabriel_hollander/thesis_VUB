% create a figure for chapter 5

load('Z:\2017-03-08 NARX models\With David Westwick\Gabriel_s decouplings ALS\decoupling_data.mat');

%%
plot(JvalCoeffs{5})
set(gca, 'xtick', [0 10000], 'ylim', [0 0.6], 'ytick', [0 0.6], 'xticklabel', {'0', '10000'})
ylabel('Normalized cost function (5.4)');
xlabel('Iterations during ALS');

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\fig_nonconverging_costfunction.tex', 'width', '0.8\textwidth', 'height', '5cm');


