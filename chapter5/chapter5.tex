\chapter{Identifying decoupled NARX models}\label{chap_many_branches}

\abstracttext{%
\noindent The general class of autoregressive exogenous models (ARX) considers the next modeled output as a function of past outputs and inputs.
Examples of nonlinear ARX (NARX) models are the Volterra series, block-oriented models and recurrent neural networks~\cite{Billings2013}.
When considering a NARX model, the number of (nonlinear) parameters might increase considerably, which hardens the physical or intuitive interpretation of the model.
To decrease the number of parameters, we searched for a decoupled representation of a NARX model by adjusting the proposed decoupling approach: instead of decomposing a tensor, we considered the decomposition of a matrix.
This chapter gives an overview of the mathematical procedure going from tensors to matrices.
Furthermore, it compares two different matrix decomposition techniques on a case study experiment.
\newline
This chapter is organized as follows: Section~\ref{sec_chap5_problem_statement} states the mathematical problem solved in this chapter and Section~\ref{sec_decoupled_narx_methods} surveys two possible methods to do so.
Further, Section~\ref{sec_case_study_narx_id} compares both methods on a case study and Section~\ref{sec_chap5_future_work} concludes this chapter by proposing future possible work directions.
}


\section{Problem statement\label{sec_chap5_problem_statement}}
As usual in the field of system identification, the starting point in this chapter is a set of measured inputs~$\u = (u_1, \ldots, u_N)$ and outputs~$\y = (y_1, \ldots, y_N)$ of an unknown system.
From these measurements, we wish to identify a so-called \emph{decoupled nonlinear autoregressive exogenous} (decoupled NARX) model.
More specifically, our starting point is a (coupled) NARX model~\cite{Billings1981,Leontaritis1985}, generally denoted as
\begin{equation}
y_k = f\bigl(y_{k-1}, \ldots, y_{k-n_y}, u_{k-n_k}, \ldots, u_{k-n_k-n_u+1}
\bigr)
+ e_k
\label{eq_narx_coupled} \end{equation}
for a multiple-input single-output function~$f\colon\ER^{n_y+n_u}\to\ER$, for which we search for a decoupled representation.
In Equation~\eqref{eq_narx_coupled}, the output~$y$ at time $k$ is function of
\begin{center}
	\begin{tabular}{r|l}
		$n_y \ge 0$ & past outputs, starting with one delayed output, \\
		$n_u \ge 0$ & past inputs, starting with \\
		$n_k \ge 0$ & delays.
	\end{tabular}
\end{center}
In the case $n_y = 0$, no delayed outputs are taken into account in the function~$f$.
Furthermore, we assume the noise $e_k$ to be i.i.d.

For the purposes of this chapter, we wish to reduce the number of parameters of coupled NARX models, and make them intuitively easier to grasp, by searching for a \emph{decoupled} representation with a certain number~$r$ of decoupled branches (Figure~\ref{fig_decoupled_NARX}).
That is, we are searching for a transformation matrix~$\V = \begin{bmatrix}
	\v_1 & \cdots & \v_r
\end{bmatrix} \in \ER^{(n_y+n_u)\times r}$ and a set of SISO functions~$\g = (g_1, \ldots, g_r)$ of degree~$d$, such that
\begin{equation}
	\hat\y = \sum_{i=1}^r g_i
	\bigl(\v_i^T (y_{k-1},\ldots, y_{k-n_y}, u_{k-n_k}, \ldots,u_{k-n_k-n_u+1})
	\bigr).
\label{eq_NARX_decoupled_output}	
\end{equation}
For the rest of this chapter, we will assume the SISO functions $\g$ to be of degree~3 (Section~\ref{sec_overview_of_thesis}).

Noteworthy, unlike in the other chapters and models of this thesis, the second transformation matrix~\W\ is not used for decoupled NARX models, as the outputs of the different functions~$g_i$ are added to create the output~$\hat\y$ of the model.
Accordingly, a decoupled NARX model could define the matrix~\W\ as an column vector consisting solely of $r$ 1's, but this would add $r$ superfluous parameters in the optimization problems discussed in this chapter, without adding new and meaningful information.
\begin{figure}[ht]
\begin{center} \includegraphics{chapter5/figures/decoupled_narx.pdf} \end{center}
\caption{A coupled NARX model has the general form $\hat y_k = f(y_{k-1},\ldots, y_{k-n_y}, u_{k-n_k}, \ldots,u_{k-n_k-n_u+1})$.
On the other hand, a decoupled NARX model consists of one transformation matrix~\V\ transforming the input~\u\ to the inputs of SISO functions~$g_1, \ldots, g_r$.
Accordingly, the SISO functions' outputs are added together to create the model's output~$\hat\y$.
In short, a decoupled NARX model can be represented by $\hat y_k = \sum_{i=1}^r g_i(\v_i^T\x)$.
As a shorthand notation, $\z$ represents all the input variables of the function~$f$.
}
\label{fig_decoupled_NARX} 
\end{figure}

In short, this chapter considers the following mathematical problem.
Given the inputs~\u\ and outputs~\y\ of a system, we are searching for a transformation matrix~\V\ and SISO functions~\g, such that the following normalized cost function is minimized:
\begin{equation}
	\minimize_{\V,\g} \frac{\norm{\hat\y - \y}^2}{\norm{\y}^2},
	\label{eq_chap5_optimization_problem}
\end{equation}
with $\hat\y$ the decoupled modeled output, Equation~\eqref{eq_NARX_decoupled_output}.
The number~$r$ of decoupled branches is chosen by the user, and varies from application to application (Section~\ref{sec_case_study_narx_id}).
In the next section, we give an overview of two possible techniques for identifying decoupled NARX models.

\section[Identification methods for decoupled NARX model]{Identification methods for decoupled \\
NARX model\label{sec_decoupled_narx_methods}}
In this section, we propose two identification methods for modeling decoupled NARX models, both of which consist of a few common steps.
The starting point is an identified coupled NARX model (Section~\ref{sec_narx_models}) which we then decouple, considering the first-order derivative information of the coupled function~$f$ evaluated at a set of operating points (Section~\ref{sec_creation_jacobian_matrix}).
During the proposed decoupling process, the function~$f$ is considered as an $m$-input 1-output coupled function, with $m = n_y + n_u$.
Afterwards, the decoupled models are validated using prediction and simulation errors.

The so-called Jacobian matrix evaluated in the operating points is decomposed into a low-rank approximation using one of the two following variants:
\begin{enumerate}
	\item a so-called relaxed alternating least-squares method (Section~\ref{sec_als_decompose}),
	\item or a separable least-squares method (Section~\ref{sec_sls_decompose}).
\end{enumerate}
After an integration step (similar to step~\ref{item_recovery_of_gi} of Algorithm~\ref{algor_exact_decoupling}), the result is processed as the starting point for the optimization problem~\eqref{eq_chap5_optimization_problem}.
The final optimization ressembles to the problem~\eqref{2eq_cost_function} in Section~\ref{2sec_problem_statement}, and optimizes all the model parameters further, in order to minimize the modeled output errors.
In the following sections, we focus our attention to the parts of this algorithm differing from the other chapters of this thesis.


\subsection{Creating the Jacobian matrix\label{sec_creation_jacobian_matrix}}
Instead of a three-dimensional Jacobian tensor as in the other chapters of this thesis, the multiple-input single-output coupled function~$f$ creates a so-called Jacobian matrix \J\ of dimensions $m \times N$.
The number~$N$ of operating points is (heuristically) defined large enough and the operating points $\z^{(1)}, \ldots, \z^{(N)}$ are chosen uniform randomly in the region of interest.
The $i$-th column of \J\ contains all the partial derivatives of~$f$ evaluated in the~$i$-th operating point.

The following two Sections~\ref{sec_als_decompose} and~\ref{sec_sls_decompose} propose two methods for approximating the Jacobian matrix~\J\ as a product of two low-rank matrices.
The rank~$r$ of the low-rank factors represents the number of decoupled branches of the NARX decoupled model and several values of~$r$ should be tested in practice.
Finally, Section~\ref{sec_overview_narx_id} summarizes our proposed identification method.

\subsection{Using relaxed ALS to decompose the Jacobian matrix\label{sec_als_decompose}}
The first low-rank decomposition technique of the Jacobian matrix~\J\ is a kind of relaxed ALS: it is an adaptation on the CPD for higher-order tensors (Section~\ref{sec_tensors_and_decompositions}) but contains only two factors~$\V\in\ER^{m\times r}$ and $\H\in\ER^{N\times r}$, instead of three factors in the case of three-dimensional tensors.
The first factor~\V\ immediately translates to the transformation matrix of the decoupled representation (Figure~\ref{fig_decoupled_NARX}), while the second factor represents column-wise the derivatives of~$\g$ evaluated in the points~$\v_1^T\z, \ldots, \v_N^T\z$, analoguous to Algorithm~\ref{algor_exact_decoupling}.
The relaxed ALS described in this section related to the so-called structured low-rank approximation (SRLA) problem~\cite{Markovsky2008}.
Other methods for finding structured low-rank approximations of matrices have been studied, for example using regularization~\cite{Ishteva2014} or variable projections~\cite{Usevich2014a}.

Decomposing the Jacobian matrix~\J\ is translated mathematically into the optimization problem
\begin{equation}
	\minimize_{\V,\H} \normf{\J - \V\H^T}^2,
	\label{eq_low_rank_costfunction}
\end{equation}
with $\normf{\cdot}$ the Frobenius norm.
This nonlinear and nonconvex optimization problem is a special case of problem~\eqref{eq_cpd_cost_function} for tensors.
To optimize the nonlinear problem~\eqref{eq_low_rank_costfunction}, we propose a relaxed variant of the Alternating Least Squares method, which is the workhorse algorithm for the generalized tensor problem.
In the case of matrix decomposition, the ALS algorithm updates one factor, while keeping the other constant, and vice versa:
\begin{enumerate}
	\item update of \V\ in the approximation problem $\J \approx \V\H^T$;
	\item update of \H\ in the approximation problem $\J^T \approx \H\V^T$.
\end{enumerate}
These two approximations problems arise from the two different matricization orders of the matrix~$\J$: with the same notations as in~\cite{Kolda2009}, the first matricization~$\J_{(1)}$ boils down to $\J$, while the second matricization~$\J_{(2)}$ boils down to~$\J^T$.


Even though the least-squares update of \V\ is straightforward and given by 
$$
\hat\V = (\H^\dagger\J_{(1)}^T)^T,
$$
this approach ignores the dependencies of $\H$ on the entries of $\V$ (see further), and hence, it is a relaxed version of the ALS method.
Furthermore, to update the matrix~\H\ in a noisy practical setting, we impose structure at every iteration step during the ALS iterations.
As every column of~\H\ represents the evaluated derivatives of~$f$, we assume the $i$-th column~$\h_i$ of~\H\ to be written directly as a linear combination of unknown coefficients~$\c$ and the Vandermonde matrices~$\X_i$:
\begin{equation}
	\H = \begin{bmatrix}
		\h_1 \cdots \h_r
	\end{bmatrix}
	=
	\begin{bmatrix}
		\X_1\c_1 \cdots \X_r\c_r
	\end{bmatrix},
	\label{eq_H_function_of_c}
\end{equation}
with the $i$-th Vandermonde matrix given by
$$
	\X_i = 
	\left.\frac{\partial g_i}{\partial c}\right|_{\z^{(1)}, \ldots, \z^{(N)}}
	=
	\begin{bmatrix}
	1 & \v_i\z^{(1)} & \Bigl(\v_i\z^{(1)}\Bigr)^2 & \cdots & \Bigl(\v_i\z^{(1)}\Bigr)^{d-1} \\
	\vdots & \vdots & \vdots & \ddots & \vdots \\
	1 & \v_i\z^{(j)} & \Bigl(\v_i\z^{(j)}\Bigr)^2 & \cdots & \Bigl(\v_i\z^{(j)}\Bigr)^{d-1} \\
	\vdots & \vdots & \vdots & \ddots & \vdots \\
	1 & \v_i\z^{(N)} & \Bigl(\v_i\z^{(N)}\Bigr)^2 & \cdots & \Bigl(\v_i\z^{(N)}\Bigr)^{d-1}
\end{bmatrix}.
$$

Equation~\eqref{eq_H_function_of_c} writes the matrix~\H\ as a function of the vectorized coefficients~$\c$:
$$
\c = 
\begin{bmatrix}
	\c_1 \\
	\vdots \\
	\c_r
\end{bmatrix},
$$
and we propose to update the coefficients~$\c$ directly, and computing the factor~\H\ afterwards.
Finding an approximation of the coefficient vector~$\c$ is possible by vectorizing the problem $\J_{(2)} = \J^T \approx \H\V^T$ into
$$
\myvec{\J^T} \approx \myvec{\H\V^T}.
$$
With the well-known Kronecker product's properties~\cite{Kolda2009}, the following calculations can be made:
\begin{align}
	 \myvec{\H\V^T} &= \myvec{\I_N \H \V^T} \nonumber\\
	 &= (\V\otimes\I_N) \, \myvec{\H} \nonumber\\
	 &= (\V\otimes\I_N) \, \begin{bmatrix}
	 	\h_1 \\
	 	\vdots \\
	 	\h_r
	 \end{bmatrix} \nonumber\\
	 &= (\V \otimes \I_N) \begin{bmatrix}
		\X_1 \\
		& \ddots \\
		&& \X_r
	\end{bmatrix}
	\begin{bmatrix}
		\c_1 \\
		\vdots \\
		\c_r
	\end{bmatrix},\label{eq_c_coefficients}
\end{align}
with $\I_N$ the identity matrix of dimensions $N\times N$.
Finally, from these calculations, the update of~$\c$ can be computed by
$$
	\hat\c = 
\left(
(\V \otimes \I_N)
\begin{bmatrix}
		\X_1 \\
		& \ddots \\
		&& \X_r
	\end{bmatrix} 
	\right)^{\!\!\!\dagger}
\myvec{\J^T}.
$$

Section~\ref{sec_case_study_narx_id} compares the proposed relaxed ALS technique developed in this section with next section's separable least-squares technique.
Furthermore, Chapter~\ref{chap_real_measurements} discusses some implications of the imposed structure on~\H, in the more general setting of three-dimensional tensor decompositions.

\subsection{Using separable least-squares to decompose the Jacobian matrix\label{sec_sls_decompose}}
The second matrix decomposition method of the Jacobian matrix~\J\ uses the so-called \emph{separable least-squares} method.
This method was developed by David Westwick during his two-month long stay at the department ELEC of the Vrije Universiteit Brussel, from March to April 2017, and was presented at the second Workshop on nonlinear system identification benchmarks at the VUB~\cite{Westwick2017}.
This section summarizes this method.

The separable least-squares method is a special case of the more general framework of constrained least square.
In the latter framework, a cost function~$V(\theta)$ is function of parameters $\theta$, which can be divided into two categories
\begin{enumerate}
	\item Some of the parameters are linear, forming the subset $\theta_l$. In Equation~\eqref{eq_c_coefficients}, the parameters $\c_1, \ldots, \c_r$ appear linearly, so these were used as linear parameters in the separable least-squares method.
	\item Some of the parameters are nonlinear, forming the subset $\theta_n$. Since~$\H$ depends nonlinearly on $\V$ and $\J = \V\H^T$, the nonlinear parameters are the entries of~$\V.$
\end{enumerate}
With separable least-squares, the linear parameters $\theta_l$ can be written in closed form as functions of the remaining nonlinear parameters~\cite{Hughes2005}.
Therefore, a \emph{separated cost function}~$V_{\mathrm{SN}}(\theta_n)$ is created, which can be optimized with a quasi-Newton algorithm.
Under mild assumptions~\cite{Golub1973}, the minima of the separated cost function~$V_{\mathrm{SN}}(\theta_n)$ are equivalent with those of the original cost function~$V(\theta)$, yielding local mimina for the original least-squares problem.
Hence, once the nonlinear paramaters~$\theta_n$ are found, the linear parameters can be computed from their closed-form solution, thus yielding a solution for the original problem.

The biggest advantage of separating the linear from the nonlinear parameters is the reduction in the number of parameters.
Section~\ref{sec_case_study_narx_id} surveys the outcomes of our NARX experiments with both methods.

\subsection{Overview of decoupling NARX models\label{sec_overview_narx_id}}
This section summerizes the proposed decoupling method for NARX models (Algorithm~\ref{algor_narx_identification}).
The starting point for this algorithm are the measured or simulated inputs~\u\ and outputs~\y\ for which we wish to identify a decoupled NARX model.
Furthermore, we assume a coupled NARX model to be available (Section~\ref{sec_narx_models}), which we decouple with Algorithm~\ref{algor_narx_identification}.

\begin{algor}{Decoupling a NARX model with $r$ branches.}\label{algor_narx_identification}
\begin{enumerate}
	\item Construct the Jacobian matrix~\J\ of~$f$ in~$N$ operating points (Section~\ref{sec_creation_jacobian_matrix});
	\item Decompose the Jacobian matrix, either with relaxed ALS (Section~\ref{sec_als_decompose}) or with separable least-squares (Section~\ref{sec_sls_decompose});
	\item Integrate the resulting vector~\c\ of coefficients to retrieve the coefficients of the internal functions~$g_1, \ldots, g_r$;
	\label{step_integration}
	\item Use the end result of step~\ref{step_integration} as initialization point for the optimization problem~\eqref{eq_chap5_optimization_problem}.
\end{enumerate}
\end{algor}

\section[Case study for the decoupled NARX model identification]{Case study for the decoupled NARX\\ model identification\label{sec_case_study_narx_id}}
To compare the two proposed decomposition techniques (Sections~\ref{sec_als_decompose} and~\ref{sec_sls_decompose}), we apply the decoupled NARX identification method on the simulated data from the so-called Bouc-Wen data~\cite{Noel2017,Esfahani2017}.
First, a general overview of the Bouc-Wen model is given.
Then, decoupled NARX models are generated with the proposed technique of Sections~\ref{sec_als_decompose} and~\ref{sec_sls_decompose}.

\subsubsection{General overview of the Bouc-Wen model}
The Bouc-Wen model is used to represent hysteretic effects.
These effects are found in several engineering fields~\cite{Morrison2001} and human sciences~\cite{Angeli2004}, and consist of multiple stable equilibria, causing input-output loops, as the input frequency approaches zero.
Systems showing hysteretic effects are inherently nonlinear, and can be referred to as systems with \emph{nonlinear memory.}

A Bouc-Wen model with a single degree of freedom or a Bouc-Wen oscillator with a single mass, is described by the second-order differential equation in continuous time~\cite{Bouc1967} and~\cite{Wen1976},
\begin{equation}
	m_L \ddot{y}(t) + \underbrace{c_L \dot{y} + k_L y}_{\hbox to 0cm{\scriptsize linear restoring force}} + z(y,\dot{y}) = u(t),
	\label{eq_bouc_wen}
\end{equation}
with the hysteretic force $z(y, \dot{y})$ given as the first-order differential equation
\begin{equation}
	\dot{z}(y, \dot{y}) = \alpha \dot{y} - \beta \bigl( \gamma \abs{\dot{y}} \abs{z}^{\nu-1} z + \delta \dot{y} \abs{z}^\nu \bigr).
	\label{eq_bouc_wen_2}
\end{equation}
In equations~\eqref{eq_bouc_wen} and~\eqref{eq_bouc_wen_2}, the following notations are used:
\begin{center}
	\begin{tabular}{r|l}
		$y$   & displacement \\
		$u$   & external force \\
		$m_L = 2 \textrm{ kg}$ & mass constant \\
		$k_L = 5\cdot 10^4 \textrm{ N}/{\textrm{m}}$ & linear stiffness coefficient \\
		$c_L = 10$ & viscous damping coefficient \\
		$z(y, \dot{y})$ & hysterestic force \\
		$\alpha = 5\cdot 10^4$ & Bouc-Wen parameter \\
		$\beta = 10^3$ & Bouc-Wen parameter \\
		$\gamma = 0.8$ & Bouc-Wen parameter \\
		$\delta = -1.1$ & Bouc-Wen parameter \\
		$\nu = 1$ & Bouc-Wen parameter \\
	\end{tabular}	
\end{center}
The values used for this case study are noted next to each parameter.
For the Bouc-Wen parameters $\beta$, $\gamma$ and~$\delta$, SI units are not uniquely defined.
Hence, we do not include the units in the table.

\subsubsection{Creating a model for the Bouc-Wen measurements}
Modeling the Bouc-Wen measurements with a (decoupled) NARX model happens in a discrete time framework, and two possible error criteria can be used (Section~\ref{sec_narx_models}).
The next modeled output~$\hat y_{k+1}$ depends (among others) on the past model outputs~$\hat y_1, \ldots, \hat y_k$ in one of the two following ways:
\begin{enumerate}
	\item If the past model outputs are replaced by the measured outputs~$y_1$, \ldots, $y_k$, then the output is called a prediction.
	The error corresponding to this a prediction output is the so-called \emph{prediction error}.
	\item If the past model outputs are replaced by simulated outputs, then the output is called simulation, and its associated error is the so-called \emph{simulated error}.
\end{enumerate}

Although not accepted as good practice, we have used the same signal for the estimation and validation of the model, as the results with the relaxed ALS method yield unstable models (see further).
This input signal was a random phase multisine with a sampling frequency of 750 Hz~\cite{Noel2017}.
Its steady-state period consists of~8192 samples and it is excited between the frequencies of~5 and~150 Hz (Figure~\ref{fig_input_benchmark}).
\begin{figure}[ht]
\centerline{\includegraphics{chapter5/figures/plot_benchmark1.pdf}}
\centerline{\includegraphics{chapter5/figures/plot_benchmark2.pdf}}
\caption{The input signal used for the Bouc-Wen identification was a random-phase multisine.
In the time domain (top), it looks like noise, but its structure appears in the frequency domain (bottom).
It consists of 8192 points sampled at 750 Hz, and it is excited between the frequency lines of~5 and~150 Hz.}
\label{fig_input_benchmark} 
\end{figure}

Using FROLS technique for identifying a coupled NARX model (Section~\ref{sec_narx_models} and \cite{Billings2013,Billings1989,Billings1988}), a third-order polynomial coupled NARX model was created with the following delay parameters:
$$
n_y = 14 \qquadtext{and} n_u = 11 \qquadtext{and} n_k = 2.
$$
These delay parameters were chosen after trial and error tests with several other sets of delay parameters, to obtain small (coupled) model errors, for the prediction and simulation errors:
\begin{itemize}
	\item the relative prediction error of this coupled model is 1.4\%,
	\item the relative simulation error of this coupled model is 3\%
\end{itemize}
However, this coupled model contains, due to the many cross terms in the coupled function~$f$, 3250 parameters.
Decoupling the model will decrease the number of parameters considerably.

\subsubsection{Decoupling the NARX model with relaxed ALS}
Considering the relaxed ALS technique for creating a decoupled NARX model yielded acceptable results for the prediction output, after optimizing the cost function~\eqref{eq_chap5_optimization_problem} with prediction: the smallest relative decoupled model errors were in the order of~22\%, when comparing~15 different random initializations for the relaxed ALS method (Figure~\ref{fig_output_als_with_errors}).
\begin{figure}[p]
\centerline{\includegraphics{chapter5/figures/fig_output_als_with_errors.pdf}}
\caption{The prediction output of the decoupled model ({\color{blue}blue}) is able to model accurately the resonance in the low frequency region.
This decoupled model is created with the relaxed ALS technique, followed by the optimization of the cost function~\eqref{eq_chap5_optimization_problem} with the prediction error.
However, the error of the decoupled NARX model ({\color{green}green dots}) are significantly higher than the linear ARX model ({\color{red}red crosses}), which is a severe flaw in the relaxed ALS technique (Figure~\ref{fig_nonconverging_costfunction}).
However, the seperable LS technique seems a good alternative with superior results (Figure~\ref{fig_output_sls_with_errors}).
}
\label{fig_output_als_with_errors} 
\end{figure}
These initializations chose the transformation matrix~$\V$ and the coefficient vector~$\c$ uniform randomly with elements in the interval $[0, 1]$, as the starting point for the relaxed ALS iterations.
Unfortunately however, the decoupled models created with the relaxed ALS technique were all unstable in the simulation mode.
This unstable behavior was very probably due to the non-converging cost function~\eqref{eq_low_rank_costfunction} on the level of the Jacobian matrix decomposition (Figure~\ref{fig_nonconverging_costfunction}).
\begin{figure}[ht]
\begin{center} \includegraphics{chapter5/figures/fig_nonconverging_costfunction.pdf} \end{center}
\caption{Unfortunately, the cost function~\eqref{eq_low_rank_costfunction} on the level of the Jacobian matrix decomposition is highly non-convergent.
This is due to the update of the factor~\V\ during the relaxed ALS process (Section~\ref{6sec_identif_decouple_smooth}).
In this plot, the cost function is plotted after successive updates of the two factors~\V\ and~\H.}
\label{fig_nonconverging_costfunction} 
\end{figure}
Even when choosing the parameter values for the minimal cost function after a certain number of iterations, the simulation outputs were always unbounded during our experiments.
Section~\eqref{6sec_identif_decouple_smooth} discusses the implications of the non-convergent cost function on the Jacobian level in greater detail, in the more general decomposition problem of three-way tensors, where the same difficulties arise.

Finally, the errors of the decoupled NARX models were compared to the errors of linear ARX models (Figure~\ref{fig_output_als_with_errors}).
These ARX models are created when imposing linearity in the definition~\eqref{eq_narx_coupled}.
From these experiments and with the relaxed ALS method, it seems that decoupling the NARX models yields similar errors at when considering the linear ARX model.

\subsubsection{Decoupling the NARX model with separable LS}
Nevertheless, the results of the separable LS method are more promising~\cite{Westwick2017}.
With this method, stable models were created, both for the prediction output (1.7\% error) and the simulation output (5.4\% error).
In both cases, the cost function~\eqref{eq_chap5_optimization_problem} was minimized with the one-step ahead prediction errors.
During this optimization process, if the cost function increased, the next update was rejected and the step size was reduced (Figure~\ref{fig_JacobianCost}).
Sometimes, several rejecting steps could happen in a row, and hence the step size was greatly reduced.
The 5-branch decoupled model contains only 141 parameters, which is a huge reduction of 96\% compared to the original coupled model's number of parameters, while keeping the model errors acceptably low.
The latter method seems thus to be more robust for decomposing the Jacobian matrix, and is, to the knowledge of the authors, currently the best state-of-the-art method for identifying decoupled NARX models (Figures~\ref{fig_output_sls_with_errors} and~\ref{fig_output_sls_with_simulation_errors}).

\begin{figure}[ht]
\begin{center} \includegraphics[width=0.8\textwidth]{chapter5/figures/JacobianCost.pdf} \end{center}
\caption{With the seperable LS method~\cite{Westwick2017}, the cost function is forced to decrease.
In the unfortunate case that the next update is increasing, this next update is rejected and the step size is reduced (wholes in the plot).
Sometimes, several rejecting steps in a row were necessary to ensure a decreasing cost function.}
\label{fig_JacobianCost} 
\end{figure}

\begin{figure}[htp!]
\begin{center} \includegraphics{chapter5/figures/fig_output_sls_with_errors.pdf} \end{center}
\caption{With the seperable LS decomposition technique, far lower model errors are obtained than with the relaxed ALS method, when considering prediction errors.
With the former, the linear ARX model errors ({\color{red}red crosses}) are generally a bit higher than the decoupled NARX model errors ({\color{green}green dots}), which implies a small increase in the accuracy of the modeled prediction output with the decoupled NARX model.}
\label{fig_output_sls_with_errors} 
\end{figure}

\begin{figure}[htp!]
\begin{center} \includegraphics{chapter5/figures/fig_output_sls_with_simulation_errors.pdf} \end{center}
\caption{The model created with the seperable least-squares method also yields good results with simulation errors.
Unfortunately, the relaxed ALS method did not result in stable models with simulation errors.}
\label{fig_output_sls_with_simulation_errors} 
\end{figure}



\section{Conclusion and future work\label{sec_chap5_future_work}}
This chapter forms, to the knowledge of the authors, the first step towards decoupled nonlinear autoregressive exogenous models.
We have tested two different matrix decomposition methods and have observed mixed results.
Due to a non-converging cost function, the first method (alternating least squares) may yield unstable models considering simulation outputs.
However, the second proposed method (separable least squares) is able to create stable models  with simulation outputs, while minimizing the increase in model errors.
Therefore, our preference goes to the latter method.

At their core, both proposed technique consider the Jacobian matrix built on (possibly many) operating points.
Future work might consider other decoupling approaches, without investigating the first-order derivatives, or instead, by considering second-order (or more) derivatives.
Future work might also consider other basis functions for the considered models, different from the polynomials used in this chapter: perhaps sinusoids or saturating functions might be better suited for creating stable decoupled NARX models.

