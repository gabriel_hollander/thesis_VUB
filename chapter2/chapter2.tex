\chapter{Numerical examples for the weighted tensor decompositions}\label{chap_2}

\abstracttext{%
\noindent A weighted tensor decomposition technique was proposed in Chapter~\ref{chap_1}, by considering a low-rank weight matrix during the decomposition process.
Testing and applying this new method on several numerical examples is needed to control its performance.
We have tested the proposed technique on three examples, from the innermost tensor level to the outer system identification level, and this chapter gives an overview of each example. \newline
This chapter is organized as follows: Section~\ref{sec_introduction_chap_2} states the numerical problems, while Section~\ref{sec_example_jacobian} shows how the considered weight matrix interacts with the tensor decomposition.
Furthermore, Section~\ref{sec_nonlinearity_jacobian} analyzes the decoupling problem on the static nonlinear level, and Section~\ref{sec_Wiener_Hammerstein_jacobian} decouples a coupled parallel Wiener-Hammerstein model.
Finally, Section~\ref{sec_conclusion_weighted_cpd_sid} concludes this chapter and links it to Chapter~\ref{chap_3}.
}


\section{Problem statement\label{sec_introduction_chap_2}}
The proposed weighted tensor decomposition (Chapter~\ref{chap_1}) considers a linearly transformed covariance matrix, associated to the elements of the Jacobian tensor to be decomposed.
This weighted tensor decomposition should be tested to guarantee its performance.
The following two problems will be adressed in this chapter:
\begin{enumerate}
	\item Does the weighted tensor decomposition keep track of the covariances between tensor elements?
	When decomposing a tensor with a weight matrix, two correlated tensor elements should imply correlated errors in the weighted decomposition (Section~\ref{sec_example_jacobian}).
	\label{en_question1}
	\item Is it valuable to consider a weight during the tensor decomposition in a nonlinear polynomial decoupling context?
	When decoupling a coupled nonlinear function in a system identification context, considering the weighted tensor decomposition approach should yield better results than without the weight matrix (Sections~\ref{sec_example_jacobian} and~\ref{sec_Wiener_Hammerstein_jacobian}).
	\label{en_question2}
\end{enumerate}
The next sections are ordered following the mathematical layers, from the innermost tensor level, passing by the nonlinearity level and ending at the system identification level.

\section{Correlations of the errors in weighted CPD\label{sec_example_jacobian}}
This section demonstrates that the weighted tensor decomposition keeps track of the covariances between elements.
For this, we consider an example tensor decomposition with tensor dimensions $2\times2\times2$ and start with the following $8\times8$ positive definite weight matrix
\begin{equation}
\weight = \begin{bmatrix}                                            
   0.74 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
   0 & 1.67 & 0 & 0 & \color{blue}\underline{0.87} & 0 & 0 & 0 \\
   0 & 0 & 0.96 & 0 & 0 & 0 & 0 & \color{red}\underline{\underline{0}} \\
   0 & 0 & 0 & 0.63 & 0 & 0 & 0 & 0 \\
   0 & \color{blue}\underline{0.87} & 0 & 0 & 1 & 0 & 0 & 0 \\
   0 & 0 & 0 & 0 & 0 & 0.11 & 0 & 0 \\
   0 & 0 & 0 & 0 & 0 & 0 & 0.77 & 0 \\
   0 & 0 & \color{red}\underline{\underline{0}} & 0 & 0 & 0 & 0 & 0.31 \\
\end{bmatrix}.
\label{eq_wDefinition}\end{equation}
To simplify the example, the matrix~$\weight$ is chosen to be almost diagonal, and has two extra nonzero covariance elements. 
Any tensor~$\Tcal$\ to be decomposed with this weight matrix, has dimensions $2\times2\times2$, and we index its eight elements as follows: 
\begin{equation}
\Tcal_1 = \begin{bmatrix}
t_1 & t_3 \\
t_2 & t_4
\end{bmatrix} \quadtext{and} \Tcal_2 = \begin{bmatrix}
t_5 & t_7 \\
t_6 & t_8
\end{bmatrix},
\label{eq_tensorNumbering}\end{equation}
where $\Tcal_1$ and $\Tcal_2$ denote the first and second frontal slices of \Tcal\ respectively. 
Interpreting Equation~\eqref{eq_wDefinition} with these notations, we compared the following set of elements of~\Tcal\ for correlation:
\begin{itemize}
	\item elements $t_2$ and $t_5$ are correlated with covariance 0.87 ({\color{blue}\underline{blue}}),
	\item elements $t_3$ and~$t_8$ are uncorrelated ({\color{red}\underline{\underline{red}}}).
\end{itemize}

For this comparison, 500 uniform random $2\times2\times2$ tensors~\Tcal\ are decomposed using the proposed weighted tensor decomposition (Section~\ref{sec_computing_wCPD}) with weight matrix~$\L^T\L = \weight$. 
When recombining the sum of rank-one factors, we denote this approximate tensor as~$\hat{\Tcal}$. 
The tensor elements $\hat{t}_2$ and $\hat{t}_5$ of $\hat{\Tcal}$ can be compared to their correspondenting elements $t_2$ and $t_5$ of \Tcal, and the errors $t_2 - \hat{t}_2$ are correlated to the errors $t_5 - \hat{t}_5$ (top in Figure~\ref{fig_result1}).
Furthermore, the pair of elements $\hat{t}_3$ and $\hat{t}_8$ of $\hat{\Tcal}$ can be compared to their corresponding elements $t_3$ and $t_8$ of \Tcal, and their respective errors are uncorrelated (top in Figure~\ref{fig_result1}).


% In the left plot of~\fig{fig_result1}, we plot the differences $t_5 - \hat{t}_5$ on the vertical axis, and the differences $t_2 - \hat{t}_2$ on the horizontal axis. 
% When repeating this experiment~500 times with random tensors~$\Tcal$ and uniform random initialization points, we see that the errors are correlated, as expected.

% In the right plot of~\fig{fig_result1}, the differences between $\Tcal$ and $\hat{\Tcal}$ are shown, but between the uncorrelated elements $t_3$ and~$t_8$. We see that here, the scatter plot shows uncorrelated errors between the corresponding elements of the tensor and its decomposition.

\begin{figure}[p]
\begin{center}
	\includegraphics{chapter2/figures/results1_corr.pdf}
	\includegraphics{chapter2/figures/results1_uncorr.pdf}
\end{center}
\caption{The errors between decomposed and original tensor are either correlated or not, depending on the weight element in the corresponding element of the weight matrix~$\weight$, Equation~\eqref{eq_wDefinition}.
Top: the errors between elements 2 and 5 are correlated. Bottom: the errors between elements 3 and 8 are not.}
\label{fig_result1} 
\end{figure}

This behavior is expected from the weight matrix and answers question~\ref{en_question1} (Section~\ref{sec_introduction_chap_2}).
The difference in error dependencies can be understood as follows: the weight matrix~$\weight$ imposes extra conditions during the weighted ALS iterations.
As~$\weight$ has full rank, it is defined as the inverse of a covariance matrix.
Hence, a nonzero weight element in~$\weight$ is originated from a nonzero covariance and thus it implies correlated errors.
On the contrary, the zero weight element originates also from a zero covariance between the two tensor elements~$t_3$ and~$t_8$.
Hence, uncorrelated errors result (Figure~\ref{fig_result1}).

The next section considers the weighted tensor decomposition in a broader context: the noisy multivariate polynomial decoupling.

\section[Approximate multivariate polynomial decoupling]{Approximate multivariate polynomial\\ decoupling\label{sec_nonlinearity_jacobian}}
In this section, we apply the weighted tensor decomposition in the broader context of approximate multivariate polynomial decoupling. 
The starting point is the following coupled function~$\f_0$ (the numbers are rounded to two decimals):
\begin{equation}
   \begin{aligned}
      &\f_0(u_1, u_2) \\
      &= 
         \begin{cases}
       0.1u_1^3 - 0.38u_1^2u_2 + 0.16u_1^2 - 0.06u_1u_2^2 - 0.28u_1u_2 + 0.32u_1 \\ 
       \qquad - 0.01u_2^3 - 0.02u_2^2 - 0.29u_2, \\
        0.56u_1^3 + 0.14u_1^2u_2 + 0.62u_1^2 + 0.09u_1u_2^2 + 0.08u_1u_2 + 1.3u_1 \\
         \qquad + 0.01u_2^3 + 0.03u_2^2 + 0.09u_2.
      \end{cases}
   \end{aligned}
\label{eq_definition_f0}
\end{equation}
which is exactly decomposable as follows (numbers rounded to two decimal places)
$$
\f_0(u_1, u_2) = 
\begin{bmatrix}    
   -0.43 & -0.65 \\
   0.44 & -0.52 \\ 
\end{bmatrix}      
\begin{bmatrix}
 -0.09x_1^3 + 0.18x_1^2 -0.7x_1 \\ 
   0.14x_2^3 -0.27x_2^2 + 0.86
\end{bmatrix}
$$
with
$$
\begin{bmatrix}
x_1 \\
x_2
\end{bmatrix}
=
\begin{bmatrix}    
   -1.86 & -0.58 \\
   -1.57 & 0.20 \\ 
\end{bmatrix}      
\begin{bmatrix}
u_1 \\
u_2
\end{bmatrix}.
$$

Next, we repeatedly add correlated noise directly to the coefficients of~$\f_0$, thus creating different noisy coupled polynomials $\f$.
The noise is correlated by a random positive semi-definite matrix, kept fixed during the experiments.
Afterwards, we compute approximate decouplings of the noisy polynomials $\f$, comparing the unweighted decoupling with the different proposed weighting techniques (Chapter~\ref{chap_1}).
Hence, the following four different decouplings~$\f_\textrm{dec}$ of~$\f$ are compared:
\begin{enumerate}
\item the unweighted decoupling \cite{Dreesen2015},
\item the weighted decoupling with the element-wise approximation $\SigmaJ^{\textrm{e}}$ of the covariance matrix, 
\item the weighted decoupling with the slice-wise approximation $\SigmaJ^{\textrm{s}}$ of the covariance matrix, 
\item the weighted decoupling with the dense weight, defined with the covariance matrix $\SigmaJ$.
\end{enumerate}
With a stopping tolerance of $10^{-6}$ (Section~\ref{sec_theoretical_results_wALS}), the (weighted) ALS iterations stopped most of the time before reaching the maximum number of 6000 iterations.

As a measure for the comparison, we used the relative Frobenius norm on the outputs of a validation set, with respect to the Frobenius norm of the original output signal. 
This relative Frobenius yields relative output errors of the constructred models, which makes the understanding of the results easier (Figures~\ref{fig_lambdaSearch} and~\ref{fig_application2}).
Also, we tested 68 different values of the hyperparameter $\lambda$ between $10^{-5}$ and 2, and found a local minimum for $\lambda = 0.3565$ (Figure~\ref{fig_lambdaSearch}).
This value for $\lambda$ is based on a single experiment for each~$\lambda$ and will be used in the rest of this section.
% For this figure, I used the data in the file 
% Z:\2016-02-05 NLA article\Version of 2016-04-01\data for the second application\lambda1e-5to2.mat
\begin{figure}[tp]%
\includegraphics{chapter2/figures/lambdaSweap.pdf}
\caption{Sixty eight values of the hyperparameter $\lambda$ were tested, by looking at the relative errors between a validation set and the original output, using the Frobenius norm.
In the range between $10^{-6}$ and $2$, a local optimum is found for $\lambda = 0.3565$.}
\label{fig_lambdaSearch}
\end{figure}

After adding correlated noise 200~times, and computing the different (approximate) decouplings, the following three observations can be made (Figure~\ref{fig_application2}).
\begin{enumerate}
   \item All proposed decoupling techniques (weighted or unweighted), are quite sensitive to the exact noise realization and could be prone to local minima: relative output errors vary between $0.07\%$ and $0.8\%$, over the different weighting methods.
   Therefore, in practice, it is generally good practice to choose the best decoupling solution over a set of repeated experiments.
   \item Only small differences in errors can be observed between the unweighted decoupling and the decoupling with element-wise approximation $\SigmaJ^{\textrm{e}}$.
   Apparantly, the element-wise approximation makes a too rough approximation of the tensor covariance matrix, and is hence too sparse to contain (much) useful information.
   \item Although a few spikes occured (due to a few bad local minima), the smallest relative output errors are obtained with the slice-wise approximation $\SigmaJ^{\textrm{s}}$ or the low-rank dense covariance matrix $\SigmaJ$.
   Between these two weighted decompositions, the best overal results are obtained with the full covariance matrix~$\SigmaJ$ decoupling.
\end{enumerate}
To sum up briefly, these observations can be seen from the following averages of the relative errors on the outputs $\norm{\f_\textrm{dec}(\u) - \f_0(\u)}$ over the~200 measurements:
\begin{center}
\begin{tabular}{r|l}
error no weight & 0.1974 \\
error element-wise weight & 0.1949 \\
error slice-wise weight & 0.1946 \\
error full-weight & 0.1727
\end{tabular}   
\end{center}

As a conclusion from these experiments, weighting helps for the decoupling problem, when considering the Frobenius norm of the output errors. 
The best weighting method seems to be the low-rank dense covariance matrix.
Also, although the noise level remains constant over the experiments, the output error of the approximated decouplings is dependent on the exact noise realization.

\begin{figure}[tp]
\includegraphics{chapter2/figures/application2.pdf}
\caption{Using 200 different correlated noise realizations on the coefficients of a coupled polynomial~$\f_0$, Equation~\eqref{eq_definition_f0}, the noisy coupled polynomial is decoupled using different weighting methods (Section~\ref{sec_constructing_covariance}).  
Ordering the measurements on the $x$-axis according to the Frobenius norm of the error of the unweighted CPD decoupling method reveals the better results of the full-weighted decoupling method.}
\label{fig_application2}
\end{figure}


\section[Decoupling a parallel Wiener-Hammerstein system]{Decoupling an identified parallel Wiener-Hammerstein system\label{sec_Wiener_Hammerstein_jacobian}}
To gain an understanding of how much impact the weight has during the tensor decomposition, the decoupling problem is examined from an even further distance than the two preceding sections.
Instead of considering the coupled nonlinearity on its own (Section~\ref{sec_nonlinearity_jacobian}), we surround it in this section by a set of linear time-invariant filters.
Hence, the output of the full model is the sum of the partial outputs at the right filters (Figure~\ref{fig:sysID}).
\begin{figure}[ht]
   \includegraphics{chapter2/figures/systemID.pdf}
   \caption{The model type studied in this section consists of the nonlinear polynomial function \f\ surrounded by linear time-invariant filters $\mathrm{L}_1,\mathrm{L}_2, \mathrm{R}_1$ and $\mathrm{R}_2$. 
   For the purposes of this chapter, we assume the left filters $L_1$ and $L_2$ (resp. the right filters $R_1$ and $R_2$) to behave similarly and have similar transfer functions.} 
   \label{fig:sysID}
\end{figure}
Such a model appears during the state-of-the-art nonlinear system identificaiton technique of parallel Wiener-Hammerstein models (\cite{Schoukens2015} and Section~\ref{sec_block_oriented_models}).
For the example discussed further, we consider low-pass filters for the surrounding linear filters (Figure~\ref{fig_lowPass}).
\begin{figure}[ht]
   \begin{minipage}[c]{0.5\textwidth}
   \centerline{%
         \includegraphics{chapter2/figures/bodePlots1.pdf}}
   \end{minipage}
   \begin{minipage}[c]{0.5\textwidth}
   \centering
      \includegraphics{chapter2/figures/bodePlots3.pdf}
   \end{minipage}
   \begin{minipage}[c]{0.5\textwidth}
   \centering
      \includegraphics{chapter2/figures/bodePlots2.pdf}
   \end{minipage}
   \begin{minipage}[c]{0.5\textwidth}
   \centering
      \includegraphics{chapter2/figures/bodePlots4.pdf}
   \end{minipage}
   \caption{The input (left) and output filters (right) of the linear time-invariant system used in this section are low-passed filters.} 
   \label{fig_lowPass}
\end{figure}

In this section, we wish to decouple the multivariate polynomial given by (the decimals are rounded up until two decimal places)
% This is the example used in the folder Z:\2016-03-25s fW decoupling special cases_lamda\Case Study 2015_10_28_12_16_11.867\experiments.
\begin{equation}
   \begin{aligned}
      &\f(\u) \\
      &= \begin{cases}
      - 0.27u_1^3 - 0.15u_1^2u_2 + 0.18u_1^2 + 0.01u_1u_2^2 + 0.21u_1u_2 + 0.7u_1 \\ 
         \qquad {}+ 0.03u_2^3 - 0.049u_2^2 - 0.49u_2
      \\
      0.37u_1^3 - 0.18u_1^2u_2 - 0.2u_1^2 + 0.23u_1u_2^2 - 0.23u_1u_2 - 0.49u_1 \\
         \qquad {}- 0.091u_2^3 + 0.1u_2^2 + 0.67u_2.
       \end{cases}
   \end{aligned}
   \label{eq_f_to_be_decoupled}
\end{equation}
Also, we assume that the coefficients of \f\ are correlated with the covariance matrix~$\mathbf{\Sigma}_\f$ in Equation~\eqref{mat_covar}.
\afterpage{
   % \clearpage
   \begin{landscape}
      \begin{figure}
      \leavevmode\scalebox{0.65}{   
      \begin{equation}
      \mathbf{\Sigma}_\f = 10^{-2}
      \left[\begin{array}{@{}rrrrrrrrrrrrrrrrrr@{}}                      
         \textbf{1.54} & 0.00 & 0.50 & 0.15 & -0.06 & -1.71 & -0.05 & -0.25 & 0.05 & -1.51 & 0.04 & -0.48 & -0.08 & 0.04 & 1.60 & -0.04 & 0.23 & -0.05 \\ 
         0.00 & \textbf{0.16} & 0.01 & -0.03 & -0.00 & 0.09 & -0.34 & 0.16 & -0.14 & -0.01 & -0.14 & -0.00 & 0.02 & 0.00 & -0.07 & 0.29 & -0.14 & 0.12 \\ 
         0.50 & 0.01 & \textbf{2.28} & 0.40 & -0.08 & -0.79 & -0.26 & 0.32 & -0.04 & -0.49 & 0.01 & -2.22 & -0.23 & 0.04 & 0.67 & 0.22 & -0.29 & 0.04 \\  
         0.15 & -0.03 & 0.40 & \textbf{0.65} & -0.08 & -0.33 & 0.17 & 0.04 & 0.01 & -0.15 & 0.03 & -0.41 & -0.55 & 0.07 & 0.29 & -0.15 & -0.02 & -0.01 \\ 
         -0.06 & -0.00 & -0.08 & -0.08 & \textbf{0.11} & 0.20 & -0.00 & -0.02 & 0.03 & 0.06 & 0.00 & 0.06 & 0.06 & -0.10 & -0.19 & 0.01 & 0.01 & -0.02 \\ 
         -1.71 & 0.09 & -0.79 & -0.33 & 0.20 & \textbf{6.44} & 0.74 & 0.02 & -0.06 & 1.62 & -0.12 & 0.66 & 0.26 & -0.16 & -6.03 & -0.33 & -0.09 & 0.06 \\ 
         -0.05 & -0.34 & -0.26 & 0.17 & -0.00 & 0.74 & \textbf{3.90} & -0.70 & 0.04 & 0.11 & 0.28 & 0.18 & -0.16 & 0.02 & -0.87 & -3.15 & 0.54 & -0.03 \\ 
         -0.25 & 0.16 & 0.32 & 0.04 & -0.02 & 0.02 & -0.70 & \textbf{1.61} & -0.38 & 0.21 & -0.13 & -0.30 & -0.03 & 0.01 & -0.02 & 0.55 & -1.35 & 0.33 \\ 
         0.05 & -0.14 & -0.04 & 0.01 & 0.03 & -0.06 & 0.04 & -0.38 & \textbf{0.29} & -0.05 & 0.12 & 0.04 & -0.01 & -0.02 & 0.07 & -0.05 & 0.33 & -0.24 \\ 
         -1.51 & -0.01 & -0.49 & -0.15 & 0.06 & 1.62 & 0.11 & 0.21 & -0.05 & \textbf{1.49} & -0.03 & 0.47 & 0.08 & -0.04 & -1.54 & 0.00 & -0.21 & 0.05 \\ 
         0.04 & -0.14 & 0.01 & 0.03 & 0.00 & -0.12 & 0.28 & -0.13 & 0.12 & -0.03 & \textbf{0.13} & -0.01 & -0.02 & -0.00 & 0.11 & -0.25 & 0.12 & -0.11 \\ 
         -0.48 & -0.00 & -2.22 & -0.41 & 0.06 & 0.66 & 0.18 & -0.30 & 0.04 & 0.47 & -0.01 & \textbf{2.17} & 0.24 & -0.03 & -0.54 & -0.16 & 0.28 & -0.04 \\
         -0.08 & 0.02 & -0.23 & -0.55 & 0.06 & 0.26 & -0.16 & -0.03 & -0.01 & 0.08 & -0.02 & 0.24 & \textbf{0.48} & -0.05 & -0.23 & 0.15 & 0.01 & 0.01 \\ 
         0.04 & 0.00 & 0.04 & 0.07 & -0.10 & -0.16 & 0.02 & 0.01 & -0.02 & -0.04 & -0.00 & -0.03 & -0.05 & \textbf{0.09} & 0.15 & -0.02 & -0.01 & 0.02 \\ 
         1.60 & -0.07 & 0.67 & 0.29 & -0.19 & -6.03 & -0.87 & -0.02 & 0.07 & -1.54 & 0.11 & -0.54 & -0.23 & 0.15 & \textbf{5.71} & 0.42 & 0.10 & -0.07 \\ 
         -0.04 & 0.29 & 0.22 & -0.15 & 0.01 & -0.33 & -3.15 & 0.55 & -0.05 & 0.00 & -0.25 & -0.16 & 0.15 & -0.02 & 0.42 & \textbf{2.69} & -0.46 & 0.04 \\ 
         0.23 & -0.14 & -0.29 & -0.02 & 0.01 & -0.09 & 0.54 & -1.35 & 0.33 & -0.21 & 0.12 & 0.28 & 0.01 & -0.01 & 0.10 & -0.46 & \textbf{1.21} & -0.30 \\ 
         -0.05 & 0.12 & 0.04 & -0.01 & -0.02 & 0.06 & -0.03 & 0.33 & -0.24 & 0.05 & -0.11 & -0.04 & 0.01 & 0.02 & -0.07 & 0.04 & -0.30 & \textbf{0.22} \\ 
      \end{array}\right] 
      \label{mat_covar}\end{equation}
      }
      \caption*{Because the function~\f\ to be decoupled consists of~18 coefficients, Equation~\ref{eq_f_to_be_decoupled}, the covariance matrix on its coefficients has dimensions $18\times18$.
      We assume this matrix to be given for the purposes of this section, although it can be approximated with system identification techniques, for example when approximating~\f\ from an earlier stage in the identification process (Section~\ref{sec_decouplingStage}).
      }
      \end{figure}
   \end{landscape}
}
In this matrix, the element on position~$(i,j)$ contains the covariance between the $i$-th and~$j$-th coefficient of \f, if all the function's coefficients are vectorized.

Noteworthy, the assumption of having Gaussian distributed coefficients of~\f\ is asymptotically satisfied for large numbers of samples.
In such an asymptotic case, the proposed weighted decoupled methods (Section~\ref{sec_computing_wCPD}) lies in the framework of maximum likelihood.
Within the current section of system identification, the same assumption of Gaussian distribution is made on the coefficients of~\f.

The decoupling of \f\ is computed four times, as in Section~\ref{sec_nonlinearity_jacobian}, to compare the effect of the different kinds of weights, if any, on the CPD:
  \begin{enumerate}
  \item decoupling without any weight~\cite{Dreesen2015};
  \item decoupling with the element-wise approximation~$\SigmaJ^{\textrm{e}}$;
  \item decoupling with the slice-wise approximation~$\SigmaJ^{\textrm{s}}$; and
  \item decoupling with the dense covariance matrix~\SigmaJ.
  \end{enumerate}
After these various decouplings, a random-phase multisine~(\cite{Pintelon2012} and Figure~\ref{fig_valSignal}) is sent as a validation signal through the original coupled system (\fig{fig:sysID}) and through the four different decoupled models.
\begin{figure}[ht]
   \begin{minipage}[c]{0.5\textwidth}
   \centering
      \includegraphics{chapter2/figures/valSignal_magn.pdf}
   \end{minipage}
   \begin{minipage}[c]{0.5\textwidth}
   \centering
      \includegraphics{chapter2/figures/valSignal_phase.pdf}
   \end{minipage}
   \caption{The validation signal used to compare the different decoupled models looks in the time domain as a random signal, but it is actually structurally defined.
   Only its phases are randomly chosen, as the number of excited frequency lines are chosen depending on the application.}
   \label{fig_valSignal}
\end{figure}

To find a good value of the hyperparameter~$\lambda$, the weighted decoupling-validation steps are repeated for 35 different values of the $\lambda$, chosen between $10^{-5}$ and $10$ (\fig{fig_RMSvsLambda}). 
The current example system seems to have a local minimum at $\lambda = 3.667$, which is the value we set for the rest of this discussion. 
\begin{figure}[p]
      \begin{center}
         \includegraphics{chapter2/figures/errorsVSlambdaTotal.pdf}
         \includegraphics{chapter2/figures/errorsVSlambda.pdf}
      \end{center}
   \caption{To find an appropriate value of the hyperparamater $\lambda$ for the current model, its effect on the output decoupled error is checked, with a random-phased multisine as the validation signal.
   Because many different values of $\lambda$ should be tested, we started with the large interval $10^{-5}$ and 10 (top) and then zoomed in on a smaller interval 2.5 to 7 (bottom).
   The value for $\lambda$ was finally set to the local minimum $3.667$, even though any value for $\lambda>2$ might be good in this example, as the output error does not significantly increase for larger values of~$\lambda$.
   Noteworthy, the exact curve for the output error with respect to~$\lambda$ differs from model to model (for example, Figure~\ref{fig_lambdaSearch}), and should be checked in every application.
   } 
   \label{fig_RMSvsLambda}
\end{figure}

With this value of $\lambda$, the four different decoupling methods cited above were tested and analyzed as follows.
The dense covariance matrix~\SigmaJ\ or its slice-wise approximation offers lower output errors, with respect to the unweighted decoupling, or with the element-wise approximation of the covariance matrix~$\SigmaJ$ (\fig{fig:results}).

Next to these numerical experiments, extensive simulations were done with hundreds of different systems and as many coupled multivariate polynomials. 
The parameters of these systems were uniform randomly chosen in a fixed interval, similar to the above-mentioned nonlinearity~\eqref{mat_covar}.
From these simulations, we could derive the following three (heuristic) observations about the different types of (weighted) decoupled representations:
\begin{enumerate}
   \item Decoupling noisy multivariate polynomials with the dense covariance matrix~$\SigmaJ$ and its slice-wise approximation~$\SigmaJ^{\textrm{s}}$ generally yield at least as good output errors as the decompositions with no weight described.
   \item Overall, the element-wise approximation~$\SigmaJ^{\textrm{e}}$ does not incorporate enough information to improve the unweighted decoupling and yields comparable output errors as the latter.
   \item Finally, for difficult decoupling problems where the unweighted decoupling does not yield acceptable results, we observed improvements with the slice-wise or dense weighted decoupling.
\end{enumerate}
These results are encouraging and stay in the line of the popular technique of weighted least squares.

\begin{figure}[ht!]
   \centerline{\includegraphics{chapter2/figures/results.pdf}}
   \caption{The errors between the original output signal and the output produced by the different decoupling methods. Plotted in the frequency domain; the vertical axis is in dB of the magnitude of the signals.} 
   \label{fig:results}
\end{figure}
% \clearpage

\section[Conclusions of the numerical experiments]{Conclusions of numerical experiments of the weighted CPD\label{sec_conclusion_weighted_cpd_sid}}
In this chapter, we applied the weighted tensor decomposition process of Chapter~\ref{chap_1} to three numerical examples, starting at the tensor level, passing by the nonlinearity level, and ending at the full nonlinear system identification level.
Considering a weight during the decoupling and decomposition process does what it is supposed to do: better fit the noise to achieve smaller decoupled model errors.
However, care should be taken, as considering the weight only helps when the noise errors are larger than model errors.
Identification with large model errors will be the subject of Part~\ref{part_2} of this thesis.

To our knowledge, except a few exceptions~\cite{Bro2002,Raimondi2017}, it is quite unfortunate that so little literature is available on the weighted CPD.
Thanks to the encouraging results of this chapter, future work might consist of experimenting with the weighted tensor decomposition in different, perhaps even unrelating to system identification, scientific and engineering fields.

Another possible future work problem arrises in the outermost level in this chapter, the identification of a parallel Wiener-Hammerstein model.
During the proposed decoupling of this model, all the surrounding linear filters were assumed to be known and they were left unchanged during the process.
However, this assumption is rather unrealistic and will be softened in Chapter~\ref{chap_3}: there, a full parallel Wiener-Hammerstein model will be identified, starting from simulated data and ending with decoupled models.
Identifying the linear filters as well as possible will also be the subject of the next chapter.
