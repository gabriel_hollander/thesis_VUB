function wrapped_text = split_text(text_section, width_wrapping)
%%
    wrapped_line = 1;
%     width_wrapping = 20;
    splitted_text = strsplit(text_section);
    wrapped_text = splitted_text(wrapped_line);
    current_width = length(wrapped_text{end});
    for iter_splitted = 2 : length(splitted_text)
        if (current_width + length(splitted_text{iter_splitted})) < width_wrapping
            wrapped_text{wrapped_line} = strjoin({wrapped_text{wrapped_line}, splitted_text{iter_splitted}});
        else
            wrapped_line = wrapped_line + 1;
            wrapped_text{wrapped_line} = splitted_text{iter_splitted};
        end
        current_width = length(wrapped_text{wrapped_line});
    end
%     wrapped_text = strjoin({'\tiny', wrapped_text, splitted_text{end}});
%     wrapped_text = regexprep(wrapped_text,'(\\[^n{}])','\\$0');    
end
