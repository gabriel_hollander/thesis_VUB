% load('Z:\2017-03-08 NARX models\With David Westwick\data_for_plot_thesis_chapter5\data.mat');
load('Z:\2017-03-08 NARX models\With David Westwick\data_for_plot_thesis_chapter5\data_iterations\rand_init_2.mat')
arx_model = load('Z:\2017-03-08 NARX models\With David Westwick\data_for_plot_thesis_chapter5\arx_model.mat');
arx_ypredict = arx_model.arx_ypredict;

%%
[Voptim, Goptim, Woptim] = theta2VGW(thetaOptimizedCoeffs, n, m, rtilde, degreeMax);

yOptimModeled = OutputNL(Voptim, Goptim, Woptim, uOperatingPoints);

% measureOutput = norm(yOptimModeled - measuredOutput, 'fro') / norm(measuredOutput, 'fro');

%% create figure for my ALS algorithm
yOptimModeled_fft = db(fft(yOptimModeled));
measuredOutput_error_fft = db(fft(yOptimModeled - measuredOutput));
% yOutputReal_error_fft = db(fft(yOptimModeled - yOutputReal));
arx_output_error_fft = db(fft(arx_ypredict - measuredOutput));


clf;
hold on;
plot(yOptimModeled_fft(1:2:N/2), 'b')
plot(arx_output_error_fft(1:2:N/2), 'rx')
plot(measuredOutput_error_fft(1:2:N/2), '.g', 'markersize', 2)
set(gca, 'xtick', [0 N/12], 'xticklabel', {0, sprintf('%.3f', 1/6)}, 'xlim', [0 N/12], 'ytick', [-30 0 70], 'ylim', [-30 70])
xlabel('frequency (normalized)')
ylabel('prediction output (dB of magnitude)')
legend('decoupled output', 'error with ARX model', 'error with decoupled NARX model', 'location', 'northoutside')

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\fig_output_als_with_errors.tex', 'width', '0.8\textwidth', 'height', '6cm');


%%
%% create figure for David's SLS algorithm
uiopen('Z:\2017-03-08 NARX models\With David Westwick\FreqDomainErrors3_fromDavid.fig',1);

%% adapt the plot to show the ARX model errors
line_plots = get(gca, 'children');
% arx_output_error_fft_sls = db(fft( db2mag(get(line_plots(3), 'ydata')) - arx_ypredict(2:end)));
arx_output_error_fft_sls = db(fft( measuredOutput - arx_ypredict));
set(line_plots(1), 'ydata', arx_output_error_fft_sls)

