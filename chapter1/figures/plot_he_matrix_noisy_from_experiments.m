temp = load('C:\Users\gabriel.hollander\Google Drive\2017-02-28 NLAA Paper\Figures\experForAppl3.mat');

He = temp.exper.cpde_wTx.He;
Ve = temp.exper.cpde_wTx.Ve;
ustar = temp.exper.approximatedPoly.uOperatingPoints;

color = 'brgmk';
clf;
hold on;
for iter = 1 : size(He,2)
plot(TakeRow(Ve.'*ustar,iter), He(:,iter), [color(iter) '.'], 'markersize', 3)
% plot(TakeRow(temp.exper.cpd_bdWeights_wTx.Ve.'*ustar,iter), temp.exper.cpd_bdWeights_wTx.He(:,iter), [color(iter) '.']); %, 'markersize', 3)
end
text('string', '$h_1$', 'pos', [5 -80], 'interpreter', 'latex', 'color', 'r');
text('string', '$h_2$', 'pos', [-5 -20], 'interpreter', 'latex', 'color', 'b');
hold off;
set(gca, 'xtick', [-8 0 8], 'ytick', [-110 -10])
xlabel('$\mathbf{v}_i^T \mathbf{u}$', 'interpreter', 'latex');
ylabel('$h_i$', 'interpreter', 'latex');

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter1\figures\He_matrix_with_noise.tex', 'width', '0.8\textwidth', 'height', '5cm');
