% In this script, we process the results of the total_sim_4 experiment.
clear; close all;
direc = 'Z:\2016-04-22s Full Identification and Decoupling\Noise_level 0.01';
files = dir(fullfile(direc, 'exp*'));

for iter = 1 : length(files)
    LogWindow(iter, length(files), 'iter');
    temp = load(fullfile(direc, files(iter).name));
    coupledRMS(iter) = temp.expers.generalVariables.RMSerrorCoupledModel;
    decoupledRMS(:,:,iter) = [temp.expers.full_optimized.noWeights.validSscale4.initial.relRMSerror_finit  temp.expers.full_optimized.noWeights.validSscale4.optimized.relRMSerror_foptim;
                              temp.expers.full_optimized.dWeights.validSscale4.initial.relRMSerror_finit   temp.expers.full_optimized.dWeights.validSscale4.optimized.relRMSerror_foptim; 
                              temp.expers.full_optimized.bdWeights.validSscale4.initial.relRMSerror_finit  temp.expers.full_optimized.bdWeights.validSscale4.optimized.relRMSerror_foptim;
                              temp.expers.full_optimized.bdfWeights.validSscale4.initial.relRMSerror_finit temp.expers.full_optimized.bdfWeights.validSscale4.optimized.relRMSerror_foptim;
                              temp.expers.full_optimized.fWeights.validSscale4.initial.relRMSerror_finit   temp.expers.full_optimized.fWeights.validSscale4.optimized.relRMSerror_foptim];
end
close;



%%
% Plot the results in two plots, one before and one after the full optimization

subplot(2,1,1)
plot(sortrows([squeeze(decoupledRMS(:,1,:)).' coupledRMS.'], 1))
legend('noW init', 'dW init', 'bdW init', 'bdfW init', 'fW init', 'coupled', 'Orientation', 'hor')
set(gca, 'ylim', [0 0.1], 'xlim', [1 length(files)])
title('RMS errors before the optimization');
xlabel('Number of experiment'); ylabel('RMS of error');

subplot(2,1,2)
plot(sortrows([squeeze(decoupledRMS(:,2,:)).' coupledRMS.'], 1))
legend('noW optim', 'dW optim', 'bdW optim', 'bdfW optim', 'fW optim', 'coupled', 'Orientation', 'horizontal')
set(gca, 'ylim', [0 0.1], 'xlim', [1 length(files)])
title('RMS errors after the optimization');
xlabel('Number of experiment'); ylabel('RMS of error');


%% Plot for chapter 4 for thesis
data_for_plot = sortrows([squeeze(decoupledRMS(:,2,:)).' coupledRMS.'], 1);
semilogy(data_for_plot(:,[1 3 4 6]))
legend('decoupled no weight', 'decoupled block-diag.', 'decoupled full', 'coupled', 'location', 'northoutside')
set(gca, 'ylim', [0.003 0.3], 'xlim', [1 length(files)], 'xtick', [1 length(files)])
% title('log of RMS errors after the optimization');
xlabel('Number of experiment'); ylabel('RMS of error');


% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter3\figures\Repeated case systems\repeatedSimulations_2.tex', 'width', '0.8\textwidth', 'height', '8cm');





