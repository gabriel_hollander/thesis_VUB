\chapter{Identification and decoupling of nonlinear state-space models}\label{chap_real_measurements}

\abstracttext{%
\noindent Black-box models offer the great advantage of not needing any prior knowledge before the identification process.
One type of black-box model, the state-space model, considers a set of internal states in its model structure~\cite{Systems1980}.
Accordingly, the modeled outputs are a function of these internal states and the inputs.\newline
Identifying a polynomial nonlinear state-space model yields, as in the preceding chapters, a possibly large and complex coupled static nonlinear function.
Decoupling this nonlinearity while keeping the descriptive power of a state-space model might result in simpler and more intuitive black-box models.
Therefore, we have applied the tensor decomposition approach of the past chapters to several examples with real-life measurements.
Furthermore, we have considered a polynomial constraint during the tensor decomposition process at the core part of the algorithm.
This chapter gives an overview of the proposed decoupled polynomial nonlinear state-space identification approach, and applies it to three case studies.
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Problem statement
\section{Problem statement\label{sec6_problem_statement}}
In this chapter, we consider the following problem: given simulated or real-life measured inputs~\u\ and outputs~\y\ of an unknown single-input single-output system, we wish to represent this nonlinear system using a decoupled polynomial state-space model with a given number~$n_\x$ of states. 
More detailed, we wish to find
\begin{itemize}
 	\item a state matrix $\A \in \ER^{n_\x \times n_\x}$,
 	\item an input column vector $\b \in \ER^{n_\x \times 1}$, 
 	\item an output row vector $\c^T \in \ER^{1 \times n_\x}$ and
 	\item a feedforward scalar $d \in \ER$,
\end{itemize}
a decoupled multiple-input multiple-output nonlinear function
$$
\begin{aligned}
\f_\x\colon \ER^{n_\x+1} &\to \ER^{n_\x}\colon \\
 \bigl(\x(t), u(t)\bigr) &\mapsto \f_\x\bigl(\x(t), u(t)\bigr) 
\end{aligned},
$$
and a decoupled multiple-input single-output nonlinear function
$$
\begin{aligned}
f_\y\colon \ER^{n_\x+1} &\to \ER\colon \\
\bigl(\x(t), u(t)\bigr) &\mapsto f_\y\bigl(\x(t), u(t)\bigr),	
\end{aligned}
$$
such that the state-space modeled output~$\hat\y$ approximates the output~\y\ in the least-squared sense.
As a shorthand notation, we will note the inputs of the nonlinear functions $\f_\x$ and $f_\y$ as $\z(t)$. 

The output $\hat\y$ of the state-space model is defined as (Section~\ref{sec_state_space_models})
\begin{equation}
	\left\{
	\begin{aligned}
		\x(t+1) &= \A \x(t) + \B u(t) + \f_\x\bigl(\z(t)\bigr) \\
		\hat y(t) 	&= \c^T \x(t) + d u(t) + f_y\bigl(\z(t)\bigr).
	\end{aligned}
	\right.
	\label{6eq_state_space_model1}
\end{equation}
In this chapter, we will focus on polynomials for the nonlinear functions~$\f_\x$ and $f_\y$. 
Furthermore, without loss of generality, these nonlinearities can be written as a sum of monomials of degree two and higher.
Finally, we solely consider polynomials up to degree three (except in Section~\ref{sec6_WH_model}).

A decoupled representation of a nonlinear state-space model should contain decoupled functions $\f_\x$ and $f_\y$, that is, without any cross-terms.
More specifically, the state-space representation~(\ref{6eq_state_space_model1}) can be rewritten as
\begin{equation}
	\begin{bmatrix}
		\x(t+1) \\ \hat y(t)
	\end{bmatrix}
	=
	\begin{bmatrix}
		\A \\ \c^T
	\end{bmatrix}
	\x(t)
	+
	\begin{bmatrix}
		\B \\ d
	\end{bmatrix}
	u(t)
	+
	\underbrace{\begin{bmatrix}
		\f_\x\bigl(\z(t)\bigr) \\ f_y\bigl(\z(t)\bigr)
	\end{bmatrix}
	}_{\f\bigl(\z(t)\bigr)},
	\label{6eq_state_space_together}
\end{equation}
with the decoupled function $\f\bigl(\z(t)\bigr) = \W\g\bigl(\V^T \z(t) \bigr)$, for certain matrices~$\W\in\ER^{(n_\x+1) \times r}$ and $\V\in\ER^{(n_x+1) \times r}$ and a given number~$r$ of univariate functions $\g = (g_1, \ldots, g_r)$.

In short, the problem of this chapter is stated as follows: 
given the inputs \u\ and outputs \y\ of a nonlinear system and a number~$r$ of branches, find a decoupled state-space model with $r$ branches such that the following cost function is minimized:
\begin{equation}
\min_{\substack{\A, \B, \C, d, \\
\W, \g, \V}} 
\frac{\norm{\y - \hat \y}^2}{\norm{\y}^2}.
\label{6eq_cost_function}	
\end{equation}
Although dividing by $\norm{\y}^2$ does not change the convergence properties of the optimization problem, it simplifies the interpretation with a normalized error (Figures~\ref{fig_output_errors_wrt_Nbranches}, ~\ref{fig_WH_output_errors_wrt_Nbranches} and~\ref{fig_pWH_output_errors_wrt_Nbranches}).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Overview of the identification method
\section{Overview of the identification method\label{6sec_overview_id}}
We propose a method for identifying a decoupled state-space model in the following six stages.
\begin{enumerate}
	\item A coupled polynomial state-space model is obtained using the earlier developed algorithm for identifying nonlinear state-space models~\cite{Paduart2008,Paduart2010,Paduart2006} (Section~\ref{6sec_identif_coupled}). 
	The end result of this stage contains a coupled nonlinearity~\f, Equation~(\ref{6eq_state_space_together}).
	\label{item_PNLSS}
	\item As in Chapter~\ref{chap_1}, the tensor of Jacobians is created in $N$ random operating points $\z^{(1)}$, \ldots, $\z^{(N)}$. 
	The number~$N$ of operating points should be chosen large enough for the problem at hand (Algorithm~\ref{algor_exact_decoupling} on page~\pageref{algor_exact_decoupling}).
	\label{item_creating_J}
	\item This Jacobian tensor is decomposed with the Canonical Poly\-adic Decomposition (Section~\ref{6sec_identif_decouple_not_smooth}) with or without adding a polynomial constraint during the decomposition process (Section~\ref{6sec_identif_decouple_smooth}).
	\label{item_decoupling}
	\item As in Chapter~\ref{chap_1}, the factors of the decomposition return the matrices $\V$ and $\W$ of the decoupled function. Also the univariate functions~$g_1, \ldots, g_r$ are retrieved, after an inter\-polation-integration step (Section~II.C of~\cite{Dreesen2015a}).
	\item The decoupled representation of \f\ is used as the starting point to optimize the nonlinear part of the state-space model. 
	For this, we optimize the following normalized cost function 
	$$
		\min_{\V,\W,\g}
		\frac{
		\sum_{i=1}^N \norm{ \f(\z^{(i)}) - \W\g(\V^T \z^{(i)}) }^2
		}
		{
			\sum_{i=1}^N \norm{ \f(\z^{(i)}) }^2
		}
		.
	$$
	This optimization problem occurs at the level of the inputs and outputs of the nonlinearity \f, instead on the level of the inputs and outputs of the nonlinear state-space model.\label{item_opt}
	\item Finally, the result of step~\ref{item_opt} is used as the initial point for a full model optimization, using cost function~(\ref{6eq_cost_function}).\label{item_2nd_optim}
\end{enumerate}
In this identification scheme, three consecutive optimizations are proposed, starting from the most inner layer of the Jacobian tensor (step~\ref{item_decoupling}), going on to the level of the nonlinearity~\f\ (step~\ref{item_opt}) and arriving at the most outer layer of the full state-space model (step~\ref{item_2nd_optim}). 
Each following optimization uses the end result of the previous optimization as its initialization.
These multiple successive optimizations should result in better local minima than using a single random (and uncontrolled) initialization.

The next sections give a more detailed look into steps \ref{item_PNLSS} and~\ref{item_decoupling} of the proposed identification method.

\subsection[Identifying the coupled nonlinear state-space model]{Identifying the coupled nonlinear state-space \\
model\label{6sec_identif_coupled}}
Given the input and output measurements of a nonlinear system, it is possible to identify it using a polynomial state-space model \cite{Paduart2008}, \cite{Paduart2010}, and~\cite{Paduart2006}. 
This algorithm consists of the following three steps:
\begin{enumerate}
	\item Compute the Best Linear Approximation from the input-output data;
	\item Identify a linear state-space model, without any nonlinearity:
	$$
	\left\{
	\begin{aligned}
		\x(t+1) &= \A \x(t) + \B u(t) \\
		\hat y_{\textrm{linear}}(t) &= \C \x(t) + d u(t).
	\end{aligned}
	\right.
	$$
	This linear model approximates the input-output data, and can be optimized into a nonlinear model in the next step.
	\item For a given degree of the polynomial, use the linear state-space model as a starting point for a nonlinear optimization problem and find the polynomial nonlinear state-space (PNLSS) model.
\end{enumerate}
The end result~(\ref{6eq_state_space_model1}) contains, among others, the coupled multivariate polynomial \f, which we decouple further in the next identification steps.

In practice, identifying the coupled polynomial state-space model is done using the PNLSS toolbox~\cite{Tiels2013} for MATLAB.

%%%%%%%%%%%%%%%%%%%%% Decomposition the Jacobian tensor without polynomial constraint
\subsection[Decomposing the Jacobian tensor without a polynomial constraint]{Decomposing the Jacobian tensor without a\\ polynomial constraint\label{6sec_identif_decouple_not_smooth}}
The Jacobian tensor created in step~\ref{item_creating_J} (Section~\ref{6sec_overview_id}) is decomposed with the CPD, using ALS, the workhorse algorithm for computing the CPD.
The end result contains the factors \V, \W\ and~\H. 
Of these factors, the first two can be directly translated to the decoupled model, but the third should first be interpolated and integrated, when taking the transformated inputs into account (step~\ref{item_recovery_of_gi} of Algorithm~\ref{algor_exact_decoupling} on page~\pageref{algor_exact_decoupling}).

Due to the noisy measurements and because no polynomial constraint was added, one may vaguely guess how the matrix~\H\ represents the derivatives of $g_1, \ldots, g_r$.
Visually, clouds of points appear when plotting the $i$-th column of~$[\h_1, \ldots, \h_r] = \H$ against~$\v_i^T\u_\f$ (Figure~\ref{fig_He_noisy}).

\begin{figure}[ht]
\centerline{%
\includegraphics{chapter6/figures/He.pdf}}
\caption{When no polynomial constraint is added during the CPD in a noisy tensor decomposition setting, the decomposed factors are noisy.
Specifically, the factor~\H\ can hardly represent the derivatives of the decoupled functions, as clouds of points now arise.
These points should be interpolated and integrated, but an extra polynomial constraint will also be considered in Section~\ref{6sec_identif_decouple_smooth}.
The columns of~\H\ in the decoupling are plotted here in the case of a single-branch Wiener-Hammerstein identification using three branches (Section~\ref{sec6_WH_model}).} 
\label{fig_He_noisy}
\end{figure}

The next section introduces a polynomial constraint on the factor \H.
This is a generalization of the MISO technique introduced for NARX models (Section~\ref{sec_als_decompose}).
It directly optimizes the coefficients of the univariate functions $g_1, \ldots, g_r$, instead of their evaluations in~$\v_1^T\z, \ldots, \v_N^T\z$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Decoupling the nonlinearity with a polynomial constraint
\subsection{Decomposing the Jacobian tensor with a polynomial constraint\label{6sec_identif_decouple_smooth}}
In this section, we introduce a polynomial constraint which is added during the ALS iterations, hence creating a relaxed version of ALS.
The first part of this section defines this constraint, and the second part discusses its implications for another ALS factor.

\subsubsection{Introducing the polynomial constraint on \H}
Starting, as in Section~\ref{6sec_identif_decouple_not_smooth}, with the Jacobian tensor $\Jcal$ built with the operating points $\z^{(1)}$, \ldots, $\z^{(N)}$, we wish to decompose it with an additional polynomial constraint.
Adding this constraint should ensure~\H\ to have smooth curved lines (Figure~\ref{fig_He_smooth}) instead of clouds of points (Figure~\ref{fig_He_noisy}).
\begin{figure}[p]
\centering
\includegraphics{chapter6/figures/HeCoeffs.pdf}
\caption{When adding a polynomial constraint during the CPD, the output factor \H\ now represents directly the derivatives of the functions $g_1, \ldots, g_r$.
Because the additional polynomial constraint directly optimizes these functions' coefficients, the interpolation step can be skipped and one just needs to integrate these functions to retrieve the univariate functions $g_1, \ldots, g_r$.
The plots for this figure are generated from the same experiments as in Figure~\ref{fig_He_noisy}.}
\label{fig_He_smooth}
\end{figure}

In the following, we consider the Jacobian tensor of dimensions $(n_\x+1)\times (n_\x+1)\times N$, with $n_\x$ the number of states of the state-space model.
Adding the polynomial constraint occurs during the ALS algorithm~\cite{Kolda2009} for computing the CPD:
\begin{enumerate}
	\item update of \W\ with: $\Jcal_{(1)} \approx \W(\H \odot \V)^T$;
	\item update of \V\ with: $\Jcal_{(2)} \approx \V(\H \odot \W)^T$;
	\item update of \H\ with: $\Jcal_{(3)} \approx \H(\V \odot \W)^T$.\label{item_smoothing}
\end{enumerate}
Instead of updating the factor \H\ in step~\ref{item_smoothing} without any polynomial constraint, we wish to directly update the coefficients of the derivatives of~$g_1, \ldots, g_r$, hence considering the polynomial constraint.
Accordingly, we assume $\h_i$, the $i$-th column of \H, to be written directly as a linear combination of polynomial coefficients~$\c_i \in \ER^{d\times1}$ and the Vandermonde matrix $\X_i$:
\begin{equation}
\h_i = \X_i \, \c_i,
\label{eq_smoothing_constraint}
\end{equation}
The Vandermonde matrix $\X_i$ of dimensions $N\times d$ is defined as all the powers up to degree $d-1$ of the inputs $\v_i\u_f^{(j)}$ of the $i$-th branch function's derivative $g'_i$:
\begin{equation}
	\X_i 
	=
	\begin{bmatrix}
	1 & \v_i\u_f^{(1)} & \Bigl(\v_i\u_f^{(1)}\Bigr)^2 & \cdots & \Bigl(\v_i\u_f^{(1)}\Bigr)^{d-1} \\
	\vdots & \vdots & \vdots & \ddots & \vdots \\
	1 & \v_i\u_f^{(j)} & \Bigl(\v_i\u_f^{(j)}\Bigr)^2 & \cdots & \Bigl(\v_i\u_f^{(j)}\Bigr)^{d-1} \\
	\vdots & \vdots & \vdots & \ddots & \vdots \\
	1 & \v_i\u_f^{(N)} & \Bigl(\v_i\u_f^{(N)}\Bigr)^2 & \cdots & \Bigl(\v_i\u_f^{(N)}\Bigr)^{d-1
\end{bmatrix}.
\label{eq_vandermonde}
\end{equation}
With the imposed constraint~\eqref{eq_smoothing_constraint} for branch $i$, we can rewrite step~\ref{item_smoothing} of the ALS algorithm as the update of the coefficient vector
$$
\c = \myvec{\begin{bmatrix}
	\c_1 & \cdots & \c_r
\end{bmatrix}} \in \ER^{rd\times 1}
$$
with
\begin{equation}
	\Jcal_{(3)} \approx 
	\begin{bmatrix}
		\X_1\c_1 \cdots \X_r\c_r
	\end{bmatrix}
	(\V \odot \W)^T.
\label{eq_update_c}
\end{equation}
Defining the matrix $\X = \begin{bmatrix} \X_1\c_1 \cdots \X_r\c_r	\end{bmatrix} \in \ER^{N\times r}$ for ease of notation and vectorizing both sides of the approximation~\eqref{eq_update_c} yields
\begin{equation}
	\myvec{\Jcal_{(3)}} \approx \myvec{\X (\V \odot \W)^T}.
	\label{eq_vectorized}
\end{equation}Using the Kronecker product's properties \cite{Kolda2009}, the right-side of the vectorized expression~\eqref{eq_vectorized} can be rewritten as follows:
\begin{align*}
	\myvec{\X (\V \odot \W)^T} 
	&= \myvec{\I_N \X (\V \odot \W)^T} \\
	&= \bigl((\V \odot \W) \otimes \I_N \bigr) \myvec{\X} \\
	&= \bigl((\V \odot \W) \otimes \I_N \bigr) \begin{bmatrix}
		\X_1 \\
		& \ddots \\
		&& \X_r
	\end{bmatrix}
	\begin{bmatrix}
		\c_1 \\
		\vdots \\
		\c_r
	\end{bmatrix}
\end{align*}
Hence, the least-squares approximation $\hat\c$ of~\eqref{eq_vectorized} is given by
\begin{equation}
	\hat\c = 
\left(
\bigl((\V \odot \W) \otimes \I_N \bigr)
\begin{bmatrix}
		\X_1 \\
		& \ddots \\
		&& \X_r
	\end{bmatrix} 
	\right)^{\!\!\!\dagger}
\myvec{\Jcal_{(3)}}.
\label{eq_solution_c}
\end{equation}This value for $\hat\c$ is used as the update for $\c$ during the relaxed ALS algorithm with the additional polynomial constraint. 
From the updated $\hat\c$, the updated~$\H$ is computed with Equation~\eqref{eq_smoothing_constraint} and the relaxed ALS iterations continue, updating \W\ and \V\ until a stopping criterion is met.

\subsubsection{Implications of the polynomial constraint on the update of \V}
Introducing the polynomial constraint on the factor \H\ may have negative implications for the update of the factor \V, due to the nonlinearities inside the model.
Decoupling this model with the relaxed ALS framework replaces the single large nonlinear and nonconvex tensor decomposition problem for unstructured tensor decompositions (Section~\ref{sec_tensors_and_decompositions}) by three linear-in-the-parameter problems, which is a great advantage of the relaxed ALS method.
However, when considering the polynomial constraint for the columns in~\H\ by updating the coefficients~$\c_i$ directly, the equations for updating the factor \V\ cannot remain linear.
Indeed, updating \V\ (Step~2 of the relaxed ALS algorithm) is done by keeping the two other factor~\H\ and \W\ constant in the approximation problem
\begin{equation}
	\Jcal_{(2)} \approx \V(\H \odot \W)^T.
	\label{eq_updateV_nonlinear}
\end{equation}
However, in the approximation~\eqref{eq_updateV_nonlinear}, the matrix~$\H$ also depends on the factor~\V, due to the Vandermonde matrices~\eqref{eq_vandermonde} forming the blocks in the matrix \X\ (\ref{eq_smoothing_constraint}).

Hence, solving the approximation~\eqref{eq_updateV_nonlinear} problem for~\H\ (and thus for~\c) seems possible with one of the two following methods:
\begin{enumerate}
	\item Optimize the nonlinear problem 
	\begin{equation}
		\min_\V \norm{\myvec{\Jcal_{(2)} - \V(\H \odot \W)^T}}^2
		\label{eq_update_V}
	\end{equation}
	using a Newton-like gradient-descent tool. 
	\item Even though \H\ (and thus \c) depends nonlinearly on \V, do not take it into account and find solution~\eqref{eq_solution_c}.
	\label{option_2}
\end{enumerate}
For the purposes of this thesis, we have chosen to use and test option~\ref{option_2}, as it lies in the same technical direction as the usual unstructured ALS.
Furthermore, this option yields good results for all three (real-life) case studies, as well for the computation speed as for the model errors (Section~\ref{6sec_case_studies}).

As already mentioned, one should take special care as the nonlinear relation in the problem~\eqref{eq_update_V} is not taken into account.
For example, the normalized CPD cost function
$$
\min_{\W, \V, \H} \frac{\norm{\myvec{\Jcal - \cpdgen\W\V\H}}^2}
{\norm{\myvec{\Jcal}}}
$$
can become non-monotonously decreasing and even be similar to noise (Figure~\ref{fig_CostFunctionIncreases}).
Future research might strive to solve this non-monotonous behavior of the cost function, while keeping a good polynomial constraint on the factor \H.
\begin{figure}[htp!]
\centering
\centerline{\includegraphics{chapter6/figures/CostFunctionIncreases.pdf}}
\centerline{\includegraphics{chapter6/figures/CostFunctionMonotonouous.pdf}}
\caption{One should take care when considering the proposed polynomial constraint of this section.
Because the update of \V\ depends nonlinearly on \H, the cost function may lose its desired monotonous behaviour (top, coming from a single-branch Wiener-Hammerstein decoupling with five branches).
This, however, is not always the case, as some cases show a monotonous cost function (bottom, coming from a single-branch Wiener-Hammerstein decoupling with one branche. Note: even though this cost function does not increase, it actually remains constant. This can happen in a relaxed ALS setting.).}
\label{fig_CostFunctionIncreases}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Case studies with real measurements
\section{Case studies with a simulation and real measurements\label{6sec_case_studies}}
In the following sections, we discuss three case studies in detail of nonlinear state-space system identification.
Section~\ref{sec6_BW_model} gives an overview of the Bouc-Wen model of hysteresis, identified with simulated data.
Sections~\ref{sec6_WH_model} and~\ref{sec6_pWH_model} identify respectively a single-branch and a parallel Wiener-Hammer\-stein system using real-life measured data.
The results of Section~\ref{sec6_BW_model} were presented at the workshop~\cite{Hollander2017a}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Case study: Bouc-Wen model
\subsection{Case study: Bouc-Wen model of hysteresis\label{sec6_BW_model}}
The first discussed case study is with the data coming from the Bouc-Wen model of hysteresis (Section~\ref{sec_case_study_narx_id}).
Using the identification method described in Section~\ref{6sec_identif_coupled}, it is possible to make a coupled polynomial nonlinear state-space model with three states.
Afterwards, this coupled model is decoupled using the techniques proposed in Sections~\ref{6sec_identif_decouple_not_smooth} (without polynomial constraint) and~\ref{6sec_identif_decouple_smooth} (with polynomial constraint). 
Finally, two consecutive optimizations are performed, first on the nonlinearity's level, and then on the full model's level (steps~\ref{item_opt} and~\ref{item_2nd_optim} of Section~\ref{6sec_overview_id}).
The decoupling process is repeated~50 times with different random starting points for the CPD (with or without added polynomial constraint).

The number of branches for the decoupled model should be chosen by the user, and should be less than five for the current case of Bouc-Wen measurements, as a higher number of branches yielded unstable models for these measurements (Figure~\ref{fig_output_errors_wrt_Nbranches}).
However, the number of decoupled branches can generally be tested starting with one branch and increasing the number of branches until an acceptable decoupled model is obtained.

To compare the best model coming from the different starting points, we used a validation signal with~500 samples (Figure~\ref{fig_output_errors}) and computed the normalized RMS of the error signal, defined as
$$
\textrm{normalized error of model} = \frac{\textrm{RMS}(\hat y - y_{\textrm{validation}})}{\textrm{RMS}(y_{\textrm{validation}})},
$$
with $\hat y$ the modeled output and $y_{\textrm{validation}}$ the validation output.
\begin{figure}[ht]
\centerline{%
\includegraphics{chapter6/figures/output_errors.pdf}}
\caption{With a validation signal of~500 sample points, the errors of the decoupled modeled output vary little with or without the polynomial constraint.
These errors, however, are slightly larger than the coupled model's error (Figure~\ref{fig_output_errors_wrt_Nbranches}).}
\label{fig_output_errors}
\end{figure}

From these experiments, we make the following observations.
Adding the polynomial constraint during the experiments does not seem to decrease significantly the model errors, when comparing over different numbers of decoupled branches (Figure~\ref{fig_output_errors_wrt_Nbranches}).
\begin{figure}[ht]
\centering
\includegraphics{chapter6/figures/output_errors_wrt_Nbranches.pdf}
\caption{Considering the data of the Bouc-Wen simulations, the coupled state-space model approximates the validation data with 3\% error.
After the decoupling process, the model errors increase to 6\% with five branches, or to~9\% with a signle-branch decoupled model. 
Depending on the application, simpler models with less parameters are needed (two nonlinear parameters for the single-branch decoupling, as opposed to ten in the five-branch decoupling), if the (small) cost of higher model errors is acceptable.
The coupled model counted 30 nonlinear parameters.
Also, the overal model errors of the decoupling with polynomial constraint ({\color{blue}blue}) and without constraint ({\color{red}red}) are similar}
\label{fig_output_errors_wrt_Nbranches}
\end{figure}
However, considering the polynomial constraint seems to make the decoupling model more robust than without the polynomial constraint, and even more so with many decoupled branches.
For example, when decoupling the model with five branches, the number of stable decouple models doubles from three to six over the~50 random initializations, when considering the polynomial constraint, as opposed to without the added constraint.
Of course, guaranteeing more stable models will certainly improve the current method. 
The open question for further work remains: perhaps other basis functions than polynomials might yield more stable models?


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Case study: Wiener-Hammerstein model
\subsection{Case study: Wiener-Hammerstein model\label{sec6_WH_model}}
A Wiener-Hammerstein model consists of a static nonlinear function surrounded by two linear time-invariant blocks (Section~\ref{sec_block_oriented_models}).
For this case study, we use the benchmark~\cite{Schoukens2009}.
It consists of an electronic circuit made of three parts (Figure~\ref{fig_WH_system}):
\begin{enumerate}
	\item On the left, a Chebyshev filter $L(z)$ with a pass-band ripple of 0.5~dB and a cut-off frequency of 4.4~kHz,
	\item On the right, a third order inverse Chebyshev filter $R(z)$ with a stop-band attenuation of 40~dB starting at 5~kHz,
	\item In between the filters lies a diode circuit serving as the nonlinearity~\f.
\end{enumerate}
\begin{figure}[ht]
\centering
\includegraphics{chapter6/figures/WH_system.pdf}
\caption{The underlying electronic system consists of three parts: two dynamic filters surrounding a static nonlinear function.}
\label{fig_WH_system}
\end{figure}
The electronic system was excited with a filtered Gaussian signal with cut off frequency at 10~kHz.

Using the same identification algorithm of Section~\ref{sec6_BW_model}, three types of models are created:
\begin{enumerate}
	\item A coupled polynomial state-space model with~6 states of polynomial order five (Section~\ref{6sec_identif_coupled}),
	\item A decoupled nonlinear state-space model without polynomial constraint added during the decoupling process (Section~\ref{6sec_identif_decouple_not_smooth}), with the number of branches varying from one to five,
	\item A decoupled nonlinear state-space model with added polynomial constraint during the decoupling process (Section~\ref{6sec_identif_decouple_smooth}), with the number of branches varying from one to five.
\end{enumerate}
These models are validated with a validation signal consisting of~500 samples and the modeled outputs are compared to the measured outputs (Figure~\ref{fig_WH_output_errors}).
Also, the decoupling process was repeated for a set of 50 random starting points and the modeled outputs were compared (Figure~\ref{fig_WH_output_errors_wrt_Nbranches}).

From these Wiener-Hammerstein experiments, we could make the following conclusions.
Because the cost function~\eqref{6eq_cost_function} is highly nonlinear and nonconvex, a set of good intermediary initial points is of crucial importance to achieve the best local minima possible.
Having therefore two decoupling techniques, with and without the added polynomial constraint, can help in the creation of decoupled nonlinear state-space models.
However, the polynomial constraint experimentally seems to not always yield better models as without this constraint, from four decoupled branches on.
Accordingly, both methods can be used on measurements, as it remains unclear which method yields better results in what cases.

Anyway, the decoupled models reduce the number of nonlinear parameters considerably: the coupled state-space models counted~784 parameters (as the nonlinearity is of polynomial order five), whereas the decoupled model with three branches counts only twelve nonlinear parameters.
Decoupling a nonlinear state-space Wiener-Hammerstein of degree five reduced the number of parameters by 98\%, while keeping very small model errors (Figure~\ref{fig_WH_output_errors_wrt_Nbranches}).

Finally, we compared the decoupled models with other linear and nonlinear model types in the literature~\cite{Marconato10--13December2013}.
For this, we considered a two-branch decoupled state-space model consisting of 85 parameters and whose not normalized output error is 6.83 mV (Figure~\ref{fig_comparison_with_other_models_WH}).
This model lies somewhat in the middle of other existing methods of the literature.

\begin{figure}[htp]
\centerline{%
\includegraphics{chapter6/figures/WH_output_errors.pdf}}
\caption{Starting from the Wiener-Hammerstein measurements, it is possible to decouple the coupled nonlinear state-space model ({\color[rgb]{0.2,0.8,0.2}green}) and obtain a decoupled representation. 
In the case of the three-branch decoupling, the modeled errors are slightly larger when adding the polynomial constraint ({\color{blue}blue}), in comparison to without adding the extra constraint ({\color{red}red}).
However, this result may be different depending on the number of decoupled branches (Figure~\ref{fig_WH_output_errors_wrt_Nbranches}) and both can be compared for different applications.}
\label{fig_WH_output_errors}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics{chapter6/figures/WH_output_errors_wrt_Nbranches.pdf}
\caption{When decoupling the coupled nonlinear state-space model for the Wiener-Hammerstein measurements, we compare the effect of the added polynomial constraint ({\color{blue}blue}).
Even though the normalized output errors are quite similar for a small number of decoupled branches, the experiments yield overall smaller errors without the added constraint ({\color{red}red}), from four decoupled branches on.
However, all decoupled models (with or without polynomial constraint) are almost as good as the coupled state-space model.
}
\label{fig_WH_output_errors_wrt_Nbranches}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics{chapter6/figures/comparison_with_other_models_WH.pdf}
\caption{When comparing the two-branch decoupled nonlinear state-space model created in this section, it lies somewhat in the middle of other existing linear and nonlinear modeling techniques.
The two-branch decoupled state-space model consists of 85 parameters and it not normalized output error is 6.83 mV.
The basis of this plot originates from~\cite{Marconato10--13December2013}.
}
\label{fig_comparison_with_other_models_WH}
\end{figure}

% \afterpage{\clearpage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Case study: parallel Wiener-Hammerstein model
\subsection[Case study: parallel Wiener-Hammerstein model]{Case study: parallel Wiener-Hammerstein\\ model\label{sec6_pWH_model}}
Generalizing a single-branch Wiener-Hammerstein model to a two-branch parallel Wiener-Hammer\-stein adds modeling power and complexity (Section~\ref{sec_block_oriented_models}).
This section considers real-life electronic input-output measurements coming from a two-branch parallel Wiener-Hammer\-stein system, and models it using a (decoupled) nonlinear state-space model.

The device under test (DUT) consists of two branches of Wiener-Hammerstein systems (\cite{MaartenSchoukens2014} and Figure~\ref{fig_WH_system_2branches}):
\begin{enumerate}
	\item In these two branches, the front and back filters are third-order infinite-input response (IIR) filters.
	\item The nonlinearities are created with a diode-resistor network.
\end{enumerate}
%
\begin{figure}[htp]
\centering
\includegraphics{chapter6/figures/WH_system_2branches.pdf}
\caption{The underlying electronic system for the third case study consists of a diode-resistor network and four IIR filters. 
In this section, it will be modeled with (decoupled) nonlinear state-space models containing eight states.}
\label{fig_WH_system_2branches}
\end{figure}
%
Furthermore, the DUT was excited with a random phase multisine (Section~\ref{sec_real_system} and \cite{Pintelon2012}), consisting of 131072 sample points and a flat amplitude spectrum. The phases are independent uniformly distributed, ranging from~0 to~$2\pi$.

As in Section~\ref{sec6_WH_model}, three models are considered with the proposed identification techniques, one coupled and two decoupled:
\begin{itemize}
 	\item Using the PNLSS toolbox~\cite{Tiels2013}, an eight-state coupled model is created, containing a coupled nonlinear function;
 	\item The coupled model is decoupled twice, once with and once without the additional polynomial constraint (Section~\ref{6sec_identif_decouple_smooth}).
 \end{itemize}
The number of decoupled branches varies from one to ten (Figure~\ref{fig_pWH_output_errors_wrt_Nbranches}) as a higher number of branches seems to yield unstable models. 
In what follows, we give an overview of the results.

The smallest model errors (order of 0.1\% relative to the validation output signal) are given by the coupled nonlinear state-space model (Figures~\ref{fig_pWH_output_errors} and~\ref{fig_pWH_output_errors_wrt_Nbranches}).
Because this coupled model contains eight states, its nonlinear function consists of~210 purely nonlinear monomials (second and third degree monomials).
To decrease the complexity of the coupled model, while keeping the lowest possible model errors, we propose to consider the decoupled models with two or three branches (Figure~\ref{fig_pWH_output_errors_wrt_Nbranches}).
These decoupled models contain respectively only four (for the two-branch decoupling) or six (for the three-branch decoupling) purely nonlinear parameters (second and third degree monomials), which decreases the number of parameters by 98\% or 97\% respectively.
Therefore, the decoupling step introduces model errors, but they are in the order of 1\% or 2\% (Table~\ref{table_overview_errors_pWH}).

\begin{figure}[htp]
\centerline{%
\includegraphics{chapter6/figures/pWH_output_errors.pdf}}
\caption{Starting with the parallel Wiener-Hammerstein measurements, we decoupled the coupled ({\color{DarkGreen}green}) eight-states model with three branches.
These decoupled models yield only small differences between adding ({\color{blue}blue}) or removing ({\color{red}red}) the polynomial constraint.
The coupled model's error, however, are smaller than those of the decoupled model, but it's complexity is also higher.}
\label{fig_pWH_output_errors}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics{chapter6/figures/pWH_output_errors_wrt_Nbranches.pdf}
\caption{In the case of the parallel Wiener-Hammerstein measurements, the best decoupled model's errors are achieved using two and three decoupled branches, and these errors are similar with the polynomial ({\color{blue}blue}) and without it ({\color{red}red}).
From the experiments: the higher the number of decoupled branches, the more the decoupled model may become unstable: at every number of decoupled branches,~50 random initializations are tested for the decoupling step. 
Of the constructed~50 models, only the stable models are shown, on a validation signal of~500 sample points.}
\label{fig_pWH_output_errors_wrt_Nbranches}
\end{figure}


\begin{table}[htp]
	\begin{center}
		\begin{tabular}{r|p{2cm}p{2cm}p{2.4cm}}
			model type & coupled & decoupled two branches & decoupled three branches \\
			\hline 
			error 	          & 0.14\% & 1.26\%	  & 0.86\% \\
			error increase by & 		& $\!{}\times9$   & $\!{}\times6.1$ \\
			\# NL par         & 210    & 4 		  & 6 \\
			par reduction by  &  	    & $\!{}\div 52.5$ & $\!{}\div 35$
		\end{tabular}
	\end{center}
	\caption{When decoupling the coupled model created from the parallel Wiener-Hammerstein measurements, to two and three-branch decoupled models, the model errors increase, but the number of parameters is greatly reduced.
	As a reference, the best constructed linear state-space model~\cite{Paduart2010} contains 81 parameters and has a relative output error of 19,41\%.
	Therefore, considering nonlinear models offers a good error reduction.
	}
	\label{table_overview_errors_pWH}
\end{table}

The proposed identification method with decoupled nonlinear state-space models offers parsimonious models at a very small error cost.
However, a drawback of the current method is the introduction of possible unstable models.
This disadvantage can be addressed by considering other basis functions for the nonlinear functions, instead of possibly unbounded polynomials, and can be considered in future work.

Furthermore, considering the nonlinear SISO functions of the two and three-branch decoupled models gives intuitive understanding of the underlying functions.
These underlying functions are saturating, which can be (partially) retrieved from the two-branch decoupling (Figure~\ref{fig_pWH_gi}).
However, even though the three-branch decoupled model yields (slighty) better model errors, its nonlinear functions look less saturating.
In conclusion, even though a decoupled model is simpler than a coupled model, some prior knowledge of the DUT is needed to interpret the SISO nonlinearities physically or intuitively.

\begin{figure}[ht]
\centering
\includegraphics{chapter6/figures/pWH_gi_2branches.pdf}
\includegraphics{chapter6/figures/pWH_gi_3branches.pdf}
\caption{In the underlying system creating the parallel Wiener-Hammerstein measurements, the nonlinear part is created with a diode-resistor network, which has a saturating nonlinearity.
Trying to retrieve this nonlinear behaviour in the internal nonlinearities may be tempting (top with two decoupled branches and bottom with three decoupled branches).
However, care should be taken, as the saturating behaviour of the internal functions could alter when combining the transformation matrices.
}
\label{fig_pWH_gi}
\end{figure}
 \afterpage{\clearpage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Conclusion
\section{Conclusion\label{sec6_conclusion}}
By adding a polynomial constraint during the tensor decompositions step of the Jacobian method, we could guarantee a smooth third factor in the tensor decomposition.
This extra constraint adds intuitive insights to the decoupled problem, but does not always seem to yield smaller model errors after the optimization of cost function~\eqref{6eq_cost_function}.
Nevertheless, we have been able to create very good decoupled polynomial nonlinear state-space models for three case studies, two of which with real-life measurements.

This opens many doors for future development.
More case studies could be tested, with more complicated nonlinear behaviors.
Also, other scientific or engineering disciplines could gain insights thanks to these powerful parsimonious nonlinear models.
Finally, working with more stable basis functions might help in the creation of more stable models.

As general rules of thumb for the results of Part~\ref{part_2}, the following proposed rules can be given:
\begin{itemize}
	\item When modeling real-life measurements or computer simulations with NARX or nonlinear state-space models, the coupled models can be decoupled starting with one branch.
		Consequently, the number of decoupled branches can be increased until the output error is below a user defined level, depending on every application.
	\item When decoupling NARX or nonlinear state-space models, the unconstrained decoupling method~\cite{Dreesen2015} can be compared to the proposed constrained decoupling method (Section~\ref{6sec_identif_decouple_smooth}).
	Accordingly, the best result can be used as decoupled model representation.
\end{itemize}

