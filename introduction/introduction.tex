% \bgroup %%%%% begin scope for the section numberings
% \renewcommand*\thesection{\arabic{section}}
% \setcounter*{chapter}{1}
\chapter{Introduction}\label{chap_general_intro}
% \addcontentsline{toc}{chapter}{Introduction}

% \abstracttext{\noindent This thesis's research belongs to the general field of system identification.
% This field creates mathematical models of physical systems, based on the measurements of its input and output signals.
% In this introductory chapter, the general context of this thesis is surveyed (Section~\ref{general_context}), which leads to this thesis' general research questions (Section~\ref{central_question}).
% The general outline of this thesis is given in Section~\ref{sec_overview_of_thesis} and my contributions are enumerated in Section~\ref{sec_contributions}.
% }
\leavevmode
\vskip-2cm
\null
\section{General context of this thesis\label{general_context}}
% The ever-expanding technological world uses mathematical models at its core, for modeling the future or understanding the past.
% These mathematical models can be used when creating new systems or when simulating physical constructions, or they could be applied in automatic control systems.

% \subsubsection*{Signals}
% Although models may be considered in a wide variety of scenarios, we have focused our attention to \emph{signals}.
% In this context of signal processing and electrical engineering, a signal is a mathematical function containing the information about some physical phenomenon~\cite{Oppenheim1983}.
% The information contained in a signal is very broadly understood, and can be, amongst other, one of the following physical entities: voltage, current, temperature, light intensity, speed, acceleration, rotation angle.
% In this thesis, we focus our attention to \emph{one-dimensional signals}, i.e., the information contained in the signal can be represented on a scalar scale (Figure~\ref{fig_signal}).
% \begin{figure}[p!]
% \centerline{%
% \includegraphics{introduction/figures/signal.pdf}
% }
% \caption{The concept of signal lies at the core of signal processing and system identification, and is the mathematical representation of the carrier of information.
% In this thesis, we consider solely one-dimensional signals, that is, the information contained in the signal can be represented on a scalar scale.} 
% \label{fig_signal}
% \end{figure}

% \subsubsection*{Dynamical System}
% Studying solely one signal is somewhat artificial from a practical standpoint.
% Hence, in the context of system identification, we consider two signals and the transformation of the first---the \emph{input signal}--- into the second---the \emph{output signal} (Figure~\ref{fig_system}).
% The process that results in this transformation is called a \emph{system}~\cite{Oppenheim1983}, and examples of systems are everywhere around us:
% \begin{itemize}
% \item electrical systems relate input and output voltages or currents in electrical circuits,
% \item biological systems relate human movements with or without electrical signals in the brain,
% \item mechanical systems relate mechanical movements with velocity and acceleration in different points of a physical structure,
% \item chemical systems relate concentrations and other properties of chemicals.
% \end{itemize}
% Moreover, these systems are dependent of time, and are called a \emph{dynamical systems}, as opposed to static systems, which are independent of time.
% As many systems in the world have dynamic effects, we focused our attention to dynamical systems during this research.

% \begin{figure}[p!]
% \centerline{%
% \includegraphics{introduction/figures/system.pdf}
% }
% \caption{A dynamical system is the process of transforming an input signal to an output signal.}
% \label{fig_system}
% \end{figure}


The general context of this thesis is the field of nonlinear dynamical system identification.
First, the vast field of dynamical system identification is outlined.
Then, the subfield of nonlinear system identification is introduced.

\bigskip
% \subsubsection*{System identification}
\textbf{Dynamical system identification} is the art to create appropriate models, starting from the measurements of the inputs and outputs of a physical but unknown dynamical system.
Based on these measured signals, a mathematical model of this system is searched for, which aims to predict the system's output as a function of its input, and which may offer physical interpretation of the studied system.
Mathematically, the model can be represented as a differential or difference equation, respectively in continuous or discrete time.
As measured signals are usually digitized, we focused our attention to discrete time models.

Furthermore, models can be categorized into the two following categories:
\begin{enumerate}
	\item A model can be \emph{non-parametric}, whenever the model structure is not specified a priori, but it is determined from the data.
	For example, the impulse response of a linear model (see further) can be used as a non-parametric model.
	\item A model can be \emph{parametric}, if its model structure and the number of parameters is defined before the modeling stage.
	During this modeling stage, these parameters are then tuned based on the measured data.
	As the parametric models generally offer better intuitive insights on the studied system, we focused our attention on the study of parametric models.
\end{enumerate}

% \subsubsection*{Nonlinear system identification}
In this thesis, we focus our attention to the subfield of \textbf{nonlinear dynamical system identification} that considers mathematical models, in which the underlying equations are nonlinear in the inputs.
These nonlinear models can, in theory, better capture the nonlinear effects of the world surrounding us, as opposed to their countersides, the linear models, which underlying equations are linear.
However, nonlinear models can generally be far more complex than linear models, and in some cases, even result in difficult or impossible computations.
Furthermore, large nonlinear models may become difficult to intuitively or physically understand, compared to the simpler linear models.

% To effectively reduce the complexity of nonlinear models, we have searched for approximate structured representations of the given model.
% Considering these simpler and structured representations is a promising approach, as it helps with the computation of these models, and can give us physical or intuitive insights of the system being studied.

% \subsubsection*{Tensors}
% To create approximate and structured representations of nonlinear models, we have used so-called tensors and their decompositions.
% A \emph{tensor} is the mathematical abstraction of a multidimensional array of numbers and is an actively researched in the general field of applied mathematics.
% The structure discovered by \emph{decomposing} a tensor lies at the heart of the approach used in this thesis, and can be translated into a structured representation of nonlinear models.

\section{Central research questions\label{central_question}}
The current research belongs to this general framework of nonlinear system identification and its complex discrete-time parametric nonlinear dynamical models.
In detail, we have considered the following research question throughout this thesis:
\begin{quotation}
	\noindent\it $Q_1$: How is it possible to construct a simpler nonlinear model than the currently available nonlinear models, which approximates a given complex nonlinear model well?
\end{quotation}
Two portions of this research question are detailed below:
\begin{enumerate}
	\item Referring to \emph{a simpler nonlinear model}: instead of considering linear approximations of a given nonlinear model, we focused our attention to create nonlinear approximated models.
	These nonlinear approximations combine the modeling power of nonlinear models and reduce the complexity in the approximation.
	\item Referring to \emph{approximates a given complex nonlinear model well}: in the context of dynamical models, several possible approximation criteria can be formulated, by defining different cost functions.
	The cost functions can vary depending on the case and may have a big impact on the modeling process.
	In this thesis, these cost functions are generally defined in terms of the modeled output, and are stated for the considered models.
\end{enumerate}

The research question $Q_1$ stated above is very general and points the general research direction of this thesis.
However, because nonlinear modeling is a vast field, we have focused our attention to the following three types of nonlinear models:
\begin{enumerate}
	\item the block-oriented models,
	\item the nonlinear autoregressive models with exogenous inputs (NARX models),
	\item the nonlinear state-space models.
\end{enumerate}
Each of these model types can be modeled with the state-of-the-art techniques, and their models contain a so-called static coupled nonlinear function.
Such a coupled nonlinear function can be simplified (research question $Q_1$) by replacing the coupled function by a so-called structured decoupled function.
A few state-of-the-art techniques have been proposed~\cite{Dreesen2015,Tiels2013,Usevich2014}, and they inherently use (multi-) linear algebra techniques at their core.
However, even though the available decoupling techniques look very promising, they were developed in a mathematical context, and a generalization to measurements and applications is the next step forward.
For the reasons detailed in Chapter~\ref{chap_introduction}, we have focused our attention to the promising tensor-based decoupling approach, which considers the first-order derivatives of the coupled nonlinear function~\cite{Dreesen2015,Dreesen2015a}.

Therefore, the general research question $Q_1$ can be reformulated in more detail, as follows:
\begin{quotation}
	\noindent\it $Q_2$: For the three nonlinear model types described above, how is it possible to generalize the existing tensor-based decoupling method in application contexts with real-life measured data?
\end{quotation}
Every chapter of this thesis tries to answer to both the general research question $Q_1$ and the detailed research question~$Q_2$.


\section{Overview and outline of this thesis\label{sec_overview_of_thesis}}
This thesis gives an in-depth overview of proposed solutions for the approximated decoupling problem for coupled nonlinear functions.
With the mathematical multidimensional arrays of scalars at its heart, this thesis tries to bridge mathematical solutions to practical engineering applications in the context of nonlinear system identification.

For the static nonlinearities, we focus our attention in this thesis to (coupled multivariate) polynomials of degree three as nonlinearities, for the following three reasons:
\begin{enumerate}
	\item polynomials are popular building blocks of models, as they can approximate smooth functions arbitrarily well,
	\item estimating polynomial models is a problem which is linear in the parameters,
	\item polynomials of degree three contain all the building blocks to approximate even and odd nonlinear behavior.
\end{enumerate}

Apart from Introduction (Chapter~\ref{chap_general_intro}) and Preliminaries (Chapter~\ref{chap_introduction}), this thesis is organized into two general parts, as follows (Figure~\ref{fig_summary_chapters}): 
\begin{enumerate}
	\item Part~\ref{part_1} discusses the decoupling technique on the block-oriented models, which is a type of gray-box models.
	Chapter~\ref{chap_1} generalizes the decoupling method~\cite{Dreesen2015} by incorporating a weight matrix during the tensor decomposition process.
	Further, Chapter~\ref{chap_2} applies our proposed weighted decoupling method on several numerical examples.
	Finally, Chapter~\ref{chap_3} shows a full identification process, from measurements to decoupled model.
	\item Part~\ref{part_2} surveys decoupling methods for black-box models.
	First, Chapter~\ref{chap_4} proposes a technique for reducing the number of parameters of structured nonlinear models.
	Then, Chapter~\ref{chap_many_branches} discusses approaches to decouple NARX models.
	Finally, Chapter~\ref{chap_real_measurements} gives an overview of decoupled nonlinear state-space models, identified with real-life measurements.
\end{enumerate}
In Chapter~\ref{chap_conclusions_future_work}, general conclusions of this thesis are summarized, and possible future work paths are proposed.

\afterpage{%
    \clearpage% Flush earlier floats (otherwise order might not be correct)
		\begin{landscape}
			\begin{figure}[p]
			\centering
			\includegraphics{preliminaries/figures/summary_chapters.pdf}
			\vskip1cm
			\caption{This thesis consists of two general parts, each considering different nonlinear model types.
			Block-oriented models are examples of the more general framework of gray-box models and their decoupled identification process will be covered in Part~\ref{part_1}.
			NARX models and nonlinear state-space models are examples of black-box models and will be identified and decoupled in Part~\ref{part_2}.}
			\label{fig_summary_chapters}
			\end{figure}
		\end{landscape}
	\clearpage
}


\section{Contributions in this thesis\label{sec_contributions}}
In this PhD thesis, the state-of-the-art techniques for decoupling multivariate polynomials have gone forward with our contributions.
These contributions can be summerized as follows:
\begin{itemize}
	\item Construction of the \emph{weighted} Canonical Polyadic Decomposition, with a rank-deficient weight matrix (Chapter~\ref{chap_1}).
	\item Analysis of the limitations in the noisy case of the former state-of-the-art decoupling method~\cite{Dreesen2015} (Chapter~\ref{chap_2}).
	\item Decoupling noisy multivariate polynomials in the context of block-oriented system identification, with several possible weight matrices (Chapters~\ref{chap_2} and~\ref{chap_3}).
	\item Approximation and reduction of decoupled nonlinear models by simpler decoupled models (Chapter~\ref{chap_4}).
	\item Reducing the number of parameters of NARX models, by comparing two matrix decomposition methods (Chapter~\ref{chap_many_branches}).
	\item Adding a polynomial constraint during the tensor decomposition, imposing a polynomial structure (Chapters~\ref{chap_many_branches} and~\ref{chap_real_measurements})
	\item Decoupling multivariate polynomials in the context of nonlinear state-space identification (Chapter~\ref{chap_real_measurements}).
	\item Addition of a smoothness constraint for decoupled representation with a large number of decoupled branches (Chapter~\ref{chap_real_measurements}).
	\item Application of our proposed decoupled methods on real-life measurements (Chapters~\ref{chap_many_branches} and~\ref{chap_real_measurements}).
\end{itemize}
In conclusion, this thesis gives a broad overview of our analysis and contributions for the decoupling problem of multivariate polynomials, in the context of noisy simulations and measurements.












