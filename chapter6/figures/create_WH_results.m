addpath('Z:\2016-10-11 WH Benchmark\PNLSS 3_2 decoupled');
addpath('Z:\2016-03c WH Approximation');
addpath('Z:\2016-10-11 WH Benchmark');

direc = 'Z:\2016-10-11 WH Benchmark\results with validation and coeffs';
files = dir(fullfile(direc, '*es.mat'));


for iterrr = 1 : length(files)
    iterrr
    load(fullfile(direc, files(iterrr).name));
    errorNoCoeffs{iterrr} = y_relErrorOptimizedValBenchmark(y_relErrorOptimizedValBenchmark(~isnan(y_relErrorOptimizedValBenchmark)) ~= 0);
    errorCoeffs{iterrr} = y_relErrorOptimizedCoeffsValBenchmark(y_relErrorOptimizedCoeffsValBenchmark(~isnan(y_relErrorOptimizedCoeffsValBenchmark)) ~= 0);
    
    yNoCoeffs{iterrr} = yOptimized_decoupledVal;
    yCoeffs{iterrr} = yOptimizedCoeffs_decoupledVal;
    yBench{iterrr} = yBenchmarkVal(modelh.T2+1 : end);
    
%     compute the normalized coupled errors
    yCoupled{iterrr} = fFilterNLSS(modelh, uVal);
    yCoupled{iterrr} = yCoupled{iterrr}(modelh.T2+1 : end);
    errorCcoupled{iterrr} = norm(yCoupled{iterrr} - yBenchmarkVal(modelh.T2+1 : end)) / norm(yBenchmarkVal(modelh.T2+1 : end));

end

%%
eps = 0.03;
clf;
hold on;
for iterrr = 1 : length(files)
	plot((iterrr-eps)*ones(1, length(errorNoCoeffs{iterrr})), errorNoCoeffs{iterrr}, 'r.', 'markersize', 3)
    plot((iterrr+eps)*ones(1, length(errorCoeffs{iterrr})), errorCoeffs{iterrr}, 'bx')
end
plot([0 length(files)+0.2], ones(1,2)*min(errorCcoupled{1}), 'k:');
text('string', 'coupled model error', 'posi', [2.5, 0.02], 'hor', 'center')
hold off;
set(gca, 'xlim', [1-0.2 length(files)+0.2], 'xtick', 1:length(files), 'ylim', [0 0.4], 'ytick', [ min(errorCcoupled{1})  min(errorNoCoeffs{1}) 0.4], ...
    'yticklabel', arrayfun(@(x)(sprintf('%.2f', x)) ,  [ min(errorCcoupled{1})  min(errorNoCoeffs{1}) 0.4], 'uni', 0))
xlabel('Number of decoupled branches');
ylabel('Normalized model error');
legend('Decoupling without smoothness constraint', 'Decoupling with smoothness constraint', 'location', 'northoutside')

% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\WH_output_errors_wrt_Nbranches.tex', 'width', '0.8\textwidth', 'height', '5cm');


%%
% clf;
% hold on;
% plot(yBench{Nbranches}, 'k')
% plot(yCoupled{Nbranches} - yBench{Nbranches}, 'color', [0.2 0.8 0.2])
% plot(yNoCoeffs{Nbranches} - yBench{Nbranches}, 'r');
% plot(yCoeffs{Nbranches} - yBench{Nbranches}, 'b');
% xlabel('Time samples')
% ylabel('Signal output')
% legend('Validation output signal', 'Errors with coupled model', 'Errors with decoupled model (no smoothness)', 'Errors with decoupled model (with smoothness)', 'location', 'northoutside')
% set(gca, 'xtick', [0 500], 'ytick', [-0.6 0 0.6], 'ylim', [-0.6 0.6])
% 
% hold off;


Nbranches = 3;
N = length(yBench{3});
clf;
hold on;
plot(db(fft(yBench{Nbranches})), 'k')
plot(db(fft(yCoupled{Nbranches} - yBench{Nbranches})), 'g+')
plot(db(fft(yNoCoeffs{Nbranches} - yBench{Nbranches})), 'r.', 'markersize', 3);
plot(db(fft(yCoeffs{Nbranches} - yBench{Nbranches})), 'bx', 'markersize', 3);
xlabel('Frequency (normalized)')
ylabel('Signal output (db of magnitude)')
legend('Validation output signal', 'Errors with coupled model', 'Errors with decoupled model (no smoothness)', 'Errors with decoupled model (with smoothness)', 'location', 'northoutside')
set(gca, 'xtick', [0 N/2], 'xticklabel', {0 sprintf('%.1f', 1/2)}, 'xlim', [0 N/2], 'ytick', [-110 -20])

hold off;


% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter6\figures\WH_output_errors.tex', 'width', '0.8\textwidth', 'height', '5cm');






