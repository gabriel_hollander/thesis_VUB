benchMark = load('Z:\2016-10-19 Bouc-Wen\DATA\MSdata20160403_221443_7250_55NP7.mat');
estimation_length = 2000;
validation_length = 2000;
u = benchMark.u(:,1);
Nsamples = 8192;

%% plot 1 (time domain)
plot(u(1:Nsamples))
set(gca, 'xlim', [1 Nsamples], 'xtick', [1 Nsamples], 'ytick', [-200 0 200])
xlabel('Time samples')
ylabel('input')
% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\plot_benchmark1.tex', 'width', '0.8\textwidth', 'height', '3cm');

%% plot 2 (frequency domain)
plot((0:Nsamples-1)/Nsamples*750, db(fft(u(1:Nsamples))))
set(gca, 'xlim', [0 750/2], 'xtick', [0 5 150 750/2], 'ytick', [-300 0 100])
xlabel('Frequency (Hz)')
ylabel('input (db of magnitude)')
% CurrentFigure2pdf('C:\Users\gabriel.hollander\Google Drive\Doctoraat Thesis\Thesis\chapter5\figures\plot_benchmark2.tex', 'width', '0.8\textwidth', 'height', '3cm');




